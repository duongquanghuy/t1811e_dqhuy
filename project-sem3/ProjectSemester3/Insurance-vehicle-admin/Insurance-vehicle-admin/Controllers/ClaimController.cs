﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class ClaimController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        EmailerService Mailer { get; }
        public ClaimController(InsuranceVehicleContext dbContext , EmailerService mailer)
        {
            insuranceVehicleContext = dbContext;
            Mailer = mailer;
        }
        [HttpGet("searchsClaimApprovel/{BillCId}")]
        public BaseResponseData getBillCompensationToClaim(int BillCId)
        {
            try
            {
                var ClaimDetail = insuranceVehicleContext.BillCompensation.Where(x => x.Id == BillCId).
                   Select(bill =>
                      new ClaimDetailsDTO()
                      {
                           //thong tin claim
                          ClaimId = bill.ClaimDetail.Id,
                          BodyClaim = bill.ClaimDetail.BodyClaim,
                          CreateAt = bill.ClaimDetail.CreatedAt,
                          ClaimStatus = bill.ClaimDetail.Status,

                           //thong tin insurance
                          InsuranceId = bill.ClaimDetail.Insurance.BillOfSale.Insurance.Id,
                          PricePercentByCarValue = bill.ClaimDetail.Insurance.BillOfSale.Insurance.PricePercentByCarValue,
                          MinValueRefund = bill.ClaimDetail.Insurance.BillOfSale.Insurance.PricePercentByCarValue,
                          MaxValueRefund = bill.ClaimDetail.Insurance.BillOfSale.Insurance.MaxValueRefund,
                          DurationMonthUnit = bill.ClaimDetail.Insurance.BillOfSale.Insurance.DurationMonthUnit,

                           //
                          BillOfSaleId = bill.ClaimDetail.Insurance.BillOfSaleId,
                          InsuranceDurationMonthly = bill.ClaimDetail.Insurance.BillOfSale.InsuranceDurationMonthly,
                          CreatedBy = bill.ClaimDetail.Insurance.BillOfSale.CreatedBy,
                          TotalPrice = bill.ClaimDetail.Insurance.BillOfSale.TotalPrice,
                          PaymentStatus = bill.ClaimDetail.Insurance.BillOfSale.PaymentStatus,
                          CreatedAtBillOfSale = bill.ClaimDetail.Insurance.BillOfSale.CreatedAt,
                           //
                           LicensePlates = bill.ClaimDetail.Insurance.BillOfSale.Verhicle.LicensePlates,
                          VehicleVersionName = bill.ClaimDetail.Insurance.BillOfSale.Verhicle.Version.Name,
                          VehicleModelName = bill.ClaimDetail.Insurance.BillOfSale.Verhicle.Model.Name,
                           //
                          CusPhone = bill.Customer.PhoneNumber,
                          CustomName = bill.Customer.Name,
                          CusEmail = bill.Customer.Email,
                          Passport = bill.Customer.Passport,
                           //
                          InsuranceSoldid = bill.InsuranceSoldId,
                          ExpiresOn = bill.InsuranceSold.ExpiresOn.Date,
                          CreatedAtInsuranceSold = bill.InsuranceSold.CreatedAt,
                          Reason =  bill.Reason,
                          TotalValue = bill.TotalValue,

                           //tinh sô lần hiển thị còn lại
                          Billcount = bill.ClaimDetail.BillCompensation.Count(),
                          MaxRefundQuantity = bill.ClaimDetail.Insurance.BillOfSale.Insurance.MaxRefundQuantity
                      }

                   ).FirstOrDefault();
                return new BaseResponseData { Code = 200, Data = ClaimDetail, Message = "Success Claim" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        
        [HttpGet("claimApprovel")]
        public BaseResponseData getAllBillCompensationClaimDetails(string valueSearch = null , DateTime? timeFrom = null, DateTime? timeTo = null, int page = 1, int pageSize = 20)
        {
            try
            {
                IEnumerable<BillCompensationDTO> data = null;
                data = insuranceVehicleContext.BillCompensation.OrderByDescending(x => x.CreatedAt).
                    Select(bill => new BillCompensationDTO()
                    {
                        Id = bill.Id,
                        ClaimID = bill.ClaimDetailId,
                        BodyClaim = bill.ClaimDetail.BodyClaim,
                        CustomerId = bill.CustomerId,
                        LicensePlates = bill.InsuranceSold.BillOfSale.Verhicle.LicensePlates,
                        VehicleVersionName = bill.InsuranceSold.BillOfSale.Verhicle.Version.Name,
                        VehicleModelName = bill.InsuranceSold.BillOfSale.Verhicle.Model.Name,
                        CustomName = bill.Customer.Name,
                        CusEmail = bill.Customer.Email,
                        CusPhone = bill.Customer.PhoneNumber,
                        TotalValue = bill.TotalValue ,
                        CreateAt = bill.ClaimDetail.CreatedAt.Date,
                      
                    });
                if (timeFrom != null && timeTo != null)
                {
                    data = data.Where(x => x.CreateAt >= timeFrom.Value.Date && x.CreateAt <= timeTo.Value.Date).AsQueryable();
                }
                if (valueSearch != "" && valueSearch != null)
                {
                    var serrchBill = string.IsNullOrEmpty(valueSearch) ? "" : valueSearch.Trim().ToLower();
                    data = data.Where(x => x.CustomName.ToLower().Contains(serrchBill) || x.CusPhone.ToLower().Contains(serrchBill) || x.CusEmail.ToLower().Contains(serrchBill));
                }
                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "Success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }

        }
        [HttpGet("searchUnpaid/{claimId}")]
        public BaseResponseData getClaimDetail(int claimId)
        {
            try
            {
                var ClaimDetail = insuranceVehicleContext.ClaimDetail.Where(x => x.Id == claimId).
                    Select(claim =>
                       new ClaimDetailsDTO()
                       {
                           //thong tin claim
                          ClaimId = claim.Id,
                          BodyClaim = claim.BodyClaim,
                          CreateAt = claim.CreatedAt,
                          ClaimStatus = claim.Status,

                          //thong tin insurance
                          InsuranceId = claim.Insurance.BillOfSale.Insurance.Id,
                          PricePercentByCarValue = claim.Insurance.BillOfSale.Insurance.PricePercentByCarValue,
                          MinValueRefund = claim.Insurance.BillOfSale.Insurance.PricePercentByCarValue,
                          MaxValueRefund = claim.Insurance.BillOfSale.Insurance.MaxValueRefund,
                          DurationMonthUnit = claim.Insurance.BillOfSale.Insurance.DurationMonthUnit, 

                          //
                          BillOfSaleId =claim.Insurance.BillOfSale.Id,
                          InsuranceDurationMonthly = claim.Insurance.BillOfSale.InsuranceDurationMonthly,
                          CreatedBy = claim.Insurance.BillOfSale.CreatedBy,
                          TotalPrice = claim.Insurance.BillOfSale.TotalPrice,
                          PaymentStatus = claim.Insurance.BillOfSale.PaymentStatus,
                          CreatedAtBillOfSale = claim.Insurance.BillOfSale.CreatedAt,
                          //
                          LicensePlates = claim.Insurance.BillOfSale.Verhicle.LicensePlates,
                          VehicleVersionName = claim.Insurance.BillOfSale.Verhicle.Version.Name,
                          VehicleModelName = claim.Insurance.BillOfSale.Verhicle.Model.Name,
                          //
                          CusPhone = claim.Customer.PhoneNumber,
                          CustomName = claim.Customer.Name,
                          CusEmail = claim.Customer.Email,
                          Passport = claim.Customer.Passport,
                             //
                         InsuranceSoldid = claim.InsuranceId,
                         ExpiresOn = claim.Insurance.ExpiresOn.Date,
                         CreatedAtInsuranceSold =claim.Insurance.CreatedAt,

                            //tinh sô lần hiển thị còn lại
                         Billcount = claim.BillCompensation.Count(),
                         MaxRefundQuantity = claim.Insurance.BillOfSale.Insurance.MaxRefundQuantity
                       }

                    ).FirstOrDefault();
                return new BaseResponseData { Code = 200, Data = ClaimDetail, Message = "Success Claim" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("claimUnpaid")]
        public BaseResponseData getAllClaimDetail( string valueSearch = null , DateTime? timeFrom = null, DateTime? timeTo = null, int status = 0,int page = 1, int pageSize = 20 )
        {
            try
                {
                IEnumerable<ListClaimDetailsDTO> data = null;
                if (status != -1 && status != 0 && status != 2)
                {
                    return new BaseResponseData() { Code = 400, Message = "Error: Status Claim False" };
                }

                data = insuranceVehicleContext.ClaimDetail.Where(x => x.Status == status).OrderByDescending(x => x.CreatedAt).
                   Select(claim => new ListClaimDetailsDTO()
                   {
                       Id = claim.Id,
                       BodyClaim = claim.BodyClaim,
                       CustomName = claim.Customer.Name,
                       CusEmail = claim.Customer.Email,
                       CusPhone = claim.Customer.PhoneNumber,
                       InsuranceId = claim.InsuranceId,
                       BillOfSaleId = claim.Insurance.BillOfSaleId,
                       ClaimStatus = claim.Status,
                       CreateAt = claim.CreatedAt.Date,

                       Billcount = claim.BillCompensation.Count(),
                       MaxRefundQuantity = claim.Insurance.BillOfSale.Insurance.MaxRefundQuantity

                   });
                if (timeFrom != null && timeTo != null)
                {
                    data = data.Where(x => x.CreateAt >= timeFrom.Value.Date && x.CreateAt <= timeTo.Value.Date).AsQueryable();
                }
                if (valueSearch != "" && valueSearch != null)
                {
                    var search = string.IsNullOrEmpty(valueSearch) ? "" : valueSearch.Trim().ToLower();
                    data =  data.Where(x => x.CustomName.ToLower().Contains(search) || x.CusPhone.ToLower().Contains(search) || x.CusEmail.ToLower().Contains(search));
                }
                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
               
               
                return new BaseResponseData() { Code = 200, Data = data, Message = "Success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }

        }

        [HttpGet("getAllImg/{claimID}")]
        public BaseResponseData getAllImg(int ClaimId)
        {
            try
            {
                var ImgClaim = insuranceVehicleContext.ImageInClaim.Where(claim => claim.ClaimDetailId == ClaimId).AsQueryable();
                return new BaseResponseData { Code = 200, Data = ImgClaim, Message = "Success" };

            }
            catch (Exception e)
            {
                return new BaseResponseData { Code = 500, Message = "Error: " + e.Message.ToString() };

            }
        }

        [HttpPut("dejected")]
        public BaseResponseData deleteClaim([FromBody] EmailRequest email)
        {
            try
            {

                var ClaimToRejected = insuranceVehicleContext.ClaimDetail.Where(claim => claim.Id == email.claimId).FirstOrDefault();
                ClaimToRejected.Status = -1;
                insuranceVehicleContext.SaveChanges();

                var cus = insuranceVehicleContext.Customer.Where(x => x.Id.Equals(ClaimToRejected.CustomerId)).Select(cus => new CustomerDTO { 
                    Email = cus.Email,
                    Name = cus.Name
                } ).FirstOrDefault();
                var sendStatus = Mailer.Send(cus.Email, email.subject, email.body);
                return new BaseResponseData() { Code = 200, Data = cus ,  Message = "Dejected Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error" + e.Message };
            }
        }
        [HttpPost("createBill")]
        public BaseResponseEmptyData postCreateBillConpensation([FromBody] BaseRequestEntity entity)
        {
            try
            {
                var Searchclaim = insuranceVehicleContext.ClaimDetail.Where(x => x.Id == entity.EntityId)
                    .Select(claim => new ClaimDetailsDTO()
                {
                    ClaimId = claim.Id,
                    BodyClaim = claim.BodyClaim,
                    CustomName = claim.Customer.Name,
                    CusEmail = claim.Customer.Email,
                    CusPhone = claim.Customer.PhoneNumber,
                    InsuranceId = claim.InsuranceId,
                    BillOfSaleId = claim.Insurance.BillOfSaleId,
                    ClaimStatus = claim.Status,
                    CreateAt = claim.CreatedAt.Date,
                    CusID = claim.CustomerId,
                    InsuranceSoldid =  claim.Insurance.Id,
                    VegicleID = claim.Insurance.BillOfSale.VerhicleId,
                    Billcount = claim.BillCompensation.Count(),
                    MaxRefundQuantity = claim.Insurance.BillOfSale.Insurance.MaxRefundQuantity

                }).FirstOrDefault();

                var Billcount = Searchclaim.Billcount;
                if(Searchclaim.InsuranceId == null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Claim Not Found Insurance" };
                }
                var MaxRefundQuantity = Searchclaim.MaxRefundQuantity;

                if(Billcount >= MaxRefundQuantity)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Claim Not Found"};
                }

                var BillCompensationToAdd = new BillCompensation()
                {
                    AdminId = entity.UserId,
                    Reason = entity.Reason,
                    TotalValue = entity.Total,
                    CustomerId = Searchclaim.CusID,
                    VerhicleId = Searchclaim.VegicleID,
                    CreatedAt = DateTime.Now.Date,
                    InsuranceSoldId = Searchclaim.InsuranceSoldid,
                    ClaimDetailId = Searchclaim.ClaimId,
                    PaymentStatus = 1
                };
                
                var ClaimStastusUpdate = insuranceVehicleContext.ClaimDetail.Where(x => x.Id == entity.EntityId).FirstOrDefault();
                insuranceVehicleContext.BillCompensation.Add(BillCompensationToAdd);
                ClaimStastusUpdate.Status = 1;
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Success" };

                }
            catch(Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error" + e.Message };
            }
        }


    }
}   