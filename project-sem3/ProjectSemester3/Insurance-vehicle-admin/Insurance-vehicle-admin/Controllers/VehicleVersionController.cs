﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class VehicleVersionController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public VehicleVersionController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        //GET All Vehicle Version of Vehicle Model
        [HttpGet("ModelId={id}")]
        public BaseResponseData getAllVersionByModel(int id)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vv => vv.ModelId == id).Select(vm => new VehicleVersionDTO()
                {
                    Id = vm.Id,
                    Name = vm.Name,
                    ModelId = vm.ModelId
                })
                    .AsQueryable();

                return new BaseResponseData() { Code = 200, Data = data, Message = "Get All Vehicle Version By Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //GET Vehicle Version By Name
        [HttpGet("q={name}")]
        public BaseResponseData searchVehicleVersionByName(string name)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vv => EF.Functions.Like(vv.Name.ToLower(), '%' + name.ToLower() + '%'))
                    .Select(vv => new VehicleVersionDTO()
                    {
                        Id = vv.Id,
                        Name = vv.Name,
                        ModelId = vv.ModelId
                    })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Search Vehicle Version Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //GET Vehicle Version By Id
        [HttpGet("{id}")]
        public BaseResponseData getVehicleVersionById(int id)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vv => vv.Id == id)
                    .Select(vv => new VehicleVersionDTO()
                    {
                        Id = vv.Id,
                        Name = vv.Name,
                        ModelId = vv.ModelId
                    }).AsQueryable();

                return new BaseResponseData() { Code = 200, Data = data, Message = "Get Vehicle Version Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //POST Vehicle Version
        [HttpPost()]
        public BaseResponseEmptyData postVehicleVersion([FromBody] VehicleVersion vv)
        {
            try
            {
                insuranceVehicleContext.VehicleVersion.Add(vv);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Add Vehicle Version Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        //PUT Vehicle Version
        [HttpPut()]
        public BaseResponseEmptyData putVehicleModel([FromBody] VehicleVersion vv)
        {
            try
            {
                var vvToEdit = insuranceVehicleContext.VehicleVersion.Where(vve => vve.Id == vv.Id).FirstOrDefault();

                if (vvToEdit == null)
                {
                    return new BaseResponseEmptyData() { Code = 500, Message = "Edit Vehicle Version Failed! This Version did not exist!" };
                }
                else
                {
                    vvToEdit.Name = vv.Name;
                    vvToEdit.ModelId = vv.ModelId;

                    insuranceVehicleContext.SaveChanges();

                    return new BaseResponseEmptyData() { Code = 200, Message = "Edit Vehicle Version Success!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        //DELETE Vehicle Version
        [HttpDelete("{id}")]
        public BaseResponseEmptyData deleteVehicleVersionl(int id)
        {
            try
            {
                var vvToDelete = insuranceVehicleContext.VehicleVersion.Where(vvd => vvd.Id == id).FirstOrDefault();

                if (vvToDelete == null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Cannot Delete because this Vehicle Version is not Exist!" };
                }
                else
                {
                    insuranceVehicleContext.VehicleVersion.Remove(vvToDelete);
                    insuranceVehicleContext.SaveChanges();

                    return new BaseResponseEmptyData() { Code = 200, Message = "Delete Vehicle Version Success!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error" + e.Message };
            }
        }
    }
}