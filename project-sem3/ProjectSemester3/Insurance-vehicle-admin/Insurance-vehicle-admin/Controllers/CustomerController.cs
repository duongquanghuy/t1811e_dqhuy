﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public CustomerController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("searchCustomer/{searchString}")]
        public BaseResponseData searchCustomer(string searchString, int page = 1, int pageSize = 10)
        {
            try
            {
                var s = searchString;

                var CustomerList = insuranceVehicleContext.Customer.Where(c => c.PhoneNumber.Contains(s) || c.Email.Contains(s) || c.Name.Contains(s) || c.Passport.Contains(s))
                .Select(cus => new Customer
                {
                    Id = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport
                }).AsQueryable();

                if (CustomerList == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                } 
                else
                {
                    var totalItem = CustomerList.Count();
                    CustomerList = CustomerList.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                    var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                    return new BaseResponseData() { Code = 200, Data = CustomerList, Message = "Success search Cusromer", currentPage = page, totalPage = totalPage };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("searchCustomerByPhone")]
        public BaseResponseData getCustomerByPhone(string phone)
        {
            try
            {
                var Customer = insuranceVehicleContext.Customer.Where(
                    c => EF.Functions.Like(c.PhoneNumber.ToLower(), '%' + phone.ToLower() + '%')).
                Select(cus => new Customer
                {
                    Id = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport
                }).AsQueryable();

                if (Customer == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                

                return new BaseResponseData() { Code = 200, Data = Customer, Message = "Success search Cusromer" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("searchCustomerById/{cusId}")]
        public BaseResponseData getCustomerById(string cusId)
        {
            try
            {
                var Customer = insuranceVehicleContext.Customer.Where(
                    c => EF.Functions.Like(c.Id.ToString().ToLower(), '%' + cusId.ToString().ToLower() + '%')).
                Select(cus => new Customer
                {
                    Id = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport
                }).AsQueryable();

                if (Customer == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = Customer, Message = "Success search Cusromer" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("searchCustomerByEmail")]
        public BaseResponseData getCustomerByEmail(string email)
        {
            try
            {
                var Customer = insuranceVehicleContext.Customer.Where(
                    c => EF.Functions.Like(c.Email.ToLower(), '%' + email.ToLower() + '%')).
                Select(cus => new Customer
                {
                    Id = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport
                }).AsQueryable();

                if (Customer == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = Customer, Message = "Success search Cusromer" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        [HttpGet("all")]
        [HttpGet("all/{page}/{pageSize}")]
        public BaseResponseData getAllCustomer(int page = 1, int pageSize = 20)
        {
            try
            {
                var ListCustomer = insuranceVehicleContext.Customer.Select(cus => new Customer
                {
                    Id = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport
                }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalItem = insuranceVehicleContext.Customer.Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                return new BaseResponseData() { Code = 200, Data = ListCustomer, Message = "GET By Status Success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        [HttpPost("add")]
        public BaseResponseEmptyData postCustomer([FromBody] Customer cus)
        {
            try
            {
                var checkEamil = insuranceVehicleContext.Customer.Where(x => x.Email.Equals(cus.Email)).FirstOrDefault();
                if(checkEamil != null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Email already exists" };
                }

                var CustomerToAdd = new Customer()
                {
                    Email = cus.Email,
                    Name = cus.Name,
                    Address = cus.Address,
                    PhoneNumber =  cus.PhoneNumber,
                    Passport = cus.Passport,
                    Password =  cus.Password,
                };
                insuranceVehicleContext.Add(CustomerToAdd);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Success Create Customer" };
            }
            catch (Exception e)
            { return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message }; }
        }
        [HttpPut("edit")]
        public BaseResponseEmptyData putEditCustomer([FromBody] Customer cus)
        {
            try
            {
                var CustomerToEdit = insuranceVehicleContext.Customer.Where(x => x.Id.Equals(cus.Id)).FirstOrDefault();
                var checkEamil = insuranceVehicleContext.Customer.Where(x => x.Email.Equals(cus.Email)).FirstOrDefault();
                if (checkEamil != null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Email already exists" };
                }
                if (CustomerToEdit == null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Customer does not Exist!" };
                }
               
                CustomerToEdit.Name = cus.Name;
                CustomerToEdit.Address = cus.Address;
                CustomerToEdit.PhoneNumber = cus.PhoneNumber;
                CustomerToEdit.Passport = cus.Passport;

                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Success Create Customer" };
            }
            catch (Exception e)
            { return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message }; }
        }
        [HttpGet("searchCustomer")]
        public BaseResponseData getCustomerByEmailFirstOrDefault(string email)
        {
            try
            {
                var Customer = insuranceVehicleContext.Customer.Where(c => c.Email.Equals(email) ).FirstOrDefault();

                if (Customer == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = Customer, Message = "Success search Cusromer" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
    }
}
