﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public ReportController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet]
        public BaseResponseData GetReport(DateTime? timeFrom = null, DateTime? timeTo = null)
        {
            try
            {
                var totalMoneySale = insuranceVehicleContext.InsuranceSold
                    .Sum(IS => IS.BillOfSale.TotalPrice); //Doanh số
                var totalMoneyRefundClaim = insuranceVehicleContext.BillCompensation
                    .Sum(BC => BC.TotalValue); //SỐ tiền đã phải chi trả => Redirect qua trang claim
                var totalCustomer = insuranceVehicleContext.Customer.Count(); //Số lượng khách hàng => Redirect qua trang khách hàng
                //Số bảo hiểm sắp hết hạn
                var totalInsuranceExpiringSoon = insuranceVehicleContext.InsuranceSold.Where(IS => IS.ExpiresOn < DateTime.Now.AddDays(30) && IS.ExpiresOn > DateTime.Now).Count();
                //Số claim chưa xử lý
                var totalClaimNotYetHandle = insuranceVehicleContext.ClaimDetail.Where(claimDetail => claimDetail.Status == 0).Count();
                //Số đơn hàng chưa thanh toán
                var totalBillUnPaid = insuranceVehicleContext.BillOfSale.Where(bill => bill.PaymentStatus == 0).Count();
                return new BaseResponseData() { Code = 200, Data = new ReportDTO() { 
                    TotalMoneySale = totalMoneySale,
                    TotalMoneyClaim = totalMoneyRefundClaim ,
                    TotalClaimNotYetHandle = totalClaimNotYetHandle,
                    TotalCustomer = totalCustomer,
                    TotalInsuranceExpiringSoon = totalInsuranceExpiringSoon,
                    TotalBillUnPaid = totalBillUnPaid
                }, Message = "GET By Status Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
    }
}