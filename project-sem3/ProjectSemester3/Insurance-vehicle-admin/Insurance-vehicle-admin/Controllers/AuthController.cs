﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Insurance_vehicle_admin.Models.DTO;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public LoginController(IConfiguration config, InsuranceVehicleContext dbContext)
        {
            _config = config;
            insuranceVehicleContext = dbContext;
        }

        [Route("login")]
        [HttpPost]
        public BaseResponseData Login([FromBody] LoginModel login)
        {
            BaseResponseData response = null;

            try
            {
                var user = AuthenticateUser(login);
                if (user != null)
                {
                    var expiresDate = DateTime.Now.AddYears(1);
                    var tokenStr = GenerateJSONWebToken(user, expiresDate);
                    string refreshTokenStr = GenerateJSONWebRefreshToken(user, expiresDate);
                    var userAccount = insuranceVehicleContext.Admin.Where(admin => admin.Email.Equals(user.Email)).FirstOrDefault();
                    userAccount.RefreshToken = refreshTokenStr;
                    insuranceVehicleContext.SaveChanges();
                    var data = new LoginSuccessDTO()
                    {
                        AdminId = userAccount.Id.ToString(),
                        AdminName = userAccount.Name,
                        RefreshToken = refreshTokenStr,
                        Token = tokenStr
                    };
                    response = new BaseResponseData()
                    {
                        Code = 200,
                        Message = "Login success",
                        Data = data
                    };
                    /*SetCookie("cus-token", tokenStr, 1800, false);
                    SetCookie("cus-token-expired", DateTime.Now.AddMinutes(30).ToString(), 1800, false);
                    SetCookie("cus-refresh-token", refreshTokenStr, 365 * 24 * 60 * 60, true);
                    SetCookie("cus-name", user.CustomerName, 365 * 24 * 60 * 60, false);*/
                }
            }
            catch (Exception e)
            {
                response = new BaseResponseData()
                {
                    Code = 401,
                    Message = "Login error " + e.Message,
                    Data = null
                };
            }
            return response;

        }

        private LoginModel AuthenticateUser(LoginModel login)
        {
            LoginModel user = null;
            var _user = insuranceVehicleContext.Admin.Where(admin => admin.Email.Equals(login.Email) && admin.Password.Equals(login.Password)).FirstOrDefault();
            if (_user != null)
            {
                user = new LoginModel { Email = _user.Email, Password = _user.Password };
            }
            return user;
        }

        private string GenerateJSONWebToken(LoginModel userInfo, DateTime expires)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
                );
            var endcodeToken = new JwtSecurityTokenHandler().WriteToken(token);
            return endcodeToken;
        }

        private string GenerateJSONWebRefreshToken(LoginModel userInfo, DateTime expires)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:RefreshTokenKey"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Email, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var token = new JwtSecurityToken(
                issuer: _config["Jwt:Issuer"],
                audience: _config["Jwt:Issuer"],
                claims,
                expires: expires,
                signingCredentials: credentials
                );
            var endcodeToken = new JwtSecurityTokenHandler().WriteToken(token);
            return endcodeToken;
        }

        /// <summary>
        /// Lấy token mới 
        /// </summary>
        /// <param name="getTokenModel"></param>
        /// <returns></returns>
        [HttpPost("token")]
        public BaseResponseData Post([FromBody] GetTokenModel getTokenModel)
        {
            BaseResponseData response = null;

            try
            {
                var user = insuranceVehicleContext.Customer
               .Where(user => user.RefreshToken == getTokenModel.RefreshToken && user.Email == getTokenModel.Email)
               .FirstOrDefault();
                if (user != null)
                {
                    var _user = new LoginModel() { Email = user.Email, Password = user.Password };
                    var expiresDate = DateTime.Now.AddHours(0.5);
                    var tokenStr = GenerateJSONWebToken(_user, expiresDate);
                    string refreshTokenStr = GenerateJSONWebRefreshToken(_user, expiresDate);
                    var userAccount = insuranceVehicleContext.Customer.Where(userAccount => userAccount.Email == user.Email).FirstOrDefault();
                    userAccount.RefreshToken = refreshTokenStr;
                    insuranceVehicleContext.SaveChanges();
                    /* SetCookie("cus-token", tokenStr, 1800, false);
                     SetCookie("cus-token-expired", DateTime.Now.AddMinutes(30).ToString(), 1800, false);
                     SetCookie("cus-refresh-token", refreshTokenStr, 365 * 24 * 60 * 60, true);
                     SetCookie("cus-name", user.Name, 365 * 24 * 60 * 60, false);*/
                    var data = new LoginSuccessDTO()
                    {
                        AdminId = userAccount.Id.ToString(),
                        AdminName = userAccount.Name,
                        RefreshToken = refreshTokenStr,
                        Token = tokenStr,
                    };
                    response = new BaseResponseData()
                    {
                        Code = 200,
                        Message = "Get token success",
                        Data = data
                    };
                }
            }
            catch (Exception e)
            {
                response = new BaseResponseData()
                {
                    Code = 401,
                    Message = "Get token error " + e.Message,
                    Data = null
                };
            }
            return response;
        }



        /// <summary>
        /// Test lấy email từ jwt
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost("GetEmailFromJWT")]
        public string GetEmailFromJWT()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;
            IList<Claim> claims = identity.Claims.ToList();
            var user = claims[0].Value;
            return "Hello " + user;
        }

        /// <summary>
        /// Hàm tạo cookie ở client
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expireTime">Giây</param>
        /// <param name="isHttpOnly"></param>
        public void SetCookie(string key, string value, int? expireTime, bool isHttpOnly)
        {
            CookieOptions option = new CookieOptions();
            option.HttpOnly = isHttpOnly;
            option.Domain = "localhost:44377";
            option.Path = "/";
            option.Secure = false;
            if (expireTime.HasValue)
                option.Expires = DateTime.Now.AddYears(expireTime.Value);
            else
                option.Expires = DateTime.Now.AddSeconds(10);

            Response.Cookies.Append(key, value, option);
        }

        [Authorize]
        [HttpPost("GetvalueTest")]
        public ActionResult<IEnumerable<string>> Get()
        {
            string cookieValueFromReq = Request.Cookies["cus-name"];
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// Mã hóa chuỗi có mật khẩu
        /// </summary>
        /// <param name="toEncrypt">Chuỗi cần mã hóa</param>
        /// <returns>Chuỗi đã mã hóa</returns>
        public string Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(_config["Jwt:Key"]);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        /// <summary>
        /// Giản mã
        /// </summary>
        /// <param name="toDecrypt">Chuỗi đã mã hóa</param>
        /// <returns>Chuỗi giản mã</returns>
        public string Decrypt(string toDecrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(_config["Jwt:Key"]);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}