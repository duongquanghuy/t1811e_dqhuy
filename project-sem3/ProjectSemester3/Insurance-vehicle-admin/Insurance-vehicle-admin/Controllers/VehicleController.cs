using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public VehicleController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("getVehicleByCustomerId/{cusId}")]
        public BaseResponseData getVehicleByCustomerId(Guid cusId)
        {
            try
            {
                var vehicleList = insuranceVehicleContext.Vehicle.Where(
                    c => c.CustomerId == cusId).
                Select(vec => new VehicleDTO
                {
                    Id = vec.Id,
                    BodyNumber = vec.BodyNumber,
                    EngineNumber = vec.EngineNumber,
                    PurchaseDate = vec.PurchaseDate,
                    VersionId = vec.VersionId,
                    ModelId = vec.ModelId,
                    LicensePlates = vec.LicensePlates,
                    VersionName = vec.Version.Name,
                    ModelName = vec.Model.Name
                }).AsQueryable();

                if (vehicleList == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = vehicleList, Message = "Get Vehicle List Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("getVehicleById/{vehicleId}")]
        public BaseResponseData getVehicleById(int vehicleId)
        {
            try
            {
                var vec = insuranceVehicleContext.Vehicle.Where(
                    c => c.Id == vehicleId).
                Select(vec => new VehicleDTO
                {
                    Id = vec.Id,
                    BodyNumber = vec.BodyNumber,
                    EngineNumber = vec.EngineNumber,
                    PurchaseDate = vec.PurchaseDate,
                    VersionId = vec.VersionId,
                    ModelId = vec.ModelId,
                    LicensePlates = vec.LicensePlates,
                    VersionName = vec.Version.Name,
                    ModelName = vec.Model.Name
                }).FirstOrDefault();

                if (vec == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = vec, Message = "Get Vehicle Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        
        [HttpGet("{id}")]
        public BaseResponseData GetById(int id)
        {
            try
            {
                var vehicleDTO  = new VehicleDTO();
                var vehicle = insuranceVehicleContext.Vehicle.Where(vehicle => vehicle.Id == id ).FirstOrDefault();
                var price = insuranceVehicleContext.PriceListVehicleNew.Where(price => price.ModelId == vehicle.ModelId && price.VersionId == vehicle.VersionId).FirstOrDefault().Value;
                var versionName = insuranceVehicleContext.VehicleVersion.Where(version => version.Id == vehicle.VersionId).FirstOrDefault().Name;
                var ModelName = insuranceVehicleContext.VehicleModel.Where(model => model.Id == vehicle.ModelId).FirstOrDefault().Name;

                vehicleDTO.Value = price;
                vehicleDTO.VehicleId = vehicle.Id;
                vehicleDTO.BodyNumber = vehicle.BodyNumber;
                vehicleDTO.LicensePlates = vehicle.LicensePlates;
                vehicleDTO.PurchaseDate = vehicle.PurchaseDate;
                vehicleDTO.Version = versionName;
                vehicleDTO.Model = ModelName;
                return new BaseResponseData() { Code = 200, Data = vehicleDTO, Message = "Get success" };
            }
            catch(Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPost("addCustomerVehicle")]
        public BaseResponseEmptyData addCustomerVehicle(Vehicle vec)
        {
            try
            {
                var data = new Vehicle()
                {
                    CustomerId = vec.CustomerId,
                    PurchaseDate = vec.PurchaseDate.Date,
                    BodyNumber = vec.BodyNumber,
                    EngineNumber = vec.EngineNumber,
                    LicensePlates = vec.LicensePlates,
                    ModelId = vec.ModelId,
                    VersionId = vec.VersionId
                };

                insuranceVehicleContext.Vehicle.Add(data);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Add Vehicle Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("versionByVehicleModel/{vehicleModelId}")]
        public BaseResponseData getVersionByVehicleModel(int vehicleModelId)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vs => vs.ModelId == vehicleModelId)
                    .Select(vs => new VehicleVersionDTO() { Id = vs.Id, Name = vs.Name })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("searchVehicleModelByName/{name}")]
        public BaseResponseData searchVehicleModelByName(string name)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Where(vm => EF.Functions.Like(vm.Name.ToLower(), '%' + name.ToLower() + '%'))
                    .Select(vm => new VehicleModelDTO()
                    {
                        Id = vm.Id,
                        Name = vm.Name,
                        VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                        {
                            Id = vv.Id,
                            Name = vv.Name
                        })
                    }).AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}