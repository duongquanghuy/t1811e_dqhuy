﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Query.Internal;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public InsuranceController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("all")]
        [HttpGet("all/{page}/{pageSize}")]
        public BaseResponseData getAllInsurance(int page = 1 , int pageSize = 10)
        {

            try
            {
                //Parse data qua DTO để tạo object trả về dữ liệu
                var data = insuranceVehicleContext.Insurance.Select(insurance => new InsuranceDTO()
                {
                    Id = insurance.Id,
                    Name = insurance.InsuranceName,
                    PricePercentByCarValue = insurance.PricePercentByCarValue,
                    CreatedAt = insurance.CreatedAt,
                    DurationMonth = insurance.DurationMonthUnit,
                    MaxRefundQuantity = insurance.MaxRefundQuantity,
                    MaxValueRefund = insurance.MaxValueRefund,
                    MinValueRefund = insurance.MinValueRefund,
                })
                   .Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();

                var totalItem = insuranceVehicleContext.Insurance.Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpPost("add")]
        public BaseResponseEmptyData postAddInsurance([FromBody]Insurance insurance)
        {
            try {
                var InsuranceToAdd = new Insurance()
                {
                    InsuranceName = insurance.InsuranceName,
                    PricePercentByCarValue = insurance.PricePercentByCarValue,
                    CreatedAt = DateTime.Now,
                    DurationMonthUnit = insurance.DurationMonthUnit,
                    MaxRefundQuantity = insurance.MaxRefundQuantity,
                    MaxValueRefund = insurance.MaxValueRefund,
                    MinValueRefund = insurance.MinValueRefund,
                    Description = insurance.Description
                };
                insuranceVehicleContext.Insurance.Add(InsuranceToAdd);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Success Add Insurance" };
            } catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpDelete("delete/{id}")]
        public BaseResponseEmptyData deletedInsurance(int id)
        {
            try {
                var deletedToInsurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == id).FirstOrDefault();
                insuranceVehicleContext.Insurance.Remove(deletedToInsurance);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Delete Success insurance" };
            
            } catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpPut("edit")]
        public BaseResponseEmptyData putEditInsurance([FromBody] Insurance insurance)
        {
            try {
                var InsuranceToEdit = insuranceVehicleContext.Insurance.Where(ins => ins.Id == insurance.Id).FirstOrDefault();

                InsuranceToEdit.Id = insurance.Id;
                InsuranceToEdit.InsuranceName = insurance.InsuranceName;
                InsuranceToEdit.PricePercentByCarValue = insurance.PricePercentByCarValue;
                InsuranceToEdit.CreatedAt = DateTime.Now;
                InsuranceToEdit.DurationMonthUnit = insurance.DurationMonthUnit;
                InsuranceToEdit.MaxRefundQuantity = insurance.MaxRefundQuantity;
                InsuranceToEdit.MaxValueRefund = insurance.MaxValueRefund;
                InsuranceToEdit.MinValueRefund = insurance.MinValueRefund;
                InsuranceToEdit.Description = insurance.Description;

                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Edit Insurance Success!" };

            }
            catch(Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("search/{id}")]
        public BaseResponseData getInsurance(int id)
        {
            try {
                var Insurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == id).FirstOrDefault();
                return new BaseResponseData() { Code = 200, Data = Insurance, Message = "Search Insurance Success!" };
            }catch(Exception e)
            {
                return new BaseResponseData() { Code = 500,Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getInsuranceBoughtTimes/{id}")]
        public BaseResponseData getInsuranceBoughtTimes(int id)
        {
            try
            {
                var countBought = insuranceVehicleContext.BillOfSale.Where(ins => ins.InsuranceId == id).AsQueryable().Count();

                return new BaseResponseData() { Code = 200, Data = countBought, Message = "Get Count Bought Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getBillOfSaleByInsurance/{id}")]
        public BaseResponseData getBillOfSaleByInsurance(int id)
        {
            try
            {
                var listBill = insuranceVehicleContext.BillOfSale.Where(ins => ins.InsuranceId == id).Select(
                        item => new BillOfSaleDTO
                        {
                            BillId = item.Id,
                            CustomerId = item.CustomerId,
                            CustumerName = item.Customer.Name,
                            TotalPrice = item.TotalPrice,
                            PaymentStatus = item.PaymentStatus,
                            VehicleId = item.Verhicle.Id,
                            LicensePlate = item.Verhicle.LicensePlates
                        }
                    ).AsQueryable();

                if (listBill != null)
                {
                    return new BaseResponseData() { Code = 200, Data = listBill, Message = "Get Bill(s) Of Sale of Insurance Success!" };
                }
                else
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "This Insurance dont have any purchase!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getCustomerBoughtByInsurance/{id}")]
        public BaseResponseData getCustomerBoughtByInsurance(int id)
        {
            try
            {
                var listPeople = insuranceVehicleContext.BillOfSale.Where(ins => ins.InsuranceId == id).Select(
                        people => new CustomerDTO
                        {
                            CusId = people.CustomerId,
                            Name = people.Customer.Name
                        }
                    ).Distinct().AsQueryable();

                if (listPeople != null)
                {
                    return new BaseResponseData() { Code = 200, Data = listPeople, Message = "Get People of Insurance Success!" };
                }
                else
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "This Insurance dont have any purchase!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getCustomerBoughtCountByInsurance/{id}")]
        public BaseResponseData getCustomerBoughtCountByInsurance(int id)
        {
            try
            {
                /*SELECT COUNT(Id), CustomerId
                FROM BillOfSale
                WHERE(BillOfSale.InsuranceId = id)
                GROUP BY CustomerId;*/

                var list = insuranceVehicleContext.BillOfSale.Where(b => b.InsuranceId == id).GroupBy(x => x.CustomerId)
                    .Select(x => new { customerId = x.Key, boughtCount = x.Count() });

                if (list != null)
                {
                    return new BaseResponseData() { Code = 200, Data = list, Message = "Get People of Insurance Success!" };
                }
                else
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "This Insurance dont have any purchase!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}
