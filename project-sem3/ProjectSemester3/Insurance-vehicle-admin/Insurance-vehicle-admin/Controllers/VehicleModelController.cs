﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class VehicleModelController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public VehicleModelController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        //GET All Vehicle Model
        [HttpGet()]
        [HttpGet("{page}/{pageSize}")]
        public BaseResponseData getAllVehicleModel(int page = 1, int pageSize = 20)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Select(vm => new VehicleModelDTO()
                {
                    Id = vm.Id,
                    Name = vm.Name,
                    VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                    {
                        Id = vv.Id,
                        Name = vv.Name
                    })
                })
                    .Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();

                var totalItem = insuranceVehicleContext.Insurance.Count();

                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                return new BaseResponseData() { Code = 200, Data = data, Message = "Get All Vehicle Model Success!", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //GET Vehicle Model By Name
        [HttpGet("q={name}")]
        public BaseResponseData searchVehicleModelByName(string name)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Where(vm => EF.Functions.Like(vm.Name.ToLower(), '%' + name.ToLower() + '%'))
                    .Select(vm => new VehicleModelDTO()
                    {
                        Id = vm.Id,
                        Name = vm.Name,
                        VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                        {
                            Id = vv.Id,
                            Name = vv.Name
                        })
                    })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Search Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //GET Vehicle Model By Id
        [HttpGet("{id}")]
        public BaseResponseData getVehicleModelById(int id)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Where(vm => vm.Id == id)
                    .Select(vm => new VehicleModelDTO()
                    {
                        Id = vm.Id,
                        Name = vm.Name,
                        VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                        {
                            Id = vv.Id,
                            Name = vv.Name
                        })
                    }).AsQueryable();

                return new BaseResponseData() { Code = 200, Data = data, Message = "Get Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //POST Vehicle Model
        [HttpPost()]
        public BaseResponseEmptyData postVehicleModel([FromBody] VehicleModel vm)
        {
            try
            {
                insuranceVehicleContext.VehicleModel.Add(vm);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Add Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        //PUT Vehicle Model
        [HttpPut()]
        public BaseResponseEmptyData putVehicleModel([FromBody] VehicleModel vm)
        {
            try
            {
                var vmToEdit = insuranceVehicleContext.VehicleModel.Where(vme => vme.Id == vm.Id).FirstOrDefault();

                if (vmToEdit == null)
                {
                    return new BaseResponseEmptyData() { Code = 500, Message = "Edit Vehicle Model Failed! This Model did not exist!" };
                }
                else
                {
                    vmToEdit.Name = vm.Name;

                    insuranceVehicleContext.SaveChanges();

                    return new BaseResponseEmptyData() { Code = 200, Message = "Edit Vehicle Model Success!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        //DELETE Vehicle Model
        [HttpDelete("{id}")]
        public BaseResponseEmptyData deleteVehicleModel(int id)
        {
            try
            {
                var vmToDelete = insuranceVehicleContext.VehicleModel.Where(vmd => vmd.Id == id).FirstOrDefault();

                if (vmToDelete == null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Cannot Delete because this Vehicle Model is not Exist!" };
                }
                else
                {
                    insuranceVehicleContext.VehicleModel.Remove(vmToDelete);
                    insuranceVehicleContext.SaveChanges();

                    return new BaseResponseEmptyData() { Code = 200, Message = "Delete Vehicle Model Success!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error" + e.Message };
            }
        }

        //GET Vehicle Version By Model Id
        [HttpGet("getVersion/{vehicleModelId}")]
        public BaseResponseData getVersionByVehicleModel(int vehicleModelId)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vs => vs.ModelId == vehicleModelId)
                    .Select(vs => new VehicleVersionDTO() { Id = vs.Id, Name = vs.Name, ModelId = vs.ModelId })
                    .AsQueryable();

                if (data == null)
                {
                    return new BaseResponseData() { Code = 500, Data = data, Message = "Get Vehicle Model's Version Failed! This Model do not have any Version!" };
                }
                else
                {
                    return new BaseResponseData() { Code = 200, Data = data, Message = "Get Vehicle Model's Version Success!" };
                }
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}