﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class BillOfSaleController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public BillOfSaleController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        /// <summary>
        /// Lấy thông tin các đơn hàng đã mua của hệ thống, fillter theo các trường nếu có
        /// </summary>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="type"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="customerEmail"></param>
        /// <returns></returns>
        [HttpGet("insurance-sold")]
        public BaseResponseData GetInsuranceSoldByType(int page = 1, int pageSize = 10, int type = 0, DateTime? timeFrom = null, DateTime? timeTo = null, string customerEmail = null)
        {
            IEnumerable<InsuranceSoldDTO> data = null;
            try
            {
                if (type == 1) //Nếu muốn lọc theo trạng thái còn nhiều hạn
                {
                    if (timeFrom == null || timeTo == null)
                    {
                        data = insuranceVehicleContext.InsuranceSold.Where(IS => IS.ExpiresOn >= DateTime.Now.AddDays(30)).Select(IS => new InsuranceSoldDTO()
                        {
                            ActivationDate = IS.CreatedAt,
                            CustomerEmail = IS.BillOfSale.Customer.Email,
                            InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                            Price = IS.BillOfSale.TotalPrice,
                            Vehicleid = IS.BillOfSale.VerhicleId,
                            DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                            InsuranceId = IS.BillOfSale.InsuranceId
                        });
                    }
                    if (timeFrom != null && timeTo != null) //Nếu có lọc theo thời gian hết hạn
                    {
                        data = insuranceVehicleContext.InsuranceSold
                            .Where(IS => IS.CreatedAt.Value >= timeFrom && IS.CreatedAt.Value <= timeTo && IS.ExpiresOn >= DateTime.Now.AddDays(30)).OrderBy(IS => IS.ExpiresOn).Select(IS => new InsuranceSoldDTO()
                            {
                                ActivationDate = IS.CreatedAt,
                                CustomerEmail = IS.BillOfSale.Customer.Email,
                                InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                                Price = IS.BillOfSale.TotalPrice,
                                Vehicleid = IS.BillOfSale.VerhicleId,
                                DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                                InsuranceId = IS.BillOfSale.InsuranceId
                            });
                    }
                }
                else if (type == 2) //Nếu muốn lọc theo trạng thái gần hết hạn
                {
                    if (timeFrom == null || timeTo == null)
                    {
                        data = insuranceVehicleContext.InsuranceSold.Where(IS => IS.ExpiresOn < DateTime.Now.AddDays(30) && IS.ExpiresOn > DateTime.Now).Select(IS => new InsuranceSoldDTO()
                        {
                            ActivationDate = IS.CreatedAt,
                            CustomerEmail = IS.BillOfSale.Customer.Email,
                            InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                            Price = IS.BillOfSale.TotalPrice,
                            Vehicleid = IS.BillOfSale.VerhicleId,
                            DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                            InsuranceId = IS.BillOfSale.InsuranceId
                        });
                    }
                    if (timeFrom != null && timeTo != null) //Nếu có lọc theo thời gian hết hạn
                    {
                        data = insuranceVehicleContext.InsuranceSold
                            .Where(IS => IS.CreatedAt.Value >= timeFrom && IS.CreatedAt.Value <= timeTo && IS.ExpiresOn < DateTime.Now.AddDays(30)).Select(IS => new InsuranceSoldDTO()
                            {
                                ActivationDate = IS.CreatedAt,
                                CustomerEmail = IS.BillOfSale.Customer.Email,
                                InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                                Price = IS.BillOfSale.TotalPrice,
                                DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                                InsuranceId = IS.BillOfSale.InsuranceId,
                                Vehicleid = IS.BillOfSale.VerhicleId,
                            });
                    }
                }
                else if (type == 3)//Nếu muốn lọc theo trạng thái đã hết hạn
                {
                    if (timeFrom == null || timeTo == null)
                    {
                        data = insuranceVehicleContext.InsuranceSold.Where(IS => IS.ExpiresOn < DateTime.Now).Select(IS => new InsuranceSoldDTO()
                        {
                            ActivationDate = IS.CreatedAt,
                            CustomerEmail = IS.BillOfSale.Customer.Email,
                            InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                            Price = IS.BillOfSale.TotalPrice,
                            DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                            InsuranceId = IS.BillOfSale.InsuranceId,
                            Vehicleid = IS.BillOfSale.VerhicleId,
                        });
                    }
                    if (timeFrom != null && timeTo != null) //Nếu có lọc theo thời gian hết hạn
                    {
                        data = insuranceVehicleContext.InsuranceSold
                            .Where(IS => IS.CreatedAt.Value >= timeFrom && IS.CreatedAt.Value <= timeTo && IS.ExpiresOn < DateTime.Now).Select(IS => new InsuranceSoldDTO()
                            {
                                ActivationDate = IS.CreatedAt,
                                CustomerEmail = IS.BillOfSale.Customer.Email,
                                InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                                Price = IS.BillOfSale.TotalPrice,
                                DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                                InsuranceId = IS.BillOfSale.InsuranceId,
                                Vehicleid = IS.BillOfSale.VerhicleId,
                            });
                    }
                }
                else //Nếu lấy tất cả
                {
                    if (timeFrom != null && timeTo != null)
                    {
                        data = insuranceVehicleContext.InsuranceSold
                            .Where(IS => IS.CreatedAt.Value >= timeFrom && IS.CreatedAt.Value <= timeTo).Select(IS => new InsuranceSoldDTO()
                            {
                                ActivationDate = IS.CreatedAt,
                                CustomerEmail = IS.BillOfSale.Customer.Email,
                                InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                                Price = IS.BillOfSale.TotalPrice,
                                Vehicleid = IS.BillOfSale.VerhicleId,
                                DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                                InsuranceId = IS.BillOfSale.InsuranceId
                            });
                    }
                    else
                    {
                        data = insuranceVehicleContext.InsuranceSold.Select(IS => new InsuranceSoldDTO()
                        {
                            ActivationDate = IS.CreatedAt,
                            CustomerEmail = IS.BillOfSale.Customer.Email,
                            InsuranceName = IS.BillOfSale.Insurance.InsuranceName,
                            Price = IS.BillOfSale.TotalPrice,
                            Vehicleid = IS.BillOfSale.VerhicleId,
                            DaysRemaining = Math.Ceiling((IS.ExpiresOn - DateTime.Now).TotalDays),
                            InsuranceId = IS.BillOfSale.InsuranceId
                        });
                    }
                }
                var totalItem = data.Count();
                if (type == 0) {
                    data = data.OrderByDescending(isuranceSoldDto => isuranceSoldDto.ActivationDate).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                }
                else
                {
                    data = data.OrderBy(isuranceSoldDto => isuranceSoldDto.DaysRemaining).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                }
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                
                return new BaseResponseData() { Code = 200, Data = data, totalPage = totalPage, currentPage = page, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e };
            }
        }
        [HttpGet("insuranceBill")]
        public BaseResponseData GetInsuranceBillType(int page = 1, int pageSize = 10, int type = 0, DateTime? timeFrom = null, DateTime? timeTo = null, string valueSearch = null)
        {
           
            try
            {
                IEnumerable<BillOfSaleDTO> data = null;
                var today = DateTime.Now.Date;
                data = insuranceVehicleContext.BillOfSale.Where(x => x.PaymentStatus ==type).
                   Select(billOfSale => new BillOfSaleDTO
                   {
                       BillId = billOfSale.Id,
                       CreatedBy = billOfSale.CreatedBy,
                       TotalPrice = billOfSale.TotalPrice,
                       PaymentStatus = billOfSale.PaymentStatus,
                       CreatedAt = billOfSale.CreatedAt,
                       CusEmail = billOfSale.Customer.Email,
                       PhoneNumber = billOfSale.Customer.PhoneNumber,
                       VehicleId = billOfSale.VerhicleId,
                       InsuranceId = billOfSale.InsuranceId,
                       InsuranceName = billOfSale.Insurance.InsuranceName,
                   });
                if (timeFrom != null && timeTo != null)
                {
                    data = data.Where(x => x.CreatedAt.Value.Date >= timeFrom.Value.Date && x.CreatedAt.Value.Date <= timeTo.Value.Date).AsQueryable();
                }
                if (valueSearch != "" && valueSearch != null)
                {
                    var serrchBill = string.IsNullOrEmpty(valueSearch) ? "" : valueSearch.Trim().ToLower();
                    data = data.Where(x => x.CusEmail.ToLower().Contains(serrchBill) || x.PhoneNumber.ToLower().Contains(serrchBill));
                }

                var totalItem = data.Count();
                data = data.OrderByDescending(x => x.CreatedAt).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                return new BaseResponseData() { Code = 200, Data = data, totalPage = totalPage, currentPage = page, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e };
            }
        }


        [HttpGet("allPaid")]
        [HttpGet("allPaid/{page}/{pageSize}")]
        public BaseResponseData getAllBillOfSalePaid(int page = 1, int pageSize = 20)
        {
            try
            {
                var ListAllBillOfSale = insuranceVehicleContext.BillOfSale.Where(bill => bill.PaymentStatus == 1).
                    Select(billOfSale => new BillOfSaleDTO
                    {
                        BillId = billOfSale.Id,
                        CreatedBy = billOfSale.CreatedBy,
                        TotalPrice = billOfSale.TotalPrice,
                        PaymentStatus = billOfSale.PaymentStatus,
                        CreatedAt = billOfSale.CreatedAt,
                        CustumerName = billOfSale.Customer.Name,
                        ModelName = billOfSale.Verhicle.Model.Name,
                        VersionName = billOfSale.Verhicle.Version.Name,
                        PhoneNumber = billOfSale.Customer.PhoneNumber,
                    }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();


                var totalItem = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.PaymentStatus == 1).Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = ListAllBillOfSale, Message = "GET By Status Success", totalPage = totalPage, currentPage = page };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        [HttpGet("allUnpaid")]
        [HttpGet("allUnpaid/{page}/{pageSize}")]
        public BaseResponseData getAllBillOfSaleUnpaid(int page = 1, int pageSize = 20)
        {
            try
            {
                var ListAllBillOfSale = insuranceVehicleContext.BillOfSale.Where(bill => bill.PaymentStatus == 0).
                    Select(billOfSale => new BillOfSaleDTO
                    {
                        BillId = billOfSale.Id,
                        CreatedBy = billOfSale.CreatedBy,
                        TotalPrice = billOfSale.TotalPrice,
                        PaymentStatus = billOfSale.PaymentStatus,
                        CreatedAt = billOfSale.CreatedAt,
                        CustumerName = billOfSale.Customer.Name,
                        ModelName = billOfSale.Verhicle.Model.Name,
                        VersionName = billOfSale.Verhicle.Version.Name,
                        PhoneNumber = billOfSale.Customer.PhoneNumber,
                    }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();


                var totalItem = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.PaymentStatus == 0).Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = ListAllBillOfSale, Message = "GET By Status Success", totalPage = totalPage, currentPage = page };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        [HttpGet("bill/{BillId}")]
        public BaseResponseData getBillOfSale(int BillId)
        {
            try
            {

                var BillOfSale = insuranceVehicleContext.BillOfSale.Where(bill => bill.Id.Equals(BillId)).
                    Select(billOfSale => new BillOfSaleDTO
                    {
                        BillId = billOfSale.Id,
                        CreatedBy = billOfSale.CreatedBy,
                        CustomerId = billOfSale.CustomerId,
                        InsuranceDurationMonthly = billOfSale.InsuranceDurationMonthly,
                        TotalPrice = billOfSale.TotalPrice,
                        PaymentStatus = billOfSale.PaymentStatus,
                        CreatedAt = billOfSale.CreatedAt,
                        InsuranceId = billOfSale.InsuranceId,
                        CustumerName = billOfSale.Customer.Name,
                        PricePercentByCarValue = billOfSale.Insurance.PricePercentByCarValue,
                        ModelName = billOfSale.Verhicle.Model.Name,
                        VersionName = billOfSale.Verhicle.Version.Name,
                        PhoneNumber = billOfSale.Customer.PhoneNumber,
                    }).FirstOrDefault();



                return new BaseResponseData() { Code = 200, Data = BillOfSale, Message = "GET By Status Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }
        [HttpGet("billActive/{page}/{pageSize}")]
        [HttpGet("billActive")]
        public BaseResponseData getInsuranceSoldActive(int page = 1, int pageSize = 20)
        {
            try
            {

                var data = insuranceVehicleContext.InsuranceSold.Where(bill => bill.ExpiresOn.Date > DateTime.Now.Date).
                    Select(bill => new BillOfSaleActiveDTO()
                    {
                        InsuranceSoldId = bill.Id,
                        EmailCustomer = bill.BillOfSale.Customer.Email,
                        InsuranceId = bill.BillOfSale.InsuranceId,
                        ExpiresOn = bill.ExpiresOn,
                        TotalPrice = bill.BillOfSale.TotalPrice,
                        VersionName = bill.BillOfSale.Verhicle.Version.Name,
                        ModelName = bill.BillOfSale.Verhicle.Model.Name,
                        LicensePlates = bill.BillOfSale.Verhicle.LicensePlates,
                        CreatedAt = bill.BillOfSale.CreatedAt,
                        PhoneNumber = bill.BillOfSale.Customer.PhoneNumber,
                        /*MaxValueRefund = bill.BillOfSale.Insurance.MaxValueRefund,
                        MaxRefundQuantity = bill.BillOfSale.Insurance.MaxRefundQuantity,
                        DurationMonthUnit = bill.BillOfSale.Insurance.DurationMonthUnit,
                        MinRefundQuantity = bill.BillOfSale.Insurance.MinValueRefund,*/
                    }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalItem = insuranceVehicleContext.InsuranceSold.Where(bill => bill.ExpiresOn.Date > DateTime.Now.Date).Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success", currentPage = page, totalPage = totalPage };



            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("billActiveExpires/{page}/{pageSize}")]
        [HttpGet("billActiveExpires")]
        public BaseResponseData getInsuranceSoldActiveExpires(DateTime dateTime, int page = 1, int pageSize = 20)
        {
            try
            {

                var datime = dateTime.Date;
                if (dateTime.Date < DateTime.Now.Date)
                {
                    return new BaseResponseData() { Code = 300, Message = "Error: Datime Not Found" };
                }
                var data = insuranceVehicleContext.InsuranceSold.Where(bill => bill.ExpiresOn.Date <= datime).
                    Select(bill => new BillOfSaleActiveDTO()
                    {
                        InsuranceSoldId = bill.Id,
                        EmailCustomer = bill.BillOfSale.Customer.Email,
                        InsuranceId = bill.BillOfSale.InsuranceId,
                        ExpiresOn = bill.ExpiresOn,
                        TotalPrice = bill.BillOfSale.TotalPrice,
                        VersionName = bill.BillOfSale.Verhicle.Version.Name,
                        ModelName = bill.BillOfSale.Verhicle.Model.Name,
                        LicensePlates = bill.BillOfSale.Verhicle.LicensePlates,
                        CreatedAt = bill.BillOfSale.CreatedAt,
                        PhoneNumber = bill.BillOfSale.Customer.PhoneNumber,
                        /*MaxValueRefund = bill.BillOfSale.Insurance.MaxValueRefund,
                        MaxRefundQuantity = bill.BillOfSale.Insurance.MaxRefundQuantity,
                        DurationMonthUnit = bill.BillOfSale.Insurance.DurationMonthUnit,
                        MinRefundQuantity = bill.BillOfSale.Insurance.MinValueRefund,*/
                    }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalItem = insuranceVehicleContext.InsuranceSold.Where(bill => bill.ExpiresOn.Date <= datime).Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                if (data == null)
                {
                    return new BaseResponseData() { Code = 500, Data = null, Message = "Not found" };
                }
                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success", currentPage = page, totalPage = totalPage };



            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpDelete("delete")]
        public BaseResponseEmptyData deleteBillOfSale([FromBody] BaseRequestEntity baseRequest)
        {
            try
            {
                if (baseRequest.EntityId <= 0)
                    return new BaseResponseEmptyData() { Code = 500, Message = "Not a valid bill of sale Id " };

                var BillToDelete = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.Id == baseRequest.EntityId).FirstOrDefault();
                if (BillToDelete == null)
                {
                    return new BaseResponseEmptyData() { Code = 400, Message = "Not found" };
                }
                if (BillToDelete.PaymentStatus == 1)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Bill Cannot be remove because it was paid already!" };
                }
                insuranceVehicleContext.BillOfSale.Remove(BillToDelete);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "DELETE Success" };

            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message };
            }
        }
        [HttpGet("searchBill/{BillId}")]
        public BaseResponseData getBillOfSaleByID(int BillId)
        {
            try {
                var BillOfSale = insuranceVehicleContext.BillOfSale.Where(x => x.Id == BillId).FirstOrDefault();
                if(BillOfSale == null)
                {
                    return new BaseResponseData() { Code = 300, Data = null, Message = "Not Found" };
                }
                return new BaseResponseData() { Code = 200, Data = BillOfSale, Message = "Success Search" };
                
            }catch(Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message };
            }
        }

        [HttpPost("postBillOfSale")]
        public BaseResponseEmptyData postBillOfSale([FromBody] CalculatePriceOfBill bill)
        {
            try
            {
                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if (bill.quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if (bill.quantityYearUsed < 10 && bill.quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }
                else if (bill.quantityYearUsed < 15 && bill.quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }
                //tính giá của bill
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == bill.modelId && pv.VersionId == bill.vesionId).FirstOrDefault().Value;

                var insurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == bill.InsuranceId)
                    .FirstOrDefault();
                var priceInsuranceByCar = insurance.PricePercentByCarValue;

                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= bill.quantityYearUsed && calc.MaxYear >= bill.quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)bill.quantity);

                var InsuranceDurationMonthlyBill = insurance.DurationMonthUnit * bill.quantity;
                BillOfSale billOfSale = new BillOfSale()
                {
                    CustomerId = bill.CustomerId,
                    VerhicleId = bill.VerhicleId,
                    InsuranceDurationMonthly = InsuranceDurationMonthlyBill,
                    InsuranceId = bill.InsuranceId,
                    TotalPrice = totalPrice,
                    PaymentStatus = 0,
                };
                insuranceVehicleContext.BillOfSale.Add(billOfSale);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "POST success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("priceInsurance")]
        public BaseResponseData getPriceInsurance(int insuranceId, int modelId, int vesionId, int quantityYearUsed, int quantity)
        {
            try
            {
                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if (quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if (quantityYearUsed < 10 && quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }
                else if (quantityYearUsed < 15 && quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == modelId && pv.VersionId == vesionId).FirstOrDefault().Value;
                var priceInsuranceByCar = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == insuranceId)
                    .FirstOrDefault().PricePercentByCarValue;
                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= quantityYearUsed && calc.MaxYear >= quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)quantity);
                return new BaseResponseData() { Code = 200, Data = totalPrice, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getBillOfSaleByCustomerId/{customerId}")]
        public BaseResponseData getBillOfSaleByCustomerId(Guid customerId)
        {
            try
            {
                var billList = insuranceVehicleContext.BillOfSale.Where(x => x.CustomerId == customerId)
                    .Select(bl => new BillOfSaleDTO
                    {
                        BillId = bl.Id,
                        VehicleId = bl.VerhicleId,
                        InsuranceId = bl.InsuranceId,
                        InsuranceName = bl.Insurance.InsuranceName,
                        VersionName = bl.Verhicle.Version.Name,
                        LicensePlate = bl.Verhicle.LicensePlates,
                        TotalPrice = bl.TotalPrice,
                        PaymentStatus = bl.PaymentStatus,
                        CreatedAt = bl.CreatedAt,
                    })
                    .AsQueryable();
                if (billList == null)
                {
                    return new BaseResponseData() { Code = 300, Data = null, Message = "Not Found" };
                }
                return new BaseResponseData() { Code = 200, Data = billList, Message = "Success Search" };

            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message };
            }
        }
        [HttpPost("postBillOfSaleOffline")]
        public BaseResponseEmptyData postBillOfSalePaymentOffline([FromBody] CalculatePriceOfBill bill)
        {
            try
            {
                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if (bill.quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if (bill.quantityYearUsed < 10 && bill.quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }
                else if (bill.quantityYearUsed < 15 && bill.quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }
                //tính giá của bill
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == bill.modelId && pv.VersionId == bill.vesionId).FirstOrDefault().Value;

                var insurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == bill.InsuranceId)
                    .FirstOrDefault();
                var priceInsuranceByCar = insurance.PricePercentByCarValue;

                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= bill.quantityYearUsed && calc.MaxYear >= bill.quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)bill.quantity);

                var InsuranceDurationMonthlyBill = insurance.DurationMonthUnit * bill.quantity;
                BillOfSale billOfSale = new BillOfSale()
                {
                    CustomerId = bill.CustomerId,
                    VerhicleId = bill.VerhicleId,
                    InsuranceDurationMonthly = InsuranceDurationMonthlyBill,
                    InsuranceId = bill.InsuranceId,
                    TotalPrice = totalPrice,
                    PaymentStatus = 1
                };
                insuranceVehicleContext.BillOfSale.Add(billOfSale);
                insuranceVehicleContext.SaveChanges();
                
                InsuranceSold insuranceSoldnew = new InsuranceSold()
                {
                    CreatedAt = DateTime.Now.Date,
                    ExpiresOn = DateTime.Now.Date.AddMonths(billOfSale.InsuranceDurationMonthly),
                    BillOfSaleId = billOfSale.Id,
                };

                insuranceVehicleContext.InsuranceSold.Add(insuranceSoldnew);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "POST success Off" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}
