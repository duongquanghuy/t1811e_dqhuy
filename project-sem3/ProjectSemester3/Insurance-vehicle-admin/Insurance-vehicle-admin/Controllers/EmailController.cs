﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Insurance_vehicle_admin.Models.EF;
using Insurance_vehicle_admin.Models.BaseRessponse;
using Insurance_vehicle_admin.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Insurance_vehicle_admin.Models.Request;
using System.Net.Mail;
using System.Net;

namespace Insurance_vehicle_admin.Controllers
{
    [Route("admin/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        EmailerService Mailer { get; }
        public EmailController(InsuranceVehicleContext dbContext, EmailerService mailer)
        {
            insuranceVehicleContext = dbContext;
            Mailer = mailer;
        }

        [HttpPost("sendEmailClaim")]
        public BaseResponseData getSendEmail([FromBody] EmailRequest email)
        {
            SmtpClient smtp = new SmtpClient();
            try
            {
                var emailTosend = insuranceVehicleContext.ClaimDetail.Where(x => x.Id == email.claimId).

                    Select(claim => new CustomerDTO
                    {
                        Name = claim.Customer.Name,
                        Email = claim.Customer.Email,
                      
                    }).FirstOrDefault();
                var ClaimStatus = insuranceVehicleContext.ClaimDetail.Where(x => x.Id == email.claimId).FirstOrDefault();
              
                if (emailTosend == null)
                {
                    return new BaseResponseData() { Code = 300, Message = "Email not Found" };
                }
                var sendStatus = Mailer.Send(emailTosend.Email, email.subject, email.body);
                ClaimStatus.Status = 2;
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseData() { Code = 200, Data = emailTosend , Message = "Email Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error :"+ e.Message };
            }
        }
        [HttpPost("sendEmailAllCustomer")]
        public BaseResponseData postAllSendEmail([FromBody] EmailRequest email)
        {
            SmtpClient smtp = new SmtpClient();
            var List = new List<CustomerDTO>();
            try
            {
                var emailTosend = insuranceVehicleContext.Customer.

                    Select(claim => new CustomerDTO
                    {
                        Name = claim.Name,
                        Email = claim.Email,

                    }).ToList();
                foreach(var cus in emailTosend)
                {
                    try {
                        var mailReceived = cus.Email;
                        //ĐỊA CHỈ SMTP Server
                        smtp.Host = "smtp.gmail.com";
                        //Cổng SMTP
                        smtp.Port = 587;
                        //SMTP yêu cầu mã hóa dữ liệu theo SSL
                        smtp.EnableSsl = true;
                        //UserName và Password của mail
                        smtp.Credentials = new NetworkCredential("vehicleinsurance.company@gmail.com", "12345678@Abc");

                        //Tham số lần lượt là địa chỉ người gửi, người nhận, tiêu đề và nội dung thư
                        smtp.Send("vehicleinsurance.company@gmail.com", mailReceived, email.subject, email.body);

                    } catch(Exception e)
                    {
                        var customer = new CustomerDTO { Name = cus.Name, Email = cus.Email };
                        List.Add(customer);
                    }
                  
                }
              

                return new BaseResponseData() { Code = 200, Data = List, Message = "Email Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error :" + e.Message };
            }
        }
        [HttpPost("sendEmailCustomer")]
        public BaseResponseData postSendEmailCustomer([FromBody] EmailRequest email)
        {
            SmtpClient smtp = new SmtpClient();
            try
            {
                var emailTosend = insuranceVehicleContext.Customer.Where(x => x.Id == email.cusId).

                    Select(claim => new CustomerDTO
                    {
                        Name = claim.Name,
                        Email = claim.Email,

                    }).FirstOrDefault();
                if (emailTosend == null)
                {
                    return new BaseResponseData() { Code = 300, Message = "Email not Found" };
                }
                email.mailReceived = emailTosend.Email;
                //ĐỊA CHỈ SMTP Server
                smtp.Host = "smtp.gmail.com";
                //Cổng SMTP
                smtp.Port = 587;
                //SMTP yêu cầu mã hóa dữ liệu theo SSL
                smtp.EnableSsl = true;
                //UserName và Password của mail
                smtp.Credentials = new NetworkCredential("vehicleinsurance.company@gmail.com", "12345678@Abc");

                //Tham số lần lượt là địa chỉ người gửi, người nhận, tiêu đề và nội dung thư
                smtp.Send("vehicleinsurance.company@gmail.com", email.mailReceived, email.subject, email.body);

                return new BaseResponseData() { Code = 200, Data = emailTosend, Message = "Email Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error :" + e.Message };
            }
        }

        /// <summary>
        /// Gửi mail có chữ ký
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpPost("commonSendMail")]
        public BaseResponseData PostCommonEmail([FromBody] CommonEmail email)
        {
            try
            {
                var sendStatus = Mailer.Send(email.EmailAddress, email.Subject, email.Body);
                return new BaseResponseData() { Code = 200, Data = sendStatus, Message = "Email Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error :" + e.Message };
            }
        }
    }
}
