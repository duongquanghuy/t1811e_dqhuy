﻿using System;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;

namespace Insurance_vehicle_admin
{
    public class EmailerService
    {
        private readonly ILogger<EmailerService> Logger;
        IConfigurationSection SMTP { get; }

        public EmailerService(
            IConfiguration config,
            ILogger<EmailerService> logger
        )
        {
            Logger = logger;

            try
            {
                // Used to access appsettings.json
                SMTP = config.GetSection("smtp");
            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex, ex.Message);
            }
        }

        public bool Send(string toMail, string subject, string body)
        {
            try
            {
                // Composing the whole email
                var message = new MimeMessage();
                message.From.Add(new MailboxAddress(SMTP["FromName"], SMTP["From"]));
                message.To.Add(new MailboxAddress(toMail, toMail));
                message.Subject = subject;

                var headers = new HeaderId[] { HeaderId.From, HeaderId.Subject, HeaderId.To };

                var builder = new BodyBuilder();
                builder.TextBody = body;
                builder.HtmlBody = body +"<br/>" + @"<table data-mysignature-date='2020-07-26T14:14:06.585Z' data-mysignature-is-paid='0' width='500' cellspacing='0' cellpadding='0' border='0'>
   <tr>
      <td>
         <table cellspacing = '0' cellpadding='0' border='0'>
            <tr>
               <td valign = 'top' width='100' style='padding:0 8px 0 0;vertical-align: top;'> <a target = '_blank' href='https://mysignature.io/?utm_source&#x3D;logo'><img alt = 'created with MySignature.io' width='100' style='width:100px;moz-border-radius:10%;khtml-border-radius:10%;o-border-radius:10%;webkit-border-radius:10%;ms-border-radius:10%;border-radius:10%;' src='https://i.imgur.com/ls47vnK.jpg' /></a> </td>
               <td style = 'font-size:1em;padding:0 15px 0 8px;vertical-align: top;' valign='top'>
                  <table cellspacing = '0' cellpadding='0' border='0' style='line-height: 1.4;font-family:Verdana, Geneva, sans-serif;font-size:90%;color: #000001;'>
                     <tr>
                        <td>
                           <div style = 'font: 1.2em Verdana, Geneva, sans-serif;color:#000001;' >
                           </div> </td> </tr> <tr> <td style='padding: 4px 0;'> 
                           <div style = 'color:#000001;font-family:Verdana, Geneva, sans-serif;' > Admin | Vehicle Insurance</div>
                        </td>
                     </tr>
                     <tr>
                        <td> <span style = 'font-family:Verdana, Geneva, sans-serif;color:#91AE6D;' > phone:&nbsp;</span> <span><a style = 'text-decoration:none;font-family:Verdana, Geneva, sans-serif;color:#000001;' href='tel:(+84)8888-9999'>tel:(+84)8888-9999</a></span> </td>
                     </tr>
                     <tr>
                        <td> <span style = 'font-family:Verdana, Geneva, sans-serif;color:#91AE6D;' > site:&nbsp;</span> <span style = 'font-family:Verdana, Geneva, sans-serif;' ><a href='http://Aptech.fpt.edu.vn' style='text-decoration: none;color:#000001;' target='_blank'>Aptech.fpt.edu.vn</a></span> </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
      </td>
   </tr>
</table>
<table width = '500' cellspacing='0' cellpadding='0' border='0'>
   <tr>
      <td class='s_pixel' colspan='2'> <a href = 'https://mysignature.io/editor/?utm_source=freepixel' ><img src='https://img.mysignature.io/pixel/964468/signature/181512'></a> </td>
   </tr>
   <tr>
      <td style = 'line-height: 1px;' ></td> </tr></table><table style='margin-top: 15px;color: gray;font-family: Arial;line-height: 1.3;font-size:1em;width: 500px' cellspacing='0' cellpadding='0' border='0'> 
   <tr>
      <td style = 'font-family:;font-size:75%;'>Car Insurance is an auto insurance plan that offers you the best service, together with many attractive benefits and competitive premium rates.</td>
   </tr>
</table>";

                message.Body = builder.ToMessageBody();
                message.Prepare(EncodingConstraint.SevenBit);

                // Sending the email
                using (var client = new SmtpClient())
                {
                    // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                    client.ServerCertificateValidationCallback = (s, c, h, e) =>
                    {
                        Console.WriteLine(e);
                        return true;
                    };

                    client.Connect(SMTP["Url"], Convert.ToInt32(SMTP["Port"]), SecureSocketOptions.StartTls);
                    client.Authenticate(SMTP["Login"], SMTP["Password"]);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Send(message);
                    client.Disconnect(true);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return false;
            }
        }
    }
}