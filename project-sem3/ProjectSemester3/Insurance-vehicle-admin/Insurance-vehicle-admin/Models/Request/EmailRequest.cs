﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.Request
{
    public class EmailRequest
    {
       public int claimId { get; set; }
       public Guid cusId { get; set; }
       public string mailReceived { get; set; }
       public string subject { get; set; }
       public string body { get; set; }
    }
}
