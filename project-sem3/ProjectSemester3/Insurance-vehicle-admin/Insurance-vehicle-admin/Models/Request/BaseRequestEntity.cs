﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.Request
{
    public class BaseRequestEntity
    {
        public int EntityId { get; set; }
        public Guid UserId { get;set; }

        public string Reason { get; set; }
        public double Total { get; set; }
    }
}
