﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class ClaimDetailsDTO
    {

        public int ClaimId { get; set; }
        public string BodyClaim { get; set; }
        public DateTime CreateAt { get; set; }
        public int ClaimStatus { get; set; }



        public int? InsuranceId { get; set; }
        public double PricePercentByCarValue { get; set; }
        public double MinValueRefund { get; set; }
        public double MaxValueRefund { get; set; }
        public int DurationMonthUnit { get; set; }


        public int BillOfSaleId { get; set; }
        public int InsuranceDurationMonthly { get; set; }
        public Guid? CreatedBy { get; set; }
        public double TotalPrice { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? CreatedAtBillOfSale { get; set; }
  


        public int VegicleID { get; set; }
        public string LicensePlates { get; set; }
        public string VehicleVersionName { get; set; }
        public string VehicleModelName { get; set; }

        public Guid CusID { get; set; }
        public string CusPhone { get; set; }
        public string CustomName { get; set; }
        public string CusEmail { get; set; }
        public string Passport { get; set; }



        
        public int? InsuranceSoldid { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime? CreatedAtInsuranceSold { get; set; }
        public string Reason { get; set; }
        public double TotalValue { get; set; }



        //biến kiểm tra mắc số lần claim
        public int Billcount { get; set; }
        public int MaxRefundQuantity { get; set; }

    }
}