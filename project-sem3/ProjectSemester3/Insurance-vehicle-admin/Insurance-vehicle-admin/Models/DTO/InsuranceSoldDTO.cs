﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class InsuranceSoldDTO
    {
        public string InsuranceName { get; set; }
        public string CustomerEmail { get; set; }
        public DateTime? ActivationDate { get; set; }
        public Double Price { get; set; }
        public int Vehicleid { get; set; }
        public Double DaysRemaining { get; set; }
        public int InsuranceId { get; set; }
    }
}
