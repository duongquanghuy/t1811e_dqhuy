﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class BillCompensationDTO
    {
        public int Id { get; set; }
        public int ClaimID { get; set; }
        public string BodyClaim { get; set; }
        public Guid CustomerId { get; set; }
        public int? InsuranceId { get; set; }
        public int BillOfSaleId { get; set; }
        public string LicensePlates { get; set; }
        public string VehicleVersionName { get; set; }
        public string VehicleModelName { get; set; }
        public int InsuranceSolid { get; set; }
        public string CustomName { get; set; }
        public string CusPhone { get; set; }
        public string CusEmail { get; set; }
        public double TotalValue { get; set; }

        public DateTime CreateAt { get; set; }
        public DateTime? PaymentDate { get; set; }


    }
}
