﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class ListClaimDetailsDTO
    {
        public int Id { get; set; }
        public string BodyClaim { get; set; }
        public int? InsuranceId { get; set; }
        public int BillOfSaleId { get; set; }
        public string LicensePlates { get; set; }
        public string VehicleVersionName { get; set; }
        public string VehicleModelName { get; set; }
        public string CusPhone { get; set; }
        public string CustomName { get; set; }
        public string CusEmail { get; set; }
        public DateTime CreateAt { get; set; }
        public int ClaimStatus { get; set; }
        public Guid CusId { get; set; }
        public int VehicelId { get; set; }
        public int InsuranceSoldid { get; set; }
        //biến kiểm tra mắc số lần claim
        public int Billcount { get; set; }
        public int MaxRefundQuantity { get; set; }
    }
}
