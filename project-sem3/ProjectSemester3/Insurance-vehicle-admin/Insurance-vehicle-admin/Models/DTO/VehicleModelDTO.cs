﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class VehicleModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }  
        public virtual IEnumerable<VehicleVersionDTO> VehicleVersion { get; set; }
    }
}
