﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class ReportDTO
    {
        public double TotalMoneySale { get; set; }
        public double TotalMoneyClaim { get; set; }
        public int TotalCustomer { get; set; }
        public int TotalInsuranceExpiringSoon { get; set; }
        public int TotalClaimNotYetHandle { get; set; }
        public int TotalBillUnPaid { get; set; }
    }
}
