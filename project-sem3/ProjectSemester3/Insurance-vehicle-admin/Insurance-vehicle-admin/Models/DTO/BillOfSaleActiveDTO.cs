﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class BillOfSaleActiveDTO
    {

        public int InsuranceSoldId { get; set; }
        public int InsuranceId { get; set; }
        public DateTime ExpiresOn { get; set; }
        public double TotalPrice { get; set; }
        public double MaxValueRefund { get; set; }
        public int MaxRefundQuantity { get; set; }
        public int DurationMonthUnit { get; set; }
        public double MinRefundQuantity { get; set; }
        public double PricePercentByCarValue { get; set; }
        public string LicensePlates { get; set; }
        public string EmailCustomer { get; set; }
        public string VersionName { get; set; }
        public string ModelName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
