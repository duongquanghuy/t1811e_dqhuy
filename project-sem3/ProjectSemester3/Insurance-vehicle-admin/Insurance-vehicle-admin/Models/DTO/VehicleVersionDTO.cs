﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class VehicleVersionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ModelId { get; set; }
    }
}
