﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class BillOfSaleDTO
    {
        public int BillId { get; set; }
        public Guid CustomerId { get; set; }
        public int InsuranceDurationMonthly { get; set; }
        public Guid? CreatedBy { get; set; }
        public double TotalPrice { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceId { get; set; }
        public string CustumerName { get; set; }
        public double PricePercentByCarValue { get; set; }
        public string ModelName { get; set; }
        public string VersionName { get; set; }
        public string PhoneNumber { get; set; }
        public string LicensePlate { get; set; }
        public int VehicleId { get; set; }
        public string CusEmail { get; set; }
        public  string InsuranceName { get; set; }
    }
}
