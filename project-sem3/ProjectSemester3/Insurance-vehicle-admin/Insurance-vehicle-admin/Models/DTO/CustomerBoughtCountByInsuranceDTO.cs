﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class CustomerBoughtCountByInsuranceDTO
    {
        public int boughtCount { get; set; }
        public int customerId { get; set; }
    }
}
