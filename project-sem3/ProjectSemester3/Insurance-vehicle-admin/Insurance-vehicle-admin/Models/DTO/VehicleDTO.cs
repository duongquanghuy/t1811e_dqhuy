using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public string BodyNumber { get; set; }
        public string EngineNumber { get; set; }
        public string LicensePlates { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int? ModelId { get; set; }
        public string? ModelName { get; set; }
        public int? VersionId { get; set; }
        public string? VersionName { get; set; }
        public Guid? CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int VehicleId { get; set; }
        public string Model { get; set; }
        public string Version { get; set; }
        public Double Value { get; set; }
    }
}
