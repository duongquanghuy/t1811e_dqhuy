﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Insurance_vehicle_admin.Models.DTO
{
    public class LoginSuccessDTO
    {

        public string AdminName { get; set; }
        public string AdminId { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}

