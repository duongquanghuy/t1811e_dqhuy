﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class ImageInClaim
    {
        public int Id { get; set; }
        public int ClaimDetailId { get; set; }
        public string Link { get; set; }
        public DateTime CreatedAt { get; set; }

        public virtual ClaimDetail ClaimDetail { get; set; }
    }
}
