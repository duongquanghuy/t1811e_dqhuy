﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class Role
    {
        public Role()
        {
            Admin = new HashSet<Admin>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Admin> Admin { get; set; }
    }
}
