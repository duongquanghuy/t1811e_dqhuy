﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class ExpenseType
    {
        public ExpenseType()
        {
            CompanyExpense = new HashSet<CompanyExpense>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CompanyExpense> CompanyExpense { get; set; }
    }
}
