﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class VehicleVersion
    {
        public VehicleVersion()
        {
            Estimate = new HashSet<Estimate>();
            PriceListVehicleNew = new HashSet<PriceListVehicleNew>();
            Vehicle = new HashSet<Vehicle>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ModelId { get; set; }

        public virtual VehicleModel Model { get; set; }
        public virtual ICollection<Estimate> Estimate { get; set; }
        public virtual ICollection<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual ICollection<Vehicle> Vehicle { get; set; }
    }
}
