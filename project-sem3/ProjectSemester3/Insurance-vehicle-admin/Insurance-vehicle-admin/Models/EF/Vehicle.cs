﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class Vehicle
    {
        public Vehicle()
        {
            BillCompensation = new HashSet<BillCompensation>();
            BillOfSale = new HashSet<BillOfSale>();
        }

        public int Id { get; set; }
        public string BodyNumber { get; set; }
        public string EngineNumber { get; set; }
        public string LicensePlates { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int? ModelId { get; set; }
        public int? VersionId { get; set; }
        public Guid? CustomerId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual VehicleModel Model { get; set; }
        public virtual VehicleVersion Version { get; set; }
        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
    }
}
