﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class InsuranceVehicleContext : DbContext
    {
        public InsuranceVehicleContext()
        {
        }

        public InsuranceVehicleContext(DbContextOptions<InsuranceVehicleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<BillCompensation> BillCompensation { get; set; }
        public virtual DbSet<BillOfSale> BillOfSale { get; set; }
        public virtual DbSet<CalculatorActualValueVehicle> CalculatorActualValueVehicle { get; set; }
        public virtual DbSet<ClaimDetail> ClaimDetail { get; set; }
        public virtual DbSet<CompanyExpense> CompanyExpense { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Estimate> Estimate { get; set; }
        public virtual DbSet<ExpenseType> ExpenseType { get; set; }
        public virtual DbSet<ImageInClaim> ImageInClaim { get; set; }
        public virtual DbSet<Insurance> Insurance { get; set; }
        public virtual DbSet<InsuranceSold> InsuranceSold { get; set; }
        public virtual DbSet<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Sysdiagrams> Sysdiagrams { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }
        public virtual DbSet<VehicleVersion> VehicleVersion { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tcp:t1811e-aptech-group1.database.windows.net,1433;Initial Catalog=InsuranceVehicle;Persist Security Info=False;User ID=aptech-t1811e-group1;Password=12345678@Abc;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Admin)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Admin__RoleId__0F624AF8");
            });

            modelBuilder.Entity<BillCompensation>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Reason).HasColumnType("text");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.AdminId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Admin__10566F31");

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ClaimDetailId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Custo__114A936A");

                entity.HasOne(d => d.InsuranceSold)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.InsuranceSoldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Insur__123EB7A3");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Verhi__1332DBDC");
            });

            modelBuilder.Entity<BillOfSale>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.PaymentStatus).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__BillOfSal__Creat__151B244E");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Custo__160F4887");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.InsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Insur__17036CC0");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Verhi__17F790F9");
            });

            modelBuilder.Entity<ClaimDetail>(entity =>
            {
                entity.Property(e => e.BodyClaim).HasColumnType("text");

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Status).HasColumnName("status");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ClaimDeta__Custo__18EBB532");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.InsuranceId)
                    .HasConstraintName("FK__ClaimDeta__Insur__19DFD96B");
            });

            modelBuilder.Entity<CompanyExpense>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.ExpensesType)
                    .WithMany(p => p.CompanyExpense)
                    .HasForeignKey(d => d.ExpensesTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CompanyEx__Expen__1AD3FDA4");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Customer__A9D10534FF83E254")
                    .IsUnique();

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Passport).HasColumnType("text");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");
            });

            modelBuilder.Entity<Estimate>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Estimate__Custom__1BC821DD");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Estimate__ModelI__1CBC4616");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Estimate__Versio__1DB06A4F");
            });

            modelBuilder.Entity<ExpenseType>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ImageInClaim>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.ImageInClaim)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ImageInCl__Claim__1EA48E88");
            });

            modelBuilder.Entity<Insurance>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Description).HasColumnType("text");

                entity.Property(e => e.InsuranceName).HasColumnType("text");
            });

            modelBuilder.Entity<InsuranceSold>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExpiresOn).HasColumnType("datetime");

                entity.HasOne(d => d.BillOfSale)
                    .WithMany(p => p.InsuranceSold)
                    .HasForeignKey(d => d.BillOfSaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Insurance__BillO__1F98B2C1");
            });

            modelBuilder.Entity<PriceListVehicleNew>(entity =>
            {
                entity.HasOne(d => d.Model)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__PriceList__Model__208CD6FA");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__PriceList__Versi__2180FB33");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Sysdiagrams>(entity =>
            {
                entity.HasKey(e => e.DiagramId)
                    .HasName("PK__sysdiagr__C2B05B6180A34E55");

                entity.ToTable("sysdiagrams");

                entity.HasIndex(e => new { e.PrincipalId, e.Name })
                    .HasName("UK_principal_name")
                    .IsUnique();

                entity.Property(e => e.DiagramId).HasColumnName("diagram_id");

                entity.Property(e => e.Definition).HasColumnName("definition");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(128);

                entity.Property(e => e.PrincipalId).HasColumnName("principal_id");

                entity.Property(e => e.Version).HasColumnName("version");
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.Property(e => e.BodyNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.EngineNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LicensePlates)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Vehicle__Custome__22751F6C");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Vehicle__ModelId__236943A5");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Vehicle__Version__245D67DE");
            });

            modelBuilder.Entity<VehicleModel>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<VehicleVersion>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.VehicleVersion)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__VehicleVe__Model__25518C17");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
