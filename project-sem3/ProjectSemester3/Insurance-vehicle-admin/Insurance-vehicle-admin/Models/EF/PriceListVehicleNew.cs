﻿using System;
using System.Collections.Generic;

namespace Insurance_vehicle_admin.Models.EF
{
    public partial class PriceListVehicleNew
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public int? ModelId { get; set; }
        public int? VersionId { get; set; }

        public virtual VehicleModel Model { get; set; }
        public virtual VehicleVersion Version { get; set; }
    }
}
