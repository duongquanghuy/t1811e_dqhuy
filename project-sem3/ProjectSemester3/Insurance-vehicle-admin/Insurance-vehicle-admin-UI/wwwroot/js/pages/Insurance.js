﻿//Lấy dữ liệu Model đổ vào View
$(document).ready(function () {
    $("table").css("display", "none");
    getData();
});

//Khai báo các biến
var editInsuranceId; //dùng để đánh dấu Insurance sẽ được Sửa
var deleteInsuranceId; //dùng để đánh dấu Insurance sẽ được Xóa
var detailsInsuranceId; //dùng để đánh dấu Insurance đang được Xem Chi tiết
var cusName; //đánh dấu Tên khách hàng nhận được từ API
var cusId; //đánh dấu Id Khách hàng nhận được từ API
var cusBoughtCount; //đánh dấu lượng mua Bảo hiểm hiện tại của Khách hàng tương ứng

// format datetime
function formatDate(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;

    return date;
}

//Đổ dữ liệu vào bảng
function getData(page = 1, pageSize = 10) {
    $('.load-table').css('display', 'flex');

    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/all/' + page + '/' + pageSize,
        type: 'GET',
        dataType: 'json',
        success: function (respond) {
            $('#list-data-table-model').html('');

            $(".pagination").html('');

            $.each(respond.data, function (index, ins) {
                $('#list-data-table-model').append(
                    '<tr>' +
                    '<td style="vertical-align: middle; text-align: left; cursor: pointer;" class="td-link" data-toggle="modal" data-target="#insurance-details"' +
                    'onclick="openInsuranceDetailsModal(' + ins.id + ', \'' + ins.name + '\')">' + ins.name + '</td >' +
                    '<td style="vertical-align: middle; text-align: center;">' + ins.pricePercentByCarValue + '<i>%</i></td>' +
                    '<td style="vertical-align: middle; text-align: center;">' + ins.minValueRefund + '$</td>' +
                    '<td style="vertical-align: middle; text-align: center;">' + ins.maxValueRefund + '$</td>' +
                    '<td style="vertical-align: middle; text-align: center;">' + ins.maxRefundQuantity + ' <i>time(s)</i></td>' +
                    '<td style="vertical-align: middle; text-align: center;">' + formatDate(ins.createdAt) + '</td>' +
                    '<td style="vertical-align: middle; text-align: center;">' + ins.durationMonth + ' <i>month(s)</i></td>' +
                    '<td style="text-align:center; vertical-align: middle;">' +
                    '<button class="btn btn-sm btn-warning" style="margin-right: 7px; display: inline-block;" ' +
                    'onclick="showEditInsuranceModal(' + ins.id + ')" data-toggle="modal" data-target="#edit-insurance">' +
                    '<i class="fa fa-fw fa-edit" title="Edit Insurance"></i>'+
                    '</button>' +
                    '<button class="btn btn-sm btn-danger" style="display: inline-block;" onclick="showDeleteInsuranceModal(' + ins.id + ')"' +
                    ' data-toggle="modal" data-target="#delete-insurance">' +
                    '<i class="fa fa-fw fa-trash" title="Delete Insurance"></i>'+
                    '</button>' +
                    '</td>' +
                    '</tr>'
                );
            });

            $("table").css("display", "inline-table");
            $('.load-table').css('display', 'none');

            let currentPage = respond.currentPage;
            let totalPage = respond.totalPage;

            if (page > totalPage) {
                getData(1, 10);
            }
            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
            }

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
            }

            if (currentPage > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
            }
            $(".pagination > li").on("click", function () {
                getData($(this).val());
            })
        },
        error: function () {
            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
        },
    });
}

//Thêm Mới Insurance
function addNewInsurance() {
    var addInsuranceName = $('#add-insurance-name').val();
    var addInsPrirePercentByCarValue = $('#add-price-percent-by-car-value').val();
    var addInsMinValueRefund = $('#add-min-value-refund').val();
    var addInsMaxValueRefund = $('#add-max-value-refund').val();
    var addInsMaxRefundQuantity = $('#add-max-refund-quantity').val();
    var addInsDurationMonthUnit = $('#add-duration-month-unit').val();
    var addInsDescription = $('#add-description').val();

    if (addInsuranceName == '' || addInsPrirePercentByCarValue == '' || addInsMinValueRefund == '' || addInsMaxValueRefund == ''
        || addInsMaxRefundQuantity == '' || addInsDurationMonthUnit == '' || addInsDescription == '') {
        showNotificationModal('Add New Insurance', 'You must enter all field!');
    } else {
        var newInsurance = {
            "InsuranceName": addInsuranceName.toString(),
            "pricePercentByCarValue": parseFloat(addInsPrirePercentByCarValue),
            "minValueRefund": parseInt(addInsMinValueRefund),
            "maxValueRefund": parseInt(addInsMaxValueRefund),
            "maxRefundQuantity": parseInt(addInsMaxRefundQuantity),
            "durationMonthUnit": parseInt(addInsDurationMonthUnit),
            "Description": addInsDescription
        }

        console.log(JSON.stringify(newInsurance));

        $.ajax({
            url: 'https://localhost:44314/admin/Insurance/add',
            type: 'POST',
            data: JSON.stringify(newInsurance),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                showNotificationModal('<b>Add New Insurance</b>', 'Add Insurance <b>' + addInsuranceName + '</b> Success!');

                $('#close-add-insurance-btn').trigger('click');

                $('#add-price-percent-by-car-value').val('');
                $('#add-min-value-refund').val(0);
                $('#add-max-value-refund').val('');
                $('#add-max-refund-quantity').val('');
                $('#add-duration-month-unit').val('');
                $('#add-description').val('');

                getData();
            },
            error: function (data) {
                showNotificationModal('<b>Add New Insurance</b>', 'Error: Add Insurance <b>' + addInsuranceName + '</b> Failed!');
                $('#close-add-insurance-btn').trigger('click');

                getData();
            },
        });
    }
}

//Hiển thị Modal Sửa Insurance
function showEditInsuranceModal(id) {
    var insuranceId = id;

    editInsuranceId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/search/' + insuranceId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#edit-insurance-name').val(data.data.insuranceName);
            $('#edit-price-percent-by-car-value').val(data.data.pricePercentByCarValue);
            $('#edit-min-value-refund').val(data.data.minValueRefund);
            $('#edit-max-value-refund').val(data.data.maxValueRefund);
            $('#edit-max-refund-quantity').val(data.data.maxRefundQuantity);
            $('#edit-duration-month-unit').val(data.data.durationMonthUnit);
            $('#edit-description').val(data.data.description);
        },
        error: function (data) {
            showNotificationModal('Edit Model', 'Error: Get Insurance to Edit Failed!');
        },
    });
}

//Sửa Insurance
function editInsurance() {
    var editInsuranceName = $('#edit-insurance-name').val();
    var editInsPrirePercentByCarValue = $('#edit-price-percent-by-car-value').val();
    var editInsMinValueRefund = $('#edit-min-value-refund').val();
    var editInsMaxValueRefund = $('#edit-max-value-refund').val();
    var editInsMaxRefundQuantity = $('#edit-max-refund-quantity').val();
    var editInsDurationMonthUnit = $('#edit-duration-month-unit').val();
    var editInsDescription = $('#edit-description').val();

    if (editInsuranceName == '' || editInsPrirePercentByCarValue == '' || editInsMinValueRefund == '' || editInsMaxValueRefund == '' ||
        editInsMaxRefundQuantity == '' || editInsDurationMonthUnit == '' || editInsDescription == '') {
        showNotificationModal('Edit Insurance', 'You must enter all field!');
    } else {
        var editInsurance = {
            "id": editInsuranceId,
            "InsuranceName": editInsuranceName,
            "pricePercentByCarValue": parseFloat(editInsPrirePercentByCarValue),
            "minValueRefund": parseInt(editInsMinValueRefund),
            "maxValueRefund": parseInt(editInsMaxValueRefund),
            "maxRefundQuantity": parseInt(editInsMaxRefundQuantity),
            "durationMonthUnit": parseInt(editInsDurationMonthUnit),
            "Description": editInsDescription
        }

        console.log(JSON.stringify(editInsurance));

        $.ajax({
            url: 'https://localhost:44314/admin/Insurance/edit',
            type: 'PUT',
            data: JSON.stringify(editInsurance),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                showNotificationModal('<b>Edit New Insurance</b>', 'Edit Insurance <b>' + editInsuranceName + '</b> Success!');

                $('#close-edit-insurance-btn').trigger('click');

                $('#edit-insurance-name').val('');
                $('#edit-price-percent-by-car-value').val('');
                $('#edit-min-value-refund').val(0);
                $('#edit-max-value-refund').val('');
                $('#edit-max-refund-quantity').val('');
                $('#edit-duration-month-unit').val('');
                $('#edit-description').val('');

                getData();
            },
            error: function (data) {
                showNotificationModal('Edit Insurance', 'Error: Edit Insurance <b>' + editInsuranceName + '</b> Failed!');

                getData();
            },
        });
    }
}

//Hiển thị Modal Xóa Insurance
function showDeleteInsuranceModal(id) {
    deleteInsuranceId = id;
}

//Xóa Insurance
function deleteInsurance() {
    if (deleteInsuranceId != '') {
        $.ajax({
            url: 'https://localhost:44314/admin/Insurance/delete/' + deleteInsuranceId,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                showNotificationModal('<b>Delete Insurance</b>', 'Delete Insurance Success!');

                getData();
            },
            error: function (data) {
                showNotificationModal('<b>Delete Insurance</b>', 'Error: Delete Insurance Failed!');

                getData();
            },
        });
    } else {
        showNotificationModal('<b>Delete Insurance</b>', 'Error: Delete Insurance Failed!');
        getData();
    }
}

//Mở Modal Chi tiết Bảo hiểm
function openInsuranceDetailsModal(insuranceId, insuranceName) {
    detailsInsuranceId = insuranceId;

    $('#ins-details-title').html(insuranceName);

    //Đổ dữ liệu chi tiết của Bảo hiểm vào Modal
    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/search/' + detailsInsuranceId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#ins-details-price-percent-by-car-value').val(data.data.pricePercentByCarValue + '%');
            $('#ins-details-min-value-refund').val(data.data.minValueRefund + '$');
            $('#ins-details-max-value-refund').val(data.data.maxValueRefund + '$');
            $('#ins-details-max-refund-quantity').val(data.data.maxRefundQuantity + ' time(s)');
            $('#ins-details-duration-month-unit').val(data.data.durationMonthUnit + ' month(s)');

            $('#ins-description-title').html(insuranceName);
            $('#ins-description-body').html(data.data.description);
        },
        error: function (data) {
            showNotificationModal('<b>' + insuranceName + '</b>', 'Error: Get Insurance <b>' + insuranceName + '</b> Details Failed!');
        },
    });

    //Lấy số lần được mua của Bảo hiểm
    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/getInsuranceBoughtTimes/' + detailsInsuranceId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            $('#ins-details-total-bill').val(res.data + ' time(s)');
        },
        error: function () {
            showNotificationModal('<b>' + insuranceName + '</b>', 'Error: Get Insurance <b>' + insuranceName + '</b> Bill Bought Count Failed!');
        },
    });

    //Lấy các khách hàng đã mua Bảo hiểm này
    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/getCustomerBoughtByInsurance/' + detailsInsuranceId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            var countCustomer = res.data.length;
            $('#ins-details-total-customer').val(countCustomer + ' people(s)');

            if (countCustomer == 0) {
                $('#ins-details-present-title-3').css('display', 'none');
                $('#ins-details-present-title-4').css('display', 'block');
                $('#table-cus').css('display', 'none');
            } else {
                $('#ins-details-present-title-3').css('display', 'block');
                $('#ins-details-present-title-4').css('display', 'none');
                $('#table-cus').css('display', 'table');

                //Đổ dữ liệu vào bảng
                fillData();
            }
        },
        error: function (res) {
            showNotificationModal('<b>' + insuranceName + '</b>', 'Error: Get Insurance <b>' + insuranceName + '</b> Customer Bought This Insurance Failed!');
        },
    });

    //Lấy số hóa đơn hiện tại của Bảo hiểm
    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/getBillOfSaleByInsurance/' + detailsInsuranceId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            if (res.data.length == 0) {
                $('#ins-details-present-title-2').css('display', 'block');
                $('#ins-details-present-title-1').css('display', 'none');
                $('#table-bill').css('display', 'none');
            } else {
                $('#ins-details-present-title-2').css('display', 'none');
                $('#ins-details-present-title-1').css('display', 'block');
                $('#table-bill').css('display', 'table');

                //Đổ dữ liệu
                $('#ins-details-tbl-body').html('');

                $.each(res.data, function (index, b) {
                    if (b.paymentStatus == 1) {
                        $('#ins-details-tbl-body').append(
                            '<tr>' +
                            '<td class="ins-bill" onclick="goToBillDetails(' + b.billId + ')">' + b.billId + '</td>' +
                            '<td class="ins-name" onclick="goToCustomerDetails(\'' + b.customerId + '\')">' + b.custumerName + '</td>' +
                            '<td class="ins-price">' + b.totalPrice + '$</td>' +
                            '<td class="ins-paid">Paid</td>' +
                            '<td class="ins-vehicle" onclick="goToVehicle(' + b.vehicleId + ')">' + b.licensePlate + '</td>' +
                            '</tr>'
                        );
                    } else {
                        $('#ins-details-tbl-body').append(
                            '<tr>' +
                            '<td class="ins-bill" onclick="goToBillDetails(' + b.billId + ')">' + b.billId + '</td>' +
                            '<td class="ins-name" onclick="goToCustomerDetails(\'' + b.customerId + '\')">' + b.customerName + '</td>' +
                            '<td class="ins-price">' + b.totalPrice + '$</td>' +
                            '<td class="ins-unpaid">Unpaid</td>' +
                            '<td class="ins-vehicle" onclick="goToVehicleDetails(' + b.vehicleId + ')">' + b.licensePlate + '</td>' +
                            '</tr>'
                        );
                    }
                });
            }
        },
        error: function (res) {
            showNotificationModal(insuranceName, "Error: " + res.message);
        },
    });
}

//Đổ dữ liệu vào Bảng Số lần Khách hàng Mua Bảo hiểm này
function fillData() {
    $.ajax({
        url: 'https://localhost:44314/admin/Insurance/getCustomerBoughtCountByInsurance/' + detailsInsuranceId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            $('#ins-details-tbl-body-cus').html('');

            $.each(res.data, function (index, item) {

                cusId = item.customerId;

                cusBoughtCount = item.boughtCount;

                getCusNameById();

                setTimeout(function () {
                    $('#ins-details-tbl-body-cus').append(
                        '<tr><td class="ins-name" onclick="goToCustomerDetails(\'' + item.customerId + '\')">' + cusName + '</td><td class="ins-bill">' + cusBoughtCount + '</td></tr>'
                    );
                }, 1000);
            });
        },
        error: function (res) {
            showNotificationModal('<b>' + insuranceName + '</b>', 'Error: Get Insurance <b>' + insuranceName + '</b> Bought Count By Customer Failed!');
        },
    });
    
}

//Lấy Tên khách hàng theo Id
function getCusNameById() {
    $.ajax({
        url: 'https://localhost:44314/admin/Customer/searchCustomerById/' + cusId,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            cusName = res.data[0].name.toString();
        },
        error: function (res) {
            showNotificationModal('<b>Get Customer Name</b>', "Get Customer Name Failed!");
        },
    });
}

//Hiện Bill Count, Ẩn Customer Count
function openBillCountArea() {
    $('#bill-count-area').css('display', 'block'); //.css('display', 'block');
    $('#customer-count-area').css('display', 'none'); //.css('display', 'none');
}

//Hiện Customer Count, Ẩn Bill Count
function openCustomerCountArea() {
    $('#bill-count-area').css('display', 'none'); //.css('display', 'none');
    $('#customer-count-area').css('display', 'block'); //.css('display', 'block');
}

//Đóng Modal Chi tiết Insurance
function closeInsuranceDetailsModal() {
    $('#bill-count-area').css('display', 'none');
    $('#customer-count-area').css('display', 'none');
    $('#ins-details-tbl-body-cus').html('');
    $('#ins-details-tbl-body').html('');
}

//Redirect về Trang Chi tiết Bill
function goToBillDetails(billId) {
    return window.location.replace('/BillOfSale/BillDetail/' + billId);
}

//Redirect về Trang Chi tiết Khách hàng
function goToCustomerDetails(customerId) {
    return window.location.replace('/Customer/CustomerDetail/' + customerId);
}

//Redirect về Trang Chi tiết Xe
function goToVehicleDetails(vehicleId) {
    return window.location.replace('/Vehicle/VehicleDetail/' + vehicleId);
}