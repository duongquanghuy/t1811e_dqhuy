﻿// biến xác định thời gian đẻ tìm kiếm bill sắp hết hạn
var DatetimeSearch = null;
$(document).ready(function () {
    var now = new Date();
    var DateAlert = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8);
    var offset1 = DateAlert.getTime() - now.getTime();
    var readAlert = Math.round(offset1 / 1000 / 60 / 60 / 24);
 
    $("table").css("display", "none");
    getData();
    DatetimeSearch = null;           

});
// tìm kiếm các đơn theo ngày sắp hết hạn chở về sau
function getBillActiveExpires() {
    var datetime = $('.searchExpires').val();
    if (datetime == "") {
        console.log("not Found");
    } else {
        var now = new Date();
        var datimeNow = new Date(now.getFullYear(), now.getMonth(), now.getDate());
        var datetimeP = Date.parse(datetime);
        console.log(datetimeP);
        if (datetimeP >= datimeNow.getTime()) {
            DatetimeSearch = formattedRequestDb(datetime);
            $("table").css("display", "none");
            getData();

        } else {
            console.log('loi');
            var msgHead = 'Announcement';
            var msgBody = 'No result is foundSearch time is invalid. time must be big or by the current time!';
            showNotificationModal(msgHead, msgBody);
        }
    }
   
}

// format datime
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}

//nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 2 ) {
    var now = new Date();
    var DateAlert = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8);
    var offset1 = DateAlert.getTime() - now.getTime();
    var readAlert = Math.round(offset1 / 1000 / 60 / 60 / 24);
   
    var Link = "";
    if (DatetimeSearch != null) {
        Link = 'https://localhost:44314/admin/Billofsale/billActiveExpires/' + page + '/' + pageSize + '?dateTime=' + DatetimeSearch + '';
    } else {
        Link = 'https://localhost:44314/admin/Billofsale/BillActive/' + page + '/' + pageSize;
        DatetimeSearch = null;
    }
    $('.load-table').css('display', 'flex');
    $.ajax({

        type: 'get',
        url: Link,
        dataType: 'json',
        success: function (result) {
            console.log(result.data);
            if (result.data == null || result.data == '') {
                var msgHead = 'Announcement';
                var msgBody = 'No result is found!';
                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
                showNotificationModal(msgHead, msgBody);
            } else {
                $("#list-data-table-billActive").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {
                    console.log(val)
                    var DateExpiresOn = Date.parse(val.expiresOn);
                    var offset = DateExpiresOn - now.getTime();

                    var totalDays = Math.round(offset / 1000 / 60 / 60 / 24);


                    var dateTimeFormat = formatted(val.expiresOn);
                    var dateTimeFormat1 = formatted(val.createdAt);
                    let html = '<tr>' +
                        '<td>' + val.insuranceSoldId + '</th>' +
                        '<td>' + val.insuranceId + '</th>' +
                        '<td>' + val.totalPrice + ' $</th>' +
                        '<td>' + val.licensePlates + '</th>' +
                        '<td>' + val.phoneNumber + '</th>' +
                        '<td>' + val.emailCustomer + '</th>' +
                        '<td>' + val.versionName + '</th>' +
                        '<td>' + dateTimeFormat + '</th>' +
                        '<td class="readAlert">' + totalDays + ' Day</th>' +
                        '<td>' + dateTimeFormat1 + '</th>' +
                        '<td style="text-align:center">' +
                        '<button class="btn btn-sm btn-primary" onclick="displayBill(' + val.insuranceId + ')" >' +
                        '<i class="fa fa-fw fa-eye" title="Details"></i>' +
                        '</button>' +
                        '<button class="btn btn-sm btn-warning" style="margin-left: 7px; margin-right: 7px;">' +
                        '<i class="fa fa-fw fa-edit" title="Sửa"></i>' +
                        '</button>' +
                        '<button class="btn btn-sm btn-danger">' +
                        '<i class="fa fa-fw fa-trash" title="Xóa"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';


                    $("#list-data-table-billActive").append(html);
                    if (totalDays <= readAlert) {
                        $('.readAlert' + totalDays + '').css('color', 'red');
                    }
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                });

                let currentPage = result.currentPage;
                let totalPage = result.totalPage;

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                            <a class="page-link" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                    </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                });
            }
        },
        error: function () {
            $("table").css("display", "block");
            $('.load-table').css('display', 'none');

        }
    });
}
// format datime gửi lên server
function formattedRequestDb(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = year + "/" + monthformat + "/" + dayformat;
    return date;
}
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}

function hideNotificationModal() {
    $('#notification-modal-container').modal('hide');
}


