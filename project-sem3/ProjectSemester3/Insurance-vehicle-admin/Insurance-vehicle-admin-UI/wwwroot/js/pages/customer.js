﻿//Lấy dữ liệu Model đổ vào View
$(document).ready(function () {
    $("#customer-list-table").css("display", "none");
    getData();
});

//Khai báo các biến
var editCustomerId; //dùng để đánh dấu Customer sẽ được Sửa
var ajaxUrl, check; //dùng để thay đổi cấu trúc Url ajax và Kiểm tra điều kiện Tìm kiếm
var customerEmailList = []; //dùng để lưu toàn bộ các email của Khách hàng

//Đổ dữ liệu vào bảng
function getData(page = 1, pageSize = 10) {
    $('.load-table').css('display', 'flex');
    $('.box-body > p').css('display', 'none');

    customerEmailList = [];

    $.ajax({
        url: 'https://localhost:44314/admin/Customer/all/' + page + '/' + pageSize,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json',
        success: function (respond) {
            $('#list-data-table-account').html('');

            $(".pagination").html('');

            $.each(respond.data, function (index, item) {
                $('#list-data-table-account').append(
                    '<tr>' +
                    '<td class="td-link" style="vertical-align: middle; cursor: pointer" data-toggle="modal" data-target="#details-customer" onclick="openCustomerDetailsModal(\'' + item.id + '\')">' + item.name + '</td>' +
                    '<td style="vertical-align: middle">' + item.phoneNumber + '</td>' +
                    '<td style="vertical-align: middle">' + item.email + '</td>' +
                    '<td style="vertical-align: middle">' + item.passport + '</td>' +
                    '<td style="vertical-align: middle">' + item.address + '</td>' +
                    '<td style="vertical-align: middle; text-align: center">' +
                    '<button class="btn btn-sm btn-warning"' +
                    'onclick="getCustomerToEdit(\'' + item.id + '\')" data-toggle="modal" data-target="#edit-customer">' +
                    '<i class="fa fa-fw fa-edit" title="Edit"></i> ' +
                    'Edit Customer' +
                    '</button> | ' +
                    '<button class="btn btn-sm btn-info"' +
                    'onclick="openSendEmailToCustomerModal(\'' + item.email + '\')">' +
                    '<i class="fa fa-envelope" title="Send Email"></i> ' +
                    'Send E-Mail' +
                    '</button>' +
                    '</td>' +
                    '</tr>'
                );

                customerEmailList.push(item.email);
            });

            console.log(customerEmailList);

            $("#customer-list-table").css("display", "inline-table");
            $('.load-table').css('display', 'none');

            let currentPage = respond.currentPage;
            let totalPage = respond.totalPage;

            if (page > totalPage) {
                getData(1, 10);
            }
            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`);
            }

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
            }

            if (currentPage > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
            }

            $(".pagination > li").on("click", function () {
                getData($(this).val());
            });
        },
        error: function () {
            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
        }
    });
}

//Reset lại Add Form
function resetAddForm() {
    $('#add-full-name').val('');
    $('#add-phone').val('');
    $('#add-address').val('');
    $('#add-email').val('');
    $('#add-passport').val('');
    $('#add-password').val('');
}

//Reset lại Edit Form
function resetEditForm() {
    $('#edit-full-name').val('');
    $('#edit-phone').val('');
    $('#edit-address').val('');
    $('#edit-email').val('');
    $('#edit-passport').val('');
}

//Thêm Customer
function addNewCustomer() {
    var fullName = $('#add-full-name').val();
    var phone = $('#add-phone').val();
    var address = $('#add-address').val();
    var email = $('#add-email').val();
    var passport = $('#add-passport').val();
    var password = $('#add-password').val();

    if (fullName == '' || phone == '' || address == '' || email == '' || passport == '' || password == '') {
        showNotificationModal('<b>Add Customer</b>', 'Please enter all fields!');
    } else {
        var newCustomer = {
            "Name": fullName,
            "PhoneNumber": phone,
            "Address": address,
            "Email": email,
            "Passport": passport,
            "Password": password
        }

        console.log(JSON.stringify(newCustomer));

        $.ajax({
            url: 'https://localhost:44314/admin/Customer/add/',
            type: 'POST',
            data: JSON.stringify(newCustomer),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                showNotificationModal('<b>Add Customer</b>', 'Add New Customer <b>' + fullName + '</b> Success!');
                $('#close-add-customer-btn').trigger('click');
                resetAddForm();
                getData();
            },
            error: function () {
                showNotificationModal('<b>Add Customer</b>', 'Add New Customer <b>' + fullName + '</b> Failed!');
                resetAddForm();
                getData();
            },
        });
    }
}

//Đổ dữ liệu vào Edit Modal
function getCustomerToEdit(id) {
    editCustomerId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/Customer/searchCustomerById/' + id,
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (respond) {
            $('#edit-full-name').val(respond.data[0].name);
            $('#edit-phone').val(respond.data[0].phoneNumber);
            $('#edit-address').val(respond.data[0].address);
            $('#edit-passport').val(respond.data[0].passport);
        },
        error: function (respond) {
            showNotificationModal('<b>Edit Customer</b>', 'Get Customer to Edit Failed!');
        },
    });
}

//Sửa Customer
function editCustomer() {
    var fullName = $('#edit-full-name').val();
    var phone = $('#edit-phone').val();
    var address = $('#edit-address').val();
    var passport = $('#edit-passport').val();

    if (fullName == '' || phone == '' || address == '' || passport == '') {
        showNotificationModal('Edit Customer', 'Please enter all fields!');
    } else {
        var editCustomer = {
            "Id": editCustomerId,
            "Name": fullName,
            "PhoneNumber": phone,
            "Address": address,
            "Passport": passport
        }

        $.ajax({
            url: 'https://localhost:44314/admin/Customer/edit/',
            type: 'PUT',
            data: JSON.stringify(editCustomer),
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                showNotificationModal('<b>Edit Customer</b>', 'Edit Customer <b>' + fullName + '</b> Success!');
                $('#close-edit-customer-btn').trigger('click');
                resetEditForm();
                getData();
            },
            error: function () {
                showNotificationModal('<b>Edit Customer</b>', 'Edit Customer <b>' + fullName + '</b> Failed!');
                resetEditForm();
                getData();
            },
        });
    }
}

//Tìm kiếm Khách hàng theo Tên, Id, Điện thoại
async function searchCustomer(page = 1, pageSize = 10) {
    $('.load-table').css('display', 'flex');

    var searchText = $('#search-customer').val();

    if (searchText != '') {
        await $.ajax({
            url: 'https://localhost:44314/admin/Customer/searchCustomer/' + searchText,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            success: function (respond) {
                $('#list-data-table-account').html('');

                $(".pagination").html('');

                if (respond.data == null || respond.data.length == 0) {
                    $('#customer-list-table').css('display', 'none');
                    $('.load-table').css('display', 'none');
                    $('.box-body > p').css('display', 'block');
                } else {
                    $('.box-body > p').css('display', 'none');

                    $.each(respond.data, function (index, item) {
                        $('#list-data-table-account').append(
                            '<tr>' +
                            '<td style="vertical-align: middle">' + item.id + '</td>' +
                            '<td style="vertical-align: middle" onclick="openCustomerDetailsModal(' + item.id + ')">' + item.name + '</td>' +
                            '<td style="vertical-align: middle">' + item.phoneNumber + '</td>' +
                            '<td style="vertical-align: middle">' + item.email + '</td>' +
                            '<td style="vertical-align: middle">' + item.passport + '</td>' +
                            '<td style="vertical-align: middle">' + item.address + '</td>' +
                            '<td style="vertical-align: middle; text-align: center">' +
                            '<button class="btn btn-sm btn-warning"' +
                            'onclick="getCustomerToEdit(\'' + item.id + '\')" data-toggle="modal" data-target="#edit-customer">' +
                            '<i class="fa fa-fw fa-edit" title="Edit"></i> ' +
                            'Edit Customer' +
                            '</button> | ' +
                            '<button class="btn btn-sm btn-info"' +
                            'onclick="" data-toggle="modal" data-target="#send-email">' +
                            '<i class="fa fa-envelope" title="Send Email"></i>' +
                            'Send E-Mail' +
                            '</button>' +
                            '</td>' +
                            '</tr>'
                        );
                    });

                    $("#customer-list-table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');

                    let currentPage = respond.currentPage;
                    let totalPage = respond.totalPage;

                    if (page > totalPage) {
                        searchCustomer(1, 10);
                    }
                    if (currentPage - 1 > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`);
                    }

                    if (currentPage - 1 > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                    }

                    if (currentPage > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                    }

                    if (currentPage + 1 <= totalPage) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                    }

                    if (currentPage + 1 <= totalPage) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                    }

                    $(".pagination > li").on("click", function () {
                        searchCustomer($(this).val());
                    });
                }
                
            },
            error: function (respond) {
                showNotificationModal('<b>Search Customer</b>', 'Search Customer Failed!');
            },
        });
    } else {
        getData();
    }
}

//Đóng Search
function cancelSearch() {
    $('#search-customer').val('');
    getData();
}

//Gửi E-Mail tới Khách hàng
function openSendEmailToCustomerModal(email) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Send email to customer");
    $("#common-modal .modal-body").empty();
    $("#common-modal .modal-body").append(`
        <div id="form-container">
            <!-- Email input-->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="email">Send to</label>
            <div class="col-md-9">
                <input disabled id="email" name="email" type="text" value="`+ email + `" placeholder="email" class="form-control">
            </div>
            </div>
                
            <div class="form-group" style="border: none;">
                <label class="col-md-3 control-label" for="subject">Subject</label>
                <div class="col-md-9">
                    <input id="subject" name="subject" type="text" placeholder="Subject" class="form-control">
                </div>
            </div>

            <!-- Message body -->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="message">Your message</label>
            <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
            </div>
            </div>

            <!-- Form actions -->
            <div class="form-group" style="border: none;">
                <div class="col-md-12 text-right" id="spin-area">
                    <button id="btn-submit-send-email" type="submit" class="btn btn-primary btn-lg">Submit</button>
                    <div class="load-btn" style="display: none;">
                        <!-- loading animation for table -->
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="color: #00b2ce;"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    `);
    $("#common-modal").modal("show");
    //Click submitSendEmail
    $("#btn-submit-send-email").on("click", function () {
        $("#common-modal-wrap-content .load-btn").css("display", "block");
        $("#btn-submit-send-email").css("display", "none");

        let emailAddress = $("#email").val();
        let subject = $("#subject").val();
        let body = $("#message").val();

        if (emailAddress == '' || subject == '' || body == '') {
            let data = {
                'emailAddress': emailAddress,
                'subject': subject,
                'body': body
            };

            console.log(JSON.stringify(data));

            $.ajax({
                url: "https://localhost:44314/admin/email/commonSendMail",
                method: "POST",
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function (response) {
                    $("#modal-notification .modal-body").empty();
                    $("#modal-notification .modal-body").append("<p style='color: #00acd6;'><b>Send E-Mail Success!</b></p>");
                    $("#common-modal").modal("hide");
                    $("#modal-notification").modal("show");
                    $("#common-modal-wrap-content .load-btn").css("display", "none");
                    $("#btn-submit-send-email").css("display", "block");
                },
                error: function (error) {
                    console.log(error);
                    $("#common-modal-wrap-content .load-btn").css("display", "none");
                    $("#modal-notification .modal-body").append("<p style='color: red;'><b>Send E-Mail Failed!</b></p>");
                    $("#btn-submit-send-email").css("display", "block");
                }
            })
        } else {
            showNotificationModal('<b>Send E-Mail</b>', 'Please Enter All Fields!');
        }
    })
}

//Gửi Email tới TẤT CẢ Khách hàng
function openSendEmailToAllCustomerModal() {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Send E-mail to All Customer");
    $("#common-modal .modal-body").empty();
    $("#common-modal .modal-body").append(`
        <div id="form-container">
            <!-- Email input-->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="email">Send to</label>
            <div class="col-md-9">
                <input disabled id="email-all" name="email" type="text" value="All Customer(s)" placeholder="email" class="form-control">
            </div>
            </div>
                
            <div class="form-group" style="border: none;">
                <label class="col-md-3 control-label" for="subject">Subject</label>
                <div class="col-md-9">
                    <input id="subject-all" name="subject" type="text" placeholder="Subject" class="form-control">
                </div>
            </div>

            <!-- Message body -->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="message">Your message</label>
            <div class="col-md-9">
                <textarea class="form-control" id="message-all" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
            </div>
            </div>

            <!-- Form actions -->
            <div class="form-group" style="border: none;">
                <div class="col-md-12 text-right" id="spin-area">
                    <button id="btn-submit-send-email-all" type="submit" class="btn btn-primary btn-lg">Submit</button>
                    <div class="load-btn" style="display: none;">
                        <!-- loading animation for table -->
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="color: #00b2ce;"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    `);
    $("#common-modal").modal("show");
    //Click submitSendEmail
    $("#btn-submit-send-email-all").on("click", async function () {
        $("#common-modal-wrap-content .load-btn").css("display", "block");
        $("#btn-submit-send-email-all").css("display", "none");

        let subject = $("#subject-all").val();
        let body = $("#message-all").val();

        if (subject == '' || body == '') {
            for (var i = 0; i < customerEmailList.length; i++) {
                let emailAddress = customerEmailList[i];

                let data = {
                    'emailAddress': emailAddress,
                    'subject': subject,
                    'body': body
                };

                console.log(JSON.stringify(data));

                await $.ajax({
                    url: "https://localhost:44314/admin/email/commonSendMail",
                    method: "POST",
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify(data),
                    success: function (response) {
                        $("#modal-notification .modal-body").empty();
                        $("#modal-notification .modal-body").append("<p style='color: #00acd6;'><b>Send E-Mail Success !!!</b></p>");
                        $("#common-modal").modal("hide");
                        $("#modal-notification").modal("show");
                        $("#common-modal-wrap-content .load-btn").css("display", "none");
                        $("#btn-submit-send-email").css("display", "block");
                    },
                    error: function (error) {
                        console.log(error);
                        $("#common-modal-wrap-content .load-btn").css("display", "none");
                        $("#modal-notification .modal-body").append("<p style='color: red;'><b>Send E-Mail Failed! !!!</b></p>");
                        $("#btn-submit-send-email").css("display", "block");
                    }
                })
            }
        } else {
            showNotificationModal('<b>Send E-mail</b>', 'Please Enter All Fields!');
        }
    })
}