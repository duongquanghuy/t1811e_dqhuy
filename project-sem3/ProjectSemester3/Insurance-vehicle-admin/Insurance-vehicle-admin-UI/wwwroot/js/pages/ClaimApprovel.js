﻿let fromDate;
let toDate;
$(document).ready(function () {
    $("table").css("display", "none");
    getData();
});

function ShowImgClaim(id) {
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44314/admin/claim/getAllImg/' + id + '',
        dataType: 'json',
        success: function (result) {

            $.each(result.data, function (index, val) {

                $('#displayImgs').append('<img onclick="showThisImg(' + val.id + ')"  class="myImg' + val.id + ' imgRemove" src=' + val.link + ' alt="Trolltunga, Norway" width = "300"  >');
            });

        },
        error: function () {
            console.log('Error');
        }

    });

}

// Lấy phần Modal
var modal = document.getElementById('myModal-img');
// Lấy đường dẫn của hình ảnh và gán vào trong phần Modal

var modalImg = document.getElementById("img01");

function showThisImg(imgId) {
    modal.style.display = "block";
    modalImg.src = $('.myImg' + imgId + '').attr("src");
    $('#exampleModalCenter').modal('hide');
}
// lấy button span có chức năng đóng Modal
var span = document.getElementsByClassName("close-img")[0];

//Khi button được click, đóng modal
span.onclick = function () {
    modal.style.display = "none";
    $('#exampleModalCenter').modal('show');
}
// xóa các thẻ img
function RemoveImg() {
    $(".imgRemove").remove();
}

function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}

function hideNotificationModal() {
    $('#notification-modal-container').modal('hide');
}
function  searchBillFromCustomer() {
    $('#loader').modal('show');
    var searchValue = $('#searchClaimFromCus').val();
    if (searchValue != '') {
        getData();
    }
    $('#loader').modal('hide');
} 
$('#datePicker').on('apply.daterangepicker', function (ev, picker) {

    fromDate = picker.startDate.format('MM-DD-YYYY');
    toDate = picker.endDate.format('MM-DD-YYYY');
    getData();
});

$('#datePicker').on('cancel.daterangepicker', function (ev, picker) {

    //do something, like clearing an input
    $('#datePicker').val('');
    fromDate = null;
    toDate = null;
    getData();
});

$("#searchClaimFromCus").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
});
//nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 10) {
 
    $('.load-table').css('display', 'flex');
    var searchValue = $('#searchClaimFromCus').val();
    let url = 'https://localhost:44314/admin/claim/claimApprovel?page=' + page + '&pageSize=' + pageSize ;

    if (toDate && fromDate) {
        url = url + "&timeFrom=" + fromDate + "&timeTo=" + toDate;
    }
    if (searchValue != "" && searchValue != null) {
        url = url + "&valueSearch=" + searchValue;
    }
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',

        success: function (result) {

            if (result.data == null || result.data == '') {

                var msgHead = "Announcement";
                var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                showNotificationModal(msgHead, msgBody);
                $('#searchClaimFromCus').val('');
                fromDate = null;
                toDate = null;
                $('.load-table').css('display', 'none');
            } else {
                $("#list-data-table-ClaimApprovel").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {

                    let html = '<tr>' +
                        '<td>' + val.id + '</td>' +
                        '<td>' + val.bodyClaim + '</td>' +
                        '<td>' + val.customName + '</td>' +
                        '<td>' + val.cusPhone + '</td>' +
                        '<td>' + val.cusEmail + '</td>' +
                        '<td>' + formatDateTime(val.createAt) + 'Day</td>' +
                        '<td>' + val.totalValue + '$</td>' +
                        '<td>' +
                        '<div class="wrap-acitons">' +
                        '<a href="#" class="img-button td-link" data-toggle="modal" data-target="#exampleModalCenter" onclick="ShowImgClaim(' + val.claimID + ')">Picture</a>' +
                        '</div>' +
                        '</td>' +
                        '<td style="text-align:center">' +
                        '<button class="btn btn-sm btn-primary" onclick="displayBill(' + val.id + ')" >' +
                        '<i class="fa fa-fw fa-eye"  title="Details"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';

                    $("#list-data-table-ClaimApprovel").append(html);
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');

                });

                let currentPage = result.currentPage;
                let totalPage = result.totalPage;
                if (page > totalPage) {
                    getData(1, 10);
                }
                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })
            }
        }
        ,
            error: function () {

                $("table").css("display", "block");
                $('.load-table').css('display', 'none');
            },

    });

}
function displayBill(id) {
    window.location.replace('/Claim/ClaimDetailPaid?BiilID=' + id);
}
// ///////////////////////////
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}