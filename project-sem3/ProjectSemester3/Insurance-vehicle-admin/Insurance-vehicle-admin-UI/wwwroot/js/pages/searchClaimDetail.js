﻿$(document).ready(function () {
   
    var ClaimId = getUrlParameter('claimId');
    
    $('#Claim-Customer').empty();
    $('#claim-insurance').empty();
    $('#claim-Vehicle').empty();
    $('#claim-Detail').empty();
    $('#claim-bill-of-sale').empty();
    $('.invoice').css('display', 'none');
    $('.load-table').css('display', 'flex');

        
   
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44314/admin/claim/searchUnpaid/' + ClaimId,
        dataType: 'json',
        success: function (result) {
            var claimStatus = result.data.claimStatus;
            var status = "";
            if (claimStatus == 0) {
                status = "Pending";

            } else if (claimStatus == -1) {
                status = "Rejected";
                $('#from-submit-claim').empty();
            }
            else if (claimStatus == 2) {
                status = "Initialize";
            }
            else {
                status = "Unknown";
            }

            $('#max-total-value').val(result.data.maxValueRefund);
            $('#claimID-request').val(result.data.claimId);
            var now = new Date();
            var DateExpiresOn = Date.parse(result.data.expiresOn);
            var offset = DateExpiresOn - now.getTime();

            var totalDays = Math.round(offset / 1000 / 60 / 60 / 24);
            var refundQuantity = parseInt(result.data.maxRefundQuantity) - parseInt(result.data.billcount);

            var dateTimeFormat = formatted(result.data.expiresOn);
            var dateTimeClaim = formatted(result.data.createAt);
            let html_customer = '<div><span>Customer Name : </span>' + result.data.customName + '</div>' +
                '<div> <span>Phone : </span>' + result.data.cusPhone + '</div>' +
                '<div><span>Email : </span>' + result.data.cusEmail + '</div>' +
                '<div><span>Passport : </span>' + result.data.passport + '</div>';
            let html_insurance = '<div><span>InsuranceId : </span>' + result.data.insuranceId + '</div>' +
                '<div> <span>price Percent By Car Value : </span>' + result.data.pricePercentByCarValue + '</div>' +
                '<div><span>Min Value Refund : </span>' + result.data.minValueRefund + '</div>' +
                '<div><span>Max ValueRefund : </span>' + result.data.maxValueRefund + '</div>' +
                '<div><span>Duration Month Unit : </span>' + result.data.durationMonthUnit + '</div>';
            let html_Vehicle = '<div><span>License Plates : </span>' + result.data.licensePlates + '</div>' +
                '<div><span>Vehicle Model Name : </span>' + result.data.vehicleModelName + '</div>' +
                '<div><span>vehicle Version Name : </span>' + result.data.vehicleVersionName + '</div>';
            let html_Detail = '<tr>' +
                '<th> Body Claim:</th>' +
                '<td>' + result.data.bodyClaim + '</td>' +
                '</tr >' +
                '<tr>' +
                '<th>Create Date</th>' +
                '<td>' + dateTimeClaim + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Claim Status</th>' +
                '<td class="color-' + status + '">' + status + '</td>' +
                '</tr>' +

                '<tr>'
            '<th>Refund Qauntity:</th>' +
                '<td>' + refundQuantity + '</td>' +
                '</tr>';
            let html_Bill = '<div><span>insuranceDurationMonthly : </span>' + result.data.insuranceDurationMonthly + '</div>' +
                '<div><span>total Price : </span>' + result.data.totalPrice + ' $</div>' +
                '<div><span>expires On : </span>' + dateTimeFormat + '</div>' +
                '<div><span>Tim Remaining: </span>' + totalDays + '</div>';

            $('#Claim-Customer').append(html_customer);
            $('#claim-insurance').append(html_insurance);
            $('#claim-Vehicle').append(html_Vehicle);
            $('#claim-Detail').append(html_Detail);
            $('#claim-bill-of-sale').append(html_Bill);
            /*     $('.loader').css('display', 'none');*/
            $('.load-table').css('display', 'none');
            $('.invoice').css('display', 'block');
        }
        ,
        error: function () {
            console.log('error');
            window.location.replace('/Claim/UnPaid');
        }

    });
});
//
function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
}
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}
function NumberFormatOnKeyup() {
    var maxValueRefund = $('#max-total-value').val();
    var totalValue = $('#total-value-Bill').val();
    if (parseInt(maxValueRefund) < parseInt(totalValue)) {
        $('#total-value-Bill').css('border-color', '#dd4b39');
        $('#total-value-text-usd').css('border-color', '#dd4b39');
        $('#total-value-text-usd').html('<em style="Color : #dd4b39">Pricing imports are not greater than the premium price regulate</em>');
    } else {
        var price = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        }).format(totalValue);
        $('#total-value-text-usd').html(price);
    }
   
}
// hieb modal gui thong tin compensation
function createBillCompensation() {

    var maxValueRefund = $('#max-total-value').val();
    var totalValue = $('#total-value-Bill').val();
    if (totalValue == "") {
        var msgBody = '<em style="Color : #dd4b39">Total Value is not to be empty!</em>';
        $('#exampleModalAnnounceent').modal('show');
        $('#body-Announceent').html(msgBody);
    } else {
        if (parseInt(maxValueRefund) < parseInt(totalValue)) {
            var msgBody = '<em style="Color : #dd4b39">Pricing imports are not greater than the premium price regulate</em>';
            $('#exampleModalAnnounceent').modal('show');
            $('#body-Announceent').html(msgBody);
        } else {
            var claimId = $('#claimID-request').val()

            var price = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',

            }).format(totalValue);
            var msgHead = 'Claim Detail : ' + claimId;
            var msgBody = 'Total Value : ' + price;
            showNotificationModal(msgHead, msgBody);
        }
    }
}
// xac nhan tao bill compensation
function comfirmCreateBillCompensation() {
    debugger;
    var adminID = getCookie("admmin-id");
    
    var claimId = $('#claimID-request').val();
    var ClaimIdP = parseInt(claimId);
    var totalValue = $('#total-value-Bill').val();
    var totalValueP = parseInt(totalValue);
    var maxValueRefund = $('#max-total-value').val();
    var body = $('#reson-body-claim').val().toString();
    if (parseInt(maxValueRefund) < parseInt(totalValue)) {
        var msgBody = 'Pricing imports are not greater than the premium price regulate';
        $('#exampleModalAnnounceent').modal('show');
        $('#body-Announceent').html(msgBody);

    } else {
        var dataRequest = {
            "EntityId": parseInt(claimId),
            "UserId": adminID,
            "Total": parseInt(totalValue),
            "Reason": body.toString(),
        }
     
        var dataJson = JSON.stringify(dataRequest);
        if (totalValue != "") {
            $.ajax({
                url: 'https://localhost:44314/admin/claim/createBill/',
                type: 'POST',
                data: JSON.stringify(dataRequest),
                dataType: 'json',
                contentType: 'application/json',
                success: function (result) {
                    console.log(result.code);
                    if (result.code == 200) {
                        
                        var msgBody = 'Create Success Claim';
                        $('#exampleModalAnnounceent').modal('show');
                        $('#body-Announceent').html(msgBody);
                        window.location.replace('/Claim/UnPaid');

                    }
                    if (result.code == 300) {

                        var msgBody = 'Insurance has exceeded the number of Claim ';
                        $('#exampleModalAnnounceent').modal('show');
                        $('#body-Announceent').html(msgBody);
                        window.location.replace('/Claim/UnPaid');

                    }

                    hideNotificationModal();
                },
                error: function () {

                 
                    $('#exampleModalAnnounceent').modal('show');
                    var msgBody = 'Create Unsuccessful Claim';
                    $('#body-Announceent').html(msgBody);
                    window.location.replace('/Claim/UnPaid');
                }
            })
        } else {
            $('#exampleModalAnnounceent').modal('show');
            var msgBody = 'Total Value Illeegal';
            $('#body-Announceent').html(msgBody);
            window.location.replace('/Claim/UnPaid');
           
        }
    }
   
}
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}