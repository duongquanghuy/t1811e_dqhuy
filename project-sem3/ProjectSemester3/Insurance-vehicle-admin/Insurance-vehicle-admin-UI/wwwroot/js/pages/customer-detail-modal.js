﻿//Khai báo biến
var detailCustomerId; //Đánh dấu khách hàng đang được gọi tới trong Modal Chi tiết
var chosenVehicleId; //Đánh dấu Xe được Chọn để Tạo đơn
var chosenInsuranceId; //Đánh dấu Bảo hiểm được Chọn để Tạo đơn
var chosenModelId; //Đánh dấu Model được Chọn để Tạo đơn
var chosenVersionId; //Đánh dấu Version được Chọn để Tạo đơn
var insuranceDurationMonthly; //Đánh dấu Thời gian Bảo hiểm được Chọn để Tạo đơn
var addModelId; //Đánh dấu Model Xe được chọn để Thêm Xe cho Khách hàng





//Đổ thông tin của Khách hàng vào Modal
function openCustomerDetailsModal(customerId) {
	detailCustomerId = customerId;

	$.ajax({
		url: 'https://localhost:44314/admin/Customer/searchCustomerById/' + customerId,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			console.log(res.data.length);
			if (res.data.length == 1) {
				$('.details-customer-modal-cusname').html(res.data[0].name);
				$('#details-customer-modal-cusphone').html(res.data[0].phoneNumber);
				$('#details-customer-modal-cusemail').html(res.data[0].email);
				$('#details-customer-modal-cusaddress').html(res.data[0].address);
				$('#details-customer-modal-cuspassport').html(res.data[0].passport);
			} else {
				showNotificationModal('<b>Customer Details</b>', '<b>Get Customer Details Failed!</b>');
            }
		},
		error: function (res) {
			showNotificationModal('<b>Customer Details</b>', '<b>Get Customer Details Failed!</b>');
		},
	});
}





//Vùng Thêm Hóa đơn
function openAddBillForm() {
	$('#add-bill-of-sale-area').css('display', 'block');
	$('#btn-close-add-bill').css('display', 'block');
	$('#btn-open-add-bill').css('display', 'none');

	getInsurance();
	getCustomerVehicleList();
}

function closeAddBillForm(){
	$('#add-bill-of-sale-area').css('display', 'none');
	$('#btn-close-add-bill').css('display', 'none');
	$('#btn-open-add-bill').css('display', 'block');
}

//Lấy dữ liệu bảo hiểm
function getInsurance() {
	$.ajax({
		url: 'https://localhost:44314/admin/Insurance/all',
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			$('#insurance-list').html('');

			$.each(res.data, function (idx, ins) {
				$('#insurance-list').append(
					'<li onclick="chooseInsurance(' + ins.id + ')"><a>' +
					'<b>*' + ins.name + '</b> | *Duration Month: <b>' + ins.durationMonth + '</b> | *Refund Quantity: <b>' + ins.maxRefundQuantity + '</b>' +
					'</a></li>'
				);
			});
		},
		error: function (res) {
			showNotificationModal('<b>Get Insurance</b>', '<b>Get Insurance List Failed!</b>');
		},
	});
}

//Lấy Xe của Khách hàng
function getCustomerVehicleList() {
	$.ajax({
		url: 'https://localhost:44314/api/Vehicle/getVehicleByCustomerId/' + detailCustomerId,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			$('#customer-vehicle-list').html('');
			$('#list-vehicle-table-body').html('');

			if (res.data.length == 0) {
				//Nếu không có dữ liệu thì trả về Thông báo
				$('#customer-vehicle-list').append(
					'<li><a>' +
					'This Customer do not have any Vehicle(s) yet!' +
					'</a></li>'
				);

				//Nếu không có dữ liệu thì ẨN Bảng Danh sách Xe
				$('#list-vehicle-table').css('display', 'none');
				$('#list-vehicle-table-title').html('This Customer do not have any Vehicle(s) yet!');

			} else {
				$('#list-vehicle-table').css('display', 'table');
				$('#list-vehicle-table-title').html('Customer Vehicle(s)');

				$.each(res.data, function (idx, v) {
					//Đổ dữ liệu vào Dropdown
					$('#customer-vehicle-list').append(
						'<li onclick="chooseVehicle(' + v.id + ')"><a>' +
						'<b>*' + v.versionName + '</b> | *License Plate: <b>' + v.licensePlates + '</b>' +
						'</a></li>'
					);

					//Đổ dữ liệu vào Bảng Danh sách Xe của Khách hàng
					$('#list-vehicle-table-body').append(
						'<tr>' +
						'<td>' + v.versionName + '</td>' +
						'<td class="td-link" onclick="goToVehicleDetails(' + v.id + ')">' + v.licensePlates + '</td>' +
						'<td>' + v.bodyNumber + '</td>' +
						'<td>' + v.engineNumber + '</td>' +
						'<td>' + formatted(v.purchaseDate) + '</td>' +
						'<tr>'
					);
				});
            }
		},
		error: function (res) {
			showNotificationModal('<b>Get Vehicle</b>', '<b>Get Vehicle List Failed!</b>');
		},
	});
}

//Chọn Xe để Tạo Hóa đơn
function chooseVehicle(vehicleId) {
	chosenVehicleId = vehicleId;

	$.ajax({
		url: 'https://localhost:44314/api/Vehicle/getVehicleById/' + chosenVehicleId,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			//Hiển thị vùng Xe được chọn
			$('#chosen-vehicle-area').css('display', 'block');

			$('#chosen-vec-model').html(res.data.modelName);
			$('#chosen-vec-license-plate').html(res.data.licensePlates);
			$('#chosen-vec-version').html(res.data.versionName);
			$('#chosen-vec-engine-number').html(res.data.engineNumber);
			$('#chosen-vec-body-number').html(res.data.bodyNumber);

			chosenModelId = res.data.modelId;
			chosenVersionId = res.data.versionId;

			showNotificationModal('<b>Choose Vehicle<b/>', 'Choose Vehicle <b>' + res.data.versionName + '</b> Success!');

			//setTimeout(function () { getPriceInsurance() }, 1000);
			getPriceInsurance();
		},
		error: function (res) {
			showNotificationModal('<b>Choose Vehicle</>', '<b>Choose Vehicle Failed!</b>');
		},
	});
}

//Đóng Vùng chọn Xe
function hideVehicleArea() {
	$('#chosen-vehicle-area').css('display', 'none');

	$('#chosen-vec-model').html('');
	$('#chosen-vec-license-plate').html('');
	$('#chosen-vec-version').html('');
	$('#chosen-vec-engine-number').html('');
	$('#chosen-vec-body-number').html('');

	chosenVehicleId = null;
}

//Chọn Bảo hiểm để Tạo Hóa đơn
function chooseInsurance(insuranceId) {
	chosenInsuranceId = insuranceId;

	$.ajax({
		url: 'https://localhost:44314/admin/Insurance/search/' + chosenInsuranceId,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			//Hiển thị vùng Bảo hiểm được chọn
			$('#chosen-insurance-area').css('display', 'block');

			$('#chosen-insurance-name').html(res.data.insuranceName);
			$('#price-percent-by-car-value').html(res.data.pricePercentByCarValue + '%');
			$('#min-value-refund').html(res.data.minValueRefund + '$');
			$('#max-value-refund').html(res.data.maxValueRefund + '$');
			$('#max-refund-quantity').html(res.data.maxRefundQuantity + ' time(s)');
			$('#duration-month-unit').html(res.data.durationMonthUnit + ' month(s)');

			insuranceDurationMonthly = res.data.durationMonthUnit;

			showNotificationModal('<b>Choose Insurance <b/>', 'Choose Insurance <b>' + res.data.insuranceName + '</b> Success!');

			//setTimeout(function () { getPriceInsurance() }, 1000);
			getPriceInsurance();
		},
		error: function (res) {
			showNotificationModal('<b>Choose Insurance</>', '<b>Choose Insurance Failed!</b>');
		},
	});
}

//Đóng Vùng chọn Bảo hiểm
function hideInsuranceArea() {
	$('#chosen-insurance-area').css('display', 'none');

	$('#chosen-insurance-name').html('');
	$('#price-percent-by-car-value').html('');
	$('#min-value-refund').html('');
	$('#max-value-refund').html('');
	$('#max-refund-quantity').html('');
	$('#duration-month-unit').html('');

	chosenInsuranceId = null;
}

//Vùng Thêm Xe cho Khách hàng
	function openAddVehicleForm() {
		$('#add-vehicle-area').css('display', 'block');
		$('#btn-close-add-vehicle-form').css('display', 'block');
		$('#btn-open-add-vehicle-form').css('display', 'none');
	}

	function closeAddVehicleForm() {
		$('#add-vehicle-area').css('display', 'none');
		$('#btn-close-add-vehicle-form').css('display', 'none');
		$('#btn-open-add-vehicle-form').css('display', 'block');

		$('#add-body-number').val('');
		$('#add-engine-number').val('');
		$('#add-license-plate').val('');
		$('#add-version').html('');
		$('#add-purchase-date').val('');
		$('#add-model').val('');
	}

	//Lấy các Model phương tiện mà Bảo hiểm hỗ trợ
	function getVehicleModel() {
		var searchString = $('#add-model').val();

		if (searchString != '') {
			$.ajax({
				url: 'https://localhost:44314/api/Vehicle/searchVehicleModelByName/' + searchString,
				type: 'GET',
				dataType: 'json',
				success: function (data) {
					$('#search-result-area').css('display', 'block');

					$('#search-result-list').css('display', 'block');
					$('#search-result-list').html('');//đưa list về rỗng trước

					if (data.data.length == 0) {
						$('#search-result-list').append("<li><b>We don't support this Model!</b></li>");
					} else {
						$.each(data.data, function (index, item) {
							$('#search-result-list').append(
								'<li onclick="getVehicleVersionByModel(' + item.id + ', \'' + item.name + '\')">' + item.name + '</li>'
							);
						});
					}
				},
				error: function () {
					showNotificationModal("<b>Get Vehicle Model</b>", "<b>Get Vehicle Models Failed!</b>");
				},
			});
		} else {
			hideSearchResult();
		}
	}

	//Ẩn đi các kết quả về Model được tìm kiếm
	function hideSearchResult() {
		setTimeout(function () {
			$('#search-result-list').css('display', 'none');
			$('#search-result-list').html('');
			$('#new-vehicle-version').html('');
		}, 100);
	}

	//Lấy các Version phương tiện theo Model được chọn
	function getVehicleVersionByModel(modelId, modelName) {
		addModelId = modelId;

		$.ajax({
			url: 'https://localhost:44314/api/Vehicle/versionByVehicleModel/' + addModelId,
			type: 'GET',
			dataType: 'json',
			success: function (data) {
				$('#add-version').html('');//đưa list option về rỗng trước

				$('#search-result-area').css('display', 'none');

				$('#add-model').val(modelName);

				$('#search-result-list').html('');//đưa list về rỗng

				if (data.data.length == 0) {
					$('#btn-add-vehicle').css('display', 'none');
					$('#none-version-warning').css('display', 'block');

				} else {
					$.each(data.data, function (index, item) {//đẩy các version vào list option
						$('#add-version').append(
							'<option value="' + item.id + '">' + item.name + '</option>'
						);
					});

					$('#btn-add-vehicle').css('display', 'block');
					$('#none-version-warning').css('display', 'none');
				}
			},
			error: function () {
				showNotificationModal("<b>Get Vehicle Model's Version</b>", "<b>Get Vehicle Model's Version Failed!</b>");
			},
		});
	}

	//Thêm Xe cho Khách hàng
	function addCustomerVehicle() {
		var customerId = detailCustomerId;
		var bodyNumber = $('#add-body-number').val();
		var engineNumber = $('#add-engine-number').val();
		var licensePlate = $('#add-license-plate').val();
		var modelId = addModelId;
		var versionId = $('#add-version').val();
		var purchaseDate = $('#add-purchase-date').val();

		if (customerId == '' || bodyNumber == '' || engineNumber == '' || licensePlate == '' || modelId == '' || versionId == '' || purchaseDate == '') {
			showNotificationModal('<b>Add Customer Vehicle</b>', '<b>Please Enter All Field(s)</b>');
		} else {
			var newVehicle = {
				"CustomerId": customerId,
				"PurchaseDate": purchaseDate,
				"BodyNumber": bodyNumber,
				"EngineNumber": engineNumber,
				"LicensePlates": licensePlate,
				"ModelId": modelId,
				"VersionId": parseInt(versionId)
			}

			$.ajax({
				url: 'https://localhost:44314/api/Vehicle/addCustomerVehicle',
				type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify(newVehicle),
				dataType: 'json',
				success: function (data) {
					showNotificationModal("<b>Add Vehicle for Customer</b>", "Add Vehicle <b>" + licensePlate + "</b> Success!");
				},
				error: function () {
					showNotificationModal("<b>Add Vehicle for Customer</b>", "Add Vehicle Failed!");
				},
			});

			closeAddVehicleForm()
			getCustomerVehicleList(); //Lấy lại dữ liệu Xe của Khách hàng
        }
	}

//Gọi Api Tính Giá
function getPriceInsurance() {
	if (chosenInsuranceId != null && chosenVehicleId != null) {
		//Hiển thị Vùng Tính Giá Bảo hiểm
		$('#create-bill-area').css('display', 'block');

		var yearsOfUsedToBill = $('#years-of-used').val();
		var quantityToBill = $('#insurance-quantity').val();

		$.ajax({
			url: 'https://localhost:44314/admin/BillOfSale/priceInsurance?insuranceId=' + chosenInsuranceId +
				'&modelId=' + chosenModelId + '&vesionId=' + chosenVersionId + '&quantityYearUsed=' + yearsOfUsedToBill + '&quantity=' + quantityToBill,
			type: 'GET',
			dataType: 'json',
			contentType: 'application/json',
			success: function (res) {
				$('#add-bill-price-value').html(res.data + '$');
			},
			error: function (res) {
				showNotificationModal('<b>Get Price Insurance</b>', "<b>Get Price Insurance Failed!</b>");
			},
		});
	} else {
		$('#create-bill-area').css('display', 'none');
		$('#add-bill-price-value').html('');
    }
}

//Tạo Hóa Đơn
function createBillAction() {
	var yearsOfUsedToBill = $('#years-of-used').val();
	var quantityToBill = $('#insurance-quantity').val();

	var newBill = {
		"CustomerId": detailCustomerId,
		"VerhicleId": parseInt(chosenVehicleId),
		"InsuranceDurationMonthly": parseInt(insuranceDurationMonthly),
		"InsuranceId": parseInt(chosenInsuranceId),
		"modelId": parseInt(chosenModelId),
		"vesionId": parseInt(chosenVersionId),
		"quantityYearUsed": parseInt(yearsOfUsedToBill),
		"quantity": parseInt(quantityToBill)
	}

	 console.log(JSON.stringify(newBill));

	$.ajax({
		url: 'https://localhost:44314/admin/BillOfSale/postBillOfSale/',
		type: 'POST',
		data: JSON.stringify(newBill),
		contentType: 'application/json',
		dataType: 'application/json',
		async: false,
		success: function (res, status) {
			console.log(res);
			alert('Add Bill ' + status + ' !');
			showNotificationModal('<b>Create Bill</b>', '<b>' + res.Message + '</b>');

			$('#create-bill-area').css('display', 'none');
			$('#add-bill-price-value').html('');
			hideInsuranceArea();
			hideVehicleArea();
			closeAddBillForm();
		},
		error: function (res) {
			showNotificationModal('<b>Create Bill</b>', '<b>' + res.Message + '</b>');
		},
	});	
}

//Thanh toán Offline
function paymentOfflineAction() {
	var yearsOfUsedToBill = $('#years-of-used').val();
	var quantityToBill = $('#insurance-quantity').val();

	var newBill = {
		"CustomerId": detailCustomerId,
		"VerhicleId": parseInt(chosenVehicleId),
		"InsuranceDurationMonthly": parseInt(insuranceDurationMonthly),
		"InsuranceId": parseInt(chosenInsuranceId),
		"modelId": parseInt(chosenModelId),
		"vesionId": parseInt(chosenVersionId),
		"quantityYearUsed": parseInt(yearsOfUsedToBill),
		"quantity": parseInt(quantityToBill)
	}

	console.log(JSON.stringify(newBill));

	$.ajax({
		url: 'https://localhost:44314/admin/billOfsale/postBillOfSaleOffline/',
		type: 'POST',
		data: JSON.stringify(newBill),
		contentType: 'application/json',
		dataType: 'application/json',
		async: false,
		success: function (res, status) {
			console.log(res);
			alert('Payment Offline ' + status + ' !');
			showNotificationModal('<b>Payment Offline</b>', '<b>' + res.Message + '</b>');

			$('#create-bill-area').css('display', 'none');
			$('#add-bill-price-value').html('');
			hideInsuranceArea();
			hideVehicleArea();
			closeAddBillForm();
		},
		error: function (res) {
			showNotificationModal('<b>Payment Offline</b>', '<b>' + res.Message + '</b>');
		},
	});
}

//Lựa chọn giữa Thanh toán Offline và Tạo Đơn hàng Online
function pocSelection() {
	if ($('#create-bill-radio').prop("checked")) {
		$('#btn-create-bill').css('display', 'block');
		$('#btn-payment-offline').css('display', 'none');
	} else {
		$('#btn-create-bill').css('display', 'none');
		$('#btn-payment-offline').css('display', 'block');
    }
}



//Vùng Danh sách Bill của Khách hàng
function openListBillForm() {
	$('#bill-list-area').css('display', 'block');
	$('#btn-close-list-bill').css('display', 'block');
	$('#btn-open-list-bill').css('display', 'none');

	getListBillOfCustomer();
}

function closeListBillForm() {
	$('#bill-list-area').css('display', 'none');
	$('#bill-list-table').css('display', 'none');
	$('#bill-list-table-body').html('');
	$('#btn-close-list-bill').css('display', 'none');
	$('#btn-open-list-bill').css('display', 'block');
}

//Lấy Danh sach Hóa đơn của Khách hàng
function getListBillOfCustomer() {
	$.ajax({
		url: 'https://localhost:44314/admin/BillOfSale/getBillOfSaleByCustomerId/' + detailCustomerId,
		type: 'GET',
		dataType: 'json',
		contentType: 'application/json',
		success: function (res) {
			$('#bill-list-table').css('display', 'none');
			$('#bill-list-table-body').html('');

			if (res.data.length == 0) {
				$('#bill-list-table').css('display', 'none');
				$('#customer-bill-list-title').html('This Customer do not have any Bill(s) yet!');
			} else {
				$('#bill-list-table').css('display', 'table');
				$('#customer-bill-list-title').html('Customer Insurance Bill(s)');

				$.each(res.data, function (idx, b) {
					if (b.paymentStatus == 1) {
						$('#bill-list-table-body').append('<tr>' +
							'<td class="td-link" onclick="goToBillDetails(' + b.billId + ')">' + b.billId + '</td>' +
							'<td class="td-link" onclick="goToInsuranceDetails(' + b.insuranceId + ')">' + b.insuranceName + '</td>' +
							'<td>' + b.versionName + '</td>' +
							'<td class="td-link" onclick="goToVehicleDetails(' + b.vehicleId + ')">' + b.licensePlate + '</td>' +
							'<td>' + b.totalPrice + '$</td>' +
							'<td class="paid">Paid</td>' +
							'<td>' + formatted(b.createdAt) + '</td>' +
							'</tr');
					} else {
						$('#bill-list-table-body').append('<tr>' +
							'<td class="td-link" onclick="goToBillDetails(' + b.billId + ')">' + b.billId + '</td>' +
							'<td class="td-link" onclick="goToInsuranceDetails(' + b.insuranceId + ')">' + b.insuranceName + '</td>' +
							'<td>' + b.versionName + '</td>' +
							'<td class="td-link" onclick="goToVehicleDetails(' + b.vehicleId + ')">' + b.licensePlate + '</td>' +
							'<td>' + b.totalPrice + '$</td>' +
							'<td class="unpaid">Unpaid</td>' +
							'<td>' + formatted(b.createdAt) + '</td>' +
							'</tr');
					}
				});
            }
		},
		error: function (res) {
			showNotificationModal('<b>Get Bill List</b>', '<b>Get Bill List Success!</b>');
		},
	});
}




//Vùng Danh sách Xe của Khách hàng
function openListVehicleForm() {
	$('#list-vehicle-area').css('display', 'block');
	$('#btn-close-list-vehicle').css('display', 'block');
	$('#btn-open-list-vehicle').css('display', 'none');

	getCustomerVehicleList();
}

function closeListVehicleForm() {
	$('#list-vehicle-area').css('display', 'none');
	$('#btn-close-list-vehicle').css('display', 'none');
	$('#btn-open-list-vehicle').css('display', 'block');
}

//Đóng Modal Details
function hideCustomerDetailModal() {
	//Đóng Toàn bộ Vùng làm việc trong Modal Chi tiết Khách hàng
	closeListVehicleForm();
	closeListBillForm();
	closeAddBillForm();

	//Reset toàn bộ biến
	detailCustomerId = null;
	chosenVehicleId = null;
	chosenInsuranceId = null;
	chosenModelId = null;
	chosenVersionId = null;
	insuranceDurationMonthly = null;
}