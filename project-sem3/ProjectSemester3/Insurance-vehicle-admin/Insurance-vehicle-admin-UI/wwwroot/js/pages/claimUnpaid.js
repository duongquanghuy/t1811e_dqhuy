﻿let fromDate;
let toDate;

$(document).ready(function () {
    $("table").css("display", "none");

    getData();
});


function ShowImgClaim(id) {
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44314/admin/claim/getAllImg/' + id + '',
        dataType: 'json',
        success: function (result) {

            $.each(result.data, function (index, val) {

                $('#displayImgs').append('<img onclick="showThisImg(' + val.id + ')"  class="myImg' + val.id + ' imgRemove" src=' + val.link + ' alt="Trolltunga, Norway" width = "300" >');
            });

        },
        error: function () {
            console.log('Error');
        }

    });

}

// Lấy phần Modal
var modal = document.getElementById('myModal-img');
// Lấy đường dẫn của hình ảnh và gán vào trong phần Modal

var modalImg = document.getElementById("img01");

function showThisImg(imgId) {
    modal.style.display = "block";
    modalImg.src = $('.myImg' + imgId + '').attr("src");
    $('#exampleModalCenter').modal('hide');
}
// lấy button span có chức năng đóng Modal
var span = document.getElementsByClassName("close-img")[0];

//Khi button được click, đóng modal
span.onclick = function () {
    modal.style.display = "none";
    $('#exampleModalCenter').modal('show');
}
// xóa các thẻ img
function RemoveImg() {
    $(".imgRemove").remove();
}

function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}

function hideNotificationModal() {
   
  $('#notification-modal-container').modal('hide');
}
//hien thi trang thai da gui tin nhan
function importClaimClaimStatus() {

    var status = $('#claim-status').val();
    $('#loader').modal('show');
    switch (parseInt(status)) {
        case 0:
            console.log(status);
            $('#claim-status').val(0);
            $('#claim-status').css('background-color', '#00c0ef');
            getData();
            $('#loader').modal('hide');
            break;
        case 2:
            $('#claim-status').val(2);
            $('#claim-status').css('background-color', '#f39c12');
            getData();
            $('#loader').modal('hide');
            break
        case -1:
            $('#claim-status').val(-1);
            $('#claim-status').css('background-color', '#c0392b');
            getData();
            $('#loader').modal('hide');
            break
        default:
            $('#claim-status').val(0);
            $('#claim-status').css('background-color', '#00c0ef');
            $('#loader').modal('hide');
            break
    }
    
}
// tìm kiém claim thoe 3 tuong email, sdt ,ten
function searchClaimFromCustomer() {
    var searchValue = $('#searchClaimFromCus').val();
    if (searchValue != '' ) {
        getData();
    }
}
$("#searchClaimFromCus").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
});
$('#datePicker').on('apply.daterangepicker', function (ev, picker) {
   
    fromDate = picker.startDate.format('MM-DD-YYYY');
    toDate = picker.endDate.format('MM-DD-YYYY');
    getData();
});

$('#datePicker').on('cancel.daterangepicker', function (ev, picker) {

    //do something, like clearing an input
    $('#datePicker').val('');
    fromDate = null;
    toDate = null;
    getData();
});
//nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 10) {
    var status = $('#claim-status').val();
    $('.load-table').css('display', 'flex');

    var searchValue = $('#searchClaimFromCus').val();
    let url = 'https://localhost:44314/admin/claim/claimUnpaid?page=' + page + '&pageSize=' + pageSize + '&status=' + status;

    if (toDate && fromDate) {
        url = url + "&timeFrom=" + fromDate + "&timeTo=" + toDate;
    }
    if (searchValue != "" && searchValue != null) {
        url = url + "&valueSearch=" + searchValue;
    }
    
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',

        success: function (result) {
            //khi du lieu null
            if (result.data == null || result.data =="") {
               
                    $('#claim-status').val(parseInt(status));

                    switch (parseInt(status)) {
                        case 0:
                           
                            $('#claim-status').val(0);
                            $('#claim-status').css('background-color', '#00c0ef');
                         
                            break;
                        case 2:
                            $('#claim-status').val(2);
                            $('#claim-status').css('background-color', '#f39c12');
                       
                            break
                        case -1:
                            $('#claim-status').val(-1);
                            $('#claim-status').css('background-color', '#c0392b');
                            break
                        default:
                            $('#claim-status').val(0);
                            $('#claim-status').css('background-color', '#00c0ef');
                            break
                    }
                  
                var msgHead = "Announcement";
                var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                fromDate = null;
                toDate = null;
                $('#searchClaimFromCus').val('');
                showNotificationModal(msgHead, msgBody);
                $('.load-table').css('display', 'none');
            } else {
                $("#list-data-table-ClaimUnpaid").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {

                    /*   <th> Billcount MaxRefundQuantity</th>*/
                    var refundQuantity = parseInt(val.maxRefundQuantity) - parseInt(val.billcount);
                    var claimStatus = val.claimStatus;
                    var status = "";
                    if (claimStatus == 0) {
                        status = "Pending";

                    } else if (claimStatus == -1) {
                        status = "Rejected";
                        $('.dejected-none').css('display', 'none');
                    }
                    else if (claimStatus == 2) {
                        status = "Accepted";
                    }
                    else {
                        status = "Unknown";
                    }
                    var dateTimeFormat = formatDateTime(val.createAt);
                    let html = '<tr>' +
                        '<td>' + val.id + '</td>' +
                        '<td class="text-ellipsis">' + val.bodyClaim + '</td>' +
                        '<td>' + val.customName + '</td>' +
                        '<td>' + val.cusPhone + '</td>' +
                        '<td>' + val.cusEmail + '</td>' +
                        '<td><span class="color-' + status + '">' + status + '</span></td>' +
                        '<td>' + dateTimeFormat + '</td>' +
                        '<td>' + refundQuantity + '</td>' +
                        '<td>' +
                        '<div class="wrap-acitons">' +
                        '<a href="#" class="img-button td-link" data-toggle="modal" data-target="#exampleModalCenter" onclick="ShowImgClaim(' + val.id + ')">Picture</a>' +
                        '</div>' +
                        '</td>' +
                        '<td style="text-align:center">' +
                        '<button class="btn btn-sm btn-primary" onclick="displayBill(' + val.id + ')" style="margin-left: 7px">' +
                        '<i class="fa fa-fw fa-eye"  title="Details"></i>' +
                        '</button>' +
                        `<button class="btn btn-sm btn-warning" style="margin-right: 7px; margin-left: 7px " onclick="SendEmail(` + val.id + `,'` + val.cusEmail+ `')">`+
                        '<i class="fa fa-fw  fa-paper-plane" title="Send Email"></i>' +
                        '</button>' +
                        `<button class="btn btn-sm btn-danger dejected-none" onclick="DejectedClaim(` + val.id + `,'` + val.cusEmail +`')">` +
                        '<i class="fa fa-fw  fa-close" title="Dejected"></i>' +
                        '</button>' +
                        '</td>' +
                        '</tr>';

                    $("#list-data-table-ClaimUnpaid").append(html);
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                    if (claimStatus == -1) {
                        $('.dejected-none').remove();
                    } else {
                        $('.Claim-recovery').remove();
                    }
                });

                let currentPage = result.currentPage;
                let totalPage = result.totalPage;
                if (page > totalPage) {
                    getData(1, 10);
                }
                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                    <a class="page-link" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                    </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
                </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                });
                // Handle click on "Select all" control
                $('#example-select-all').on('click', function () {
                    // Get all rows with search applied
                    var rows = table.rows({ 'search': 'applied' }).nodes();
                    // Check/uncheck checkboxes for all rows in the table
                    $('input[type="checkbox"]', rows).prop('checked', this.checked);
                });

            }
        },
        error: function () {

            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
            $('#searchClaimFromCus').val('');
        }

    });  
 };


function displayBill(id) {
    window.location.replace('/Claim/Detail?claimId=' + id );
}
////////////// add Bill Compensation ////////

// ///////////////////////////
/*function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}*/
function SendEmail(id, cusEmail) {
   
    $('#heading-body-email').val('');
     $('#message-body-email').val('');

    $('#exampleModalEamil').modal('show');
    $('#claimIdSearchEamil').val(id);
    $('.moadl-hesding-email').html('Send Email To Claim ID :' + id + ' Email : ' + cusEmail );

 
}
function ConfirmSendEmail() {
    $('#exampleModalEamil').modal('hide');
    $('.loader').modal('show');
    var claimIDr = $('#claimIdSearchEamil').val();
    var heading = $('#heading-body-email').val();
    var body = $('#message-body-email').val();
    if (body != "" && heading != "") {

        var dataRequest = {
            claimId: parseInt(claimIDr),
            subject: heading,
            body: body
        }
        var dataRequestJSON = JSON.stringify(dataRequest);
        $.ajax({
            url: 'https://localhost:44314/admin/email/sendEmailClaim',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dataRequestJSON,
            success: function (result) {
                $('#exampleModalEamil').modal('hide');
                $('.loader').modal('hide');
                var  msgHead = "Enail Success";
                var msgBody = '<div class="row">' +
                    '<div class="col-lg-6"><label>Customer Name : </label><span> ' + result.data.name + '</span></div>' +
                    '<div class="col-lg-6"><label>Customer Email : </label><span> ' + result.data.email + '</span></div>' +
                    '</div>';
                showNotificationModal(msgHead, msgBody);
                setTimeout(location.reload(), 5000);
            },
            error: function (reponst) {
                console.log("error");
                $('.loader').modal('hide');
                $('#exampleModalEamil').modal('hide');
                $('.loader').modal('hide');
                if (reponst.code == 300) {
                    var msgHead = "Enail Error";
                    var msgBody = '' + reponst.message +'';
                    showNotificationModal(msgHead, msgBody);
                }
               
            }
        });
    } else {
       
        $('.loader').modal('hide');
        $('#exampleModalEamil').modal('hide');
        var msgHead = "Enail ";
        var msgBody = 'Email Cannot be Empty';
        showNotificationModal(msgHead, msgBody);
    }

    
}

//.DejectedClaim
function DejectedClaim(id, cusEmail) {
   
    $('#heading-body-email-dejected').val('');
    $('#message-body-email-dejected').val('');


    $('#exampleModalEamilDejected').modal('show');
    $('#dejected-claim').html('Send Email Dejected Claim ID: ' + id + ' Email : ' + cusEmail );
    $('#dejected-claim-ID').val(id);
}
function confirmDejectedClaim() {
    $('#exampleModalEamilDejected').modal('hide');
    $('.loader').modal('show');
    var heading = $('#heading-body-email-dejected').val();
    var body = $('#message-body-email-dejected').val();
    var ClaimID = $('#dejected-claim-ID').val();
    if (body != "" && heading != "") {
        var DataRequest = {
            "claimId": parseInt(ClaimID),
            "subject": heading,
            "body": body
        }
        $.ajax({
            url: 'https://localhost:44314/admin/claim/dejected',
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(DataRequest),
            success: function (result) {
                if (result.data != null && result.data != '') {
                
                        var msgHead = "Dejecte Claim";
                        var msgBody = '<div class="row">' +
                            '<div class="col-lg-6"><label>Customer Name : </label><span> ' + result.data.name + '</span></div>' +
                             '<div class="col-lg-6"><label>Customer Email : </label><span> ' + result.data.email + '</span></div>' +
                            '</div>';
                    $('.loader').modal('hide');
                    showNotificationModal(msgHead, msgBody);
                    setTimeout(location.reload(), 5000);
                } else {
                    $('.loader').modal('hide');
                    var msgHead = "Enail ";
                    var msgBody = 'Send Email Not Found';
                    showNotificationModal(msgHead, msgBody);
                }
               
            },
            error: function (reponst) {
                var msgHead = "Enail Error";
                var msgBody = '' + reponst.message + '';
                $('.loader').modal('hide');
                showNotificationModal(msgHead, msgBody);
            }
        });
    } else {
        $('.loader').modal('hide');
        var msgHead = "Enail ";
        var msgBody = 'Email Cannot be Empty';
        showNotificationModal(msgHead, msgBody);
    }
}