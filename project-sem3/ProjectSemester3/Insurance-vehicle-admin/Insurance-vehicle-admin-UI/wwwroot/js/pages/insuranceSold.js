﻿//Đổ dữ liệu vào bảng

let fromDate;
let toDate;

getData();
$("#type-insurance").on("change", function () {
    getData();
})

$('#datePicker').on('apply.daterangepicker', function (ev, picker) {
    fromDate = picker.startDate.format('MM-DD-YYYY');
    toDate = picker.endDate.format('MM-DD-YYYY');
    getData();
});

$('#datePicker').on('cancel.daterangepicker', function (ev, picker) {
    //do something, like clearing an input
    $('#datePicker').val('');
    fromDate = null;
    toDate = null;
    getData();
});
function getData(page = 1, pageSize = 10) {
    $('.load-table').css('display', 'flex');
    let type = $("#type-insurance").val();
    let url = 'https://localhost:44314/admin/BillOfSale/insurance-sold?page=' + page + '&pageSize=' + pageSize;
    if (type !== 0 && type) {
        url = url + "&type=" + type;
        console.log(url);
        console.log(type)
    }
    if (toDate && fromDate) {
        url = url + "&timeFrom=" + fromDate + "&timeTo=" + toDate;
    }
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            $('#list-data-table-model').html('');

            if (response.data.length !== 0) {
                $(".pagination").html('');
                console.log(response.data);
                $.each(response.data, function (index, item) {
                    $('#list-data-table-model').append(
                        `<tr>
                            <td class="td-link" onClick="showInfoInsurance(` + item.insuranceId + `)">` + item.insuranceName + `</td>
                            <td>`+ formatDateTime(item.activationDate) + `</td>
                            <td class="td-link" onClick="showInfoCustomer('`+ item.customerEmail + `')">` + item.customerEmail + `</td>
                            <td>`+ item.price + `</td>
                            <td class="td-link" onClick="showInfoVehicle(`+ item.vehicleid + `)">` + item.vehicleid + `</td>
                            <td class="td-tag">`+ item.daysRemaining + ` (days)` + renderTagDaysRemaining(item.daysRemaining) + `</td>
                            <td>`+ `<button class="btn btn-sm btn-info" onClick="showFormSendEmail('` + item.customerEmail + `')"><i class="fa fa-envelope" title="Send Email"></i> Send E-Mail</button>` + `</td>
                        </tr>`
                    );
                });

                $("table").css("display", "inline-table");

                let currentPage = response.currentPage;
                let totalPage = response.totalPage;

                if (page > totalPage) {
                    getData(1, 10);
                }
                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                                                <a class="page-link" aria-label="Previous">
                                                    <span aria-hidden="true">&laquo;</span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                                                <a class="page-link" aria-label="Next">
                                                     <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>`)
                }
                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })
            }
            $('.load-table').css('display', 'none');
        },
        error: function () {
            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
        },
    });
}

function renderTagDaysRemaining(days) {
    if (days <= 30 && days > 0) {
        return `<span class="label label-warning">About to expire</span>`;
    } else if (days > 30) {
        return `<span class="label label-success">New</span>`;
    } else if (days <= 0) {
        return `<span class="label label-danger">Expired</span>`;
    }
}

function showFormSendEmail(email) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Send email to customer");
    $("#common-modal .modal-body").empty();
    $("#common-modal .modal-body").append(`
        <div id="form-container">
            <!-- Email input-->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="email">Send to</label>
            <div class="col-md-9">
                <input disabled id="email" name="email" type="text" value="`+ email + `" placeholder="email" class="form-control">
            </div>
            </div>
                
            <div class="form-group" style="border: none;">
                <label class="col-md-3 control-label" for="subject">Subject</label>
                <div class="col-md-9">
                    <input id="subject" name="subject" type="text" placeholder="Subject" class="form-control">
                </div>
            </div>

            <!-- Message body -->
            <div class="form-group" style="border: none;">
            <label class="col-md-3 control-label" for="message">Your message</label>
            <div class="col-md-9">
                <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
            </div>
            </div>

            <!-- Form actions -->
            <div class="form-group" style="border: none;">
                <div class="col-md-12 text-right" id="spin-area">
                    <button id="btn-submit-send-email" type="submit" class="btn btn-primary btn-lg">Submit</button>
                   <div class="load-btn" style="display: none;">
                        <!-- loading animation for table -->
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw" style="color: #00b2ce;"></i>
                        <span class="sr-only">Loading...</span>
                </div>
                </div>
            </div>
        </div>
    `);
    $("#common-modal").modal("show");
    //Click submitSendEmail
    $("#btn-submit-send-email").on("click", function () {
        $("#common-modal-wrap-content .load-btn").css("display", "block");
        $("#btn-submit-send-email").css("display", "none");
        let emailAddress = $("#email").val();
        let subject = $("#subject").val();
        let body = $("#message").val();

        let data = {
            'emailAddress': emailAddress,
            'subject': subject,
            'body': body
        };
        console.log(JSON.stringify(data));
        $.ajax({
            url: "https://localhost:44314/admin/email/commonSendMail",
            method: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function (response) {
                $("#modal-notification .modal-body").empty();
                $("#modal-notification .modal-body").append("<p style='color: #00acd6;'>Send email success !!!</p>");
                $("#common-modal").modal("hide");
                $("#modal-notification").modal("show");
                $("#common-modal-wrap-content .load-btn").css("display", "none");
                $("#btn-submit-send-email").css("display", "block");
            },
            error: function (error) {
                console.log(error);
                $("#common-modal-wrap-content .load-btn").css("display", "none");
                $("#modal-notification .modal-body").append("<p style='color: red;'>Send email success !!!</p>");
                $("#btn-submit-send-email").css("display", "block");
            }
        })
    })
    //
}



function showInfoInsurance(insuranceId) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Insurance information");
    $("#common-modal .modal-body").empty();
    $.ajax({
        url: "https://localhost:44314/admin/insurance/search/" + insuranceId,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            let data = response.data;
            console.log(response);
            $("#common-modal .modal-body").append(`
                 <div class="form-group">
                    <label class="control-label">Insurance Id</label>
                    <label class="control-label">`+ data.id + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Insurance name</label>
                    <label class="control-label">`+ data.insuranceName + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Duration</label>
                    <label class="control-label">` + data.durationMonthUnit + ` (months)</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Maximum number of refunds</label>
                    <label class="control-label">` + data.maxRefundQuantity + ` (times)</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Maximum value refund</label>
                    <label class="control-label">`+ data.maxValueRefund + ` $</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Price percent by car value</label>
                    <label class="control-label">`+ data.pricePercentByCarValue + `%</label>
                </div>
            `);
            $("#common-modal").modal("show");
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function showInfoCustomer(email) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Customer information");
    $("#common-modal .modal-body").empty();
    $.ajax({
        url: "https://localhost:44314/admin/customer/searchCustomerByEmail?email=" + email,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            let data = response.data[0];
            console.log(response);
            $("#common-modal .modal-body").append(`
                 <div class="form-group">
                    <label class="control-label">Passport</label>
                    <label class="control-label">`+ data.passport + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Full name</label>
                    <label class="control-label">`+ data.name + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Phone number</label>
                    <label class="control-label">` + data.phoneNumber + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <label class="control-label">` + data.email + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Address</label>
                    <label class="control-label">`+ data.address + `</label>
                </div>
            `);
            $("#common-modal").modal("show");
        },
        error: function (error) {
            console.log(error);
        }
    })
}

function showInfoVehicle(id) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text("Vehicle information");
    $("#common-modal .modal-body").empty();
    $.ajax({
        url: "https://localhost:44314/api/Vehicle/" + id,
        type: 'GET',
        dataType: 'json',
        success: function (response) {
            let data = response.data;
            $("#common-modal .modal-body").append(`
                <div class="form-group">
                    <label class="control-label">Vehicle Id</label>
                    <label class="control-label">`+ data.vehicleId + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Body number</label>
                    <label class="control-label">`+ data.bodyNumber + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Registration plate</label>
                    <label class="control-label">` + data.licensePlates + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Purchase date</label>
                    <label class="control-label">` + formatDateTime(data.purchaseDate) + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Model</label>
                    <label class="control-label">`+ data.model + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Version</label>
                    <label class="control-label">`+ data.version + `</label>
                </div>
                <div class="form-group">
                    <label class="control-label">Value</label>
                    <label class="control-label">`+ data.value + ` $</label>
                </div>
            `);
            $("#common-modal").modal("show");
        },
        error: function (error) {
            console.log(error);
        }
    })
}