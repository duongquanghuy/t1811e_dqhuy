﻿$(document).ready(function () {

    var BiilID = getUrlParameter('BiilID');

    $('#Claim-Customer').empty();
    $('#claim-insurance').empty();
    $('#claim-Vehicle').empty();
    $('#claim-Detail').empty();
    $('#claim-bill-of-sale').empty();
    $('.invoice').css('display', 'none');
    $('.load-table').css('display', 'flex');



    $.ajax({
        type: 'GET',
        url: 'https://localhost:44314/admin/claim/searchsClaimApprovel/' + BiilID,
        dataType: 'json',
        success: function (result) {
            var claimStatus = result.data.claimStatus;
            var status = "";
            if (claimStatus == 1) {
                status = "Success";
            }else {
                status = "Unknown";
            }
            $('#max-total-value').val(result.data.maxValueRefund);
            $('#claimID-request').val(result.data.claimId);
            var now = new Date();
            var DateExpiresOn = Date.parse(result.data.expiresOn);
            var offset = DateExpiresOn - now.getTime();

            var totalDays = Math.round(offset / 1000 / 60 / 60 / 24);
            var refundQuantity = parseInt(result.data.maxRefundQuantity) - parseInt(result.data.billcount);

            var dateTimeFormat = formatted(result.data.expiresOn);
            var dateTimeClaim = formatted(result.data.createAt);
            let html_customer = '<div><span>Customer Name : </span>' + result.data.customName + '</div>' +
                '<div> <span>Phone : </span>' + result.data.cusPhone + '</div>' +
                '<div><span>Email : </span>' + result.data.cusEmail + '</div>' +
                '<div><span>Passport : </span>' + result.data.passport + '</div>';
            let html_insurance = '<div><span>InsuranceId : </span>' + result.data.insuranceId + '</div>' +
                '<div> <span>price Percent By Car Value : </span>' + result.data.pricePercentByCarValue + '</div>' +
                '<div><span>Min Value Refund : </span>' + result.data.minValueRefund + '</div>' +
                '<div><span>Max Value Refund : </span>' + result.data.maxValueRefund + '</div>' +
                '<div><span>Duration Month Unit : </span>' + result.data.durationMonthUnit + ' th</div>';
            let html_Vehicle = '<div><span>License Plates : </span>' + result.data.licensePlates + '</div>' +
                '<div><span>Vehicle Model Name : </span>' + result.data.vehicleModelName + '</div>' +
                '<div><span>vehicle Version Name : </span>' + result.data.vehicleVersionName + '</div>';
            let html_Detail = '<tr>' +
                '<th> Body Claim:</th>' +
                '<td>' + result.data.bodyClaim + '</td>' +
                '</tr >' +
                '<tr>' +
                '<th>Create Date</th>' +
                '<td>' + dateTimeClaim + '</td>' +
                '</tr>' +
                '<tr>' +
                '<th>Claim Status</th>' +
                '<td class="color-' + status + '">' + status + '</td>' +
                '</tr>' +
                '<tr>'+
                    '<th>Refund Qauntity:</th>' +
                '<td>' + refundQuantity + '</td>' +
                '</tr>'+
                    '<tr>'+
                        '<th>Reason :</th>' +
                        '<td>' + result.data.reason + '</td>' +
                    '</tr>'+
                    '<tr>'+
                        '<th>Total value:</th>' +
                        '<td>' + result.data.totalValue + ' $</td>' +
                    '</tr>';
            let html_Bill = '<div><span>Insurance Duration Monthly : </span>' + result.data.insuranceDurationMonthly + '</div>' +
                '<div><span>total Price : </span>' + result.data.totalPrice + ' $</div>' +
                '<div><span>expires On : </span>' + dateTimeFormat + '</div>' +
                '<div><span>Tim Remaining: </span>' + totalDays + ' Day</div>';

            $('#Claim-Customer').append(html_customer);
            $('#claim-insurance').append(html_insurance);
            $('#claim-Vehicle').append(html_Vehicle);
            $('#claim-Detail').append(html_Detail);
            $('#claim-bill-of-sale').append(html_Bill);
            /*     $('.loader').css('display', 'none');*/
            $('.load-table').css('display', 'none');
            $('.invoice').css('display', 'block');
        }
        ,
        error: function () {
            console.log('error');
            window.location.replace('/Claim/UnPaid');
        }

    });
});
//
function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
}
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}

function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}