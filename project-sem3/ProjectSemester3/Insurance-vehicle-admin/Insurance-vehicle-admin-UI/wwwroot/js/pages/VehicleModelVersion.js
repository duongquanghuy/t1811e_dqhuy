﻿//Lấy dữ liệu Model đổ vào View
$(document).ready(function () {
    $("table").css("display", "none");
    getData();
});

//Khai báo các biến
var editModelId; //dùng để đánh dấu Model sẽ được Sửa
var deleteModelId, deleteModelName; //dùng để đánh dấu Model sẽ được Xóa
var currentModelId, currentModelName; //dùng để đánh dấu Model đang được Xem Chi tiết
var currentVersionId, currentVersionName; //dùng để đánh dấu Version đang được Xử lý

//Đổ dữ liệu vào bảng
function getData(page = 1, pageSize = 2) {
    $('.load-table').css('display', 'flex');

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/',
        type: 'GET',
        dataType: 'json',
        success: function (respond) {
            $('#list-data-table-model').html('');

            $(".pagination").html('');

            $.each(respond.data, function (index, vm) {
                $('#list-data-table-model').append(
                    '<tr>' +
                        '<td style="text-align:center; vertical-align: middle;">' + vm.id + '</td>' +
                        '<td style="vertical-align: middle; padding-left: 20px;">' + vm.name + '</td>' +
                        '<td style="text-align:center; vertical-align: middle;">' +
                            '<button class="btn btn-sm btn-primary" onclick="showModelVersionModal(' + vm.id + ', `' + vm.name + '`)"' +
                            ' data-toggle="modal" data-target="#detail-model">' +
                                '<i class="fa fa-fw fa-eye" title="Show Version"></i>' + 'Show Version' +
                            '</button>' +
                        '</td>' +
                        '<td style="text-align:center; vertical-align: middle;">' +
                            '<button class="btn btn-sm btn-warning" style="margin-left: 7px; margin-right: 7px;" ' +
                            'onclick="showEditModelModal(' + vm.id + ')" data-toggle="modal" data-target="#edit-model">' +
                                '<i class="fa fa-fw fa-edit" title="Edit"></i>' + 'Edit' +
                            '</button>' +
                            '<button class="btn btn-sm btn-danger" onclick="showDeleteModelModal(' + vm.id + ')"' +
                            ' data-toggle="modal" data-target="#delete-model">' +
                                '<i class="fa fa-fw fa-trash" title="Delete"></i>' + 'Delete' +
                            '</button>' +
                        '</td>' +
                    '</tr>'
                );
            });

            $("table").css("display", "inline-table");
            $('.load-table').css('display', 'none');

            let currentPage = respond.currentPage;
            let totalPage = respond.totalPage;
            if (page > totalPage) {
                getData(1, 10);
            }
            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
            }

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
            }

            if (currentPage > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
            }

            $(".pagination > li").on("click", function () {
                getData($(this).val());
            });
        },
        error: function () {
            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
        },
    });
}

//Thêm Mới Model
function addNewModel() {
    var newModelName = $('#add-model-name').val().toString();

    if (newModelName == '') {
        showNotificationModal('Add New Model', 'You must enter Model Name!');
    } else {
        var newModel = {
            "Name": newModelName
        }

        $.ajax({
            url: 'https://localhost:44314/admin/VehicleModel/',
            type: 'POST',
            data: JSON.stringify(newModel),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                showNotificationModal('Add New Model', 'Add Model Success!');

                $('#close-add-model-btn').trigger('click');

                $('#add-model-name').val('');

                getData();
            },
            error: function (data) {
                showNotificationModal('Add New Model', 'Error: Add Model Failed!');
            },
        });
    }
}

//Hiển thị Modal Sửa Model
function showEditModelModal(id) {
    var modelId = id;

    editModelId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/' + modelId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#edit-model-name').val(data.data[0].name);
        },
        error: function (data) {
            showNotificationModal('Edit Model', 'Error: Edit Model Failed!');
        },
    });
}

//Sửa Model
function editModel() {
    var editModelName = $('#edit-model-name').val().toString();

    var editModel = {
        "Id": parseInt(editModelId),
        "Name": editModelName
    }

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/',
        type: 'PUT',
        contentType: 'application/json',
        data: JSON.stringify(editModel),
        dataType: 'json',
        success: function (data) {
            showNotificationModal('Add New Model', 'Edit Model Success!');

            getData();
        },
        error: function (data) {
            showNotificationModal('Add New Model', 'Error: Edit Model Failed!');
        },
    });
}

//Hiển thị Modal Xóa Model
function showDeleteModelModal(id) {
    var modelId = id;

    deleteModelId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/' + modelId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#delete-model-name').html(data.data[0].name);
            deleteModelName = data.data[0].name;
        },
        error: function (data) {
            showNotificationModal('Add New Model', 'Error: Add Model Failed!');
        },
    });
}

//Xóa Model
function deleteModel() {
    var modelId = deleteModelId;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/' + modelId,
        type: 'DELETE',
        dataType: 'json',
        success: function (data) {
            showNotificationModal('Add New Model', 'Delete Model ' + deleteModelName + 'Success!');

            deleteModelName = ''; //Chuyển biến về rỗng
            //$('#close-delete-model-btn').trigger('click');

            getData();
        },
        error: function (data) {
            showNotificationModal('Add New Model', 'Error: Edit Model Failed!');
        },
    });
}

//Hiển thị Chi tiết Model
function showModelVersionModal(id, name) {
    currentModelId = id;
    currentModelName = name;

    $('#model-title-details').html(currentModelName);

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleModel/getVersion/' + currentModelId,
        type: 'GET',
        dataType: 'json',
        success: function (respond) {
            $('#version-table-content').html(''); //Đưa nội dung bảng về rỗng

            if (respond.data.length == 0) {
                $('#version-noti-p1').css('display', 'none');
                $('#version-noti-p2').css('display', 'block');
                $('#version-table').css('display', 'none');

            } else {
                $('#version-noti-p1').css('display', 'block');
                $('#version-noti-p2').css('display', 'none');
                $('#version-table').css('display', 'table');

                $.each(respond.data, function (index, vv) {
                    $('#version-table-content').append(
                        '<tr>' +
                            '<td style="text-align:center; vertical-align: middle">' + vv.id + '</td>' +
                            '<td style="text-align:center; vertical-align: middle">' + vv.name + '</td>' +
                            '<td style="text-align:center; vertical-align: middle">' +
                                '<button class="btn btn-sm btn-warning" style="margin-left: 7px; margin-right: 7px;" ' +
                                'onclick="showEditVersionForm(' + vv.id + ')" data-toggle="modal" data-target="#edit-version">' +
                                    '<i class="fa fa-fw fa-edit" title="Edit"></i>' + 'Edit' + 
                                '</button>' +
                                '<button class="btn btn-sm btn-danger" onclick="showDeleteVersionModal(' + vv.id + ')"' +
                                ' data-toggle="modal" data-target="#delete-version">' +
                                    '<i class="fa fa-fw fa-trash" title="Delete"></i>' + 'Delete' + 
                                '</button>' +
                            '</td>' +
                        '</tr>'
                    );
                });
            }
        },
        error: function () {
            showNotificationModal('Model Details', 'Error: Get Versions of Model Failed!');
        },
    });
}

//Hiển thị Form Thêm mới Version của Model Hiện tại
function showAddNewVersionForm() {
    closeEditVersionForm();

    $('#add-new-version-area').css('display', 'initial');
    $('#add-version-name').val(currentModelName + ' ');
}

//Đóng Form Thêm mới Version của Model Hiện tại
function closeAddNewVersionForm() {
    $('#add-new-version-area').css('display', 'none');
    $('#add-version-name').val('');
}

//Thêm mới Version cho Model Hiện tại
function addNewVersion() {
    var newVersionName = $('#add-version-name').val();

    if (newVersionName == '') {
        showNotificationModal('Add New Version', 'You must enter Version Name!');
    } else {
        var newVersion = {
            "Name": newVersionName,
            "ModelId": currentModelId
        }

        $.ajax({
            url: 'https://localhost:44314/admin/VehicleVersion/',
            type: 'POST',
            data: JSON.stringify(newVersion),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                showNotificationModal('Add New Version', 'Add Version ' + newVersionName + ' Success!');

                closeAddNewVersionForm();

                showModelVersionModal(currentModelId, currentModelName);
            },
            error: function (data) {
                showNotificationModal('Add New Model', 'Error: Add Version ' + newVersionName + 'Failed!');
            },
        });
    }
}

//Hiển thị Form Chỉnh sửa Version của Model Hiện tại
function showEditVersionForm(id) {
    closeAddNewVersionForm();

    var versionId = id;

    currentVersionId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleVersion/' + versionId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#edit-version-name').val(data.data[0].name);
        },
        error: function (data) {
            showNotificationModal('Edit Version', 'Error: Get Version to Edit Failed!');
        },
    });

    $('#edit-version-area').css('display', 'block');
}

//Đóng Form Chỉnh sửa Version của Model Hiện tại
function closeEditVersionForm() {
    $('#edit-version-area').css('display', 'none');
    $('#edit-version-name').val('');
}

//Chỉnh sửa Version cho Model Hiện tại
function editVersion() {
    var newVersionName = $('#edit-version-name').val().toString();

    if (newVersionName == '') {
        showNotificationModal('Edit Version', 'You must enter Version Name!');
    } else {
        var newVersion = {
            "Id": parseInt(currentVersionId),
            "Name": newVersionName,
            "ModelId": parseInt(currentModelId)
        }

        $.ajax({
            url: 'https://localhost:44314/admin/VehicleVersion/',
            type: 'PUT',
            data: JSON.stringify(newVersion),
            contentType: 'application/json',
            dataType: 'json',
            success: function (data) {
                showNotificationModal('Edit Version', 'Edit Version ' + newVersionName + ' Success!');

                closeEditVersionForm();

                showModelVersionModal(currentModelId, currentModelName);
            },
            error: function (data) {
                showNotificationModal('Edit Version', 'Error: Edit Version ' + newVersionName + 'Failed!');
            },
        });
    }
}

//Hiển thị Modal Xóa Version
function showDeleteVersionModal(id) {
    var versionId = id;

    currentVersionId = id;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleVersion/' + versionId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#delete-version-name').html(data.data[0].name);
            currentVersionName = data.data[0].name;
        },
        error: function (data) {
            showNotificationModal('Delete Version', 'Error: Get Version to Delete Failed!');
        },
    });
}

//Xóa Version
function deleteVersion() {
    var versionId = currentVersionId;

    $.ajax({
        url: 'https://localhost:44314/admin/VehicleVersion/' + versionId,
        type: 'DELETE',
        dataType: 'json',
        success: function (data) {
            showNotificationModal('Delete Version', 'Delete Version ' + currentVersionName + ' Success!');

            currentVersionName = ''; //Chuyển biến về rỗng

            showModelVersionModal(currentModelId, currentModelName);
        },
        error: function (data) {
            showNotificationModal('Delete Version', 'Error: Delete Version Failed!');
        },
    });
}