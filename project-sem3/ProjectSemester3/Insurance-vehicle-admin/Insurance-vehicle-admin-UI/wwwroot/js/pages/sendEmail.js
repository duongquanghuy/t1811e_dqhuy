﻿var dataRequest = {
    claimId: parseInt(claimIDr),
    subject: heading,
    body: body
}
function ConfirmSendEmail(url, dataRequest) {

    if (dataRequest.body != "" && dataRequest.heading != "") {

     
        var dataRequestJSON = JSON.stringify(dataRequest);
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: dataRequestJSON,
            success: function (result) {
                var data = {
                    name :  result.data.name ,
                    email :  result.data.email
                }

                return data;   
        
            },
            error: function (reponst) {
                return "Error";
            }
        });
    } else {
        return 'Email Cannot be Empty';
    }

}