﻿/*<tr>
    <th>billId</th>
    <th>totalPrice</th>
    <th>custumerName</th>
    <th>modelName</th>
    <th>versionName</th>
    <th>phoneNumber</th>
    <th>createdAt</th>
    <th>paymentStatus</th>
    <th style="text-align:center">Option</th>
</tr>*/
$(document).ready(function () {
    $("table").css("display", "none");
    getData();
});
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}

//nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 2) {
    $('.load-table').css('display', 'flex');
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44314/admin/Billofsale/allPaid/' + page + '/' + pageSize,
        dataType: 'json',

        success: function (result) {
            $("#list-data-table-paid").empty();
            $(".pagination").empty();
            $.each(result.data, function (index, val) {
                var dateTimeFormat = formatted(val.createdAt);
                $("#list-data-table-paid").append('<tr>'+

                                '<td>'+val.billId+'</td>'+
                                '<td>'+val.totalPrice+' $</td>'+
                                '<td>'+val.custumerName+'</td>'+
                                '<td>'+val.modelName+'</td>'+
                                '<td>'+val.versionName+'</td>'+
                                '<td>'+val.phoneNumber+'</td>'+
                                '<td>'+dateTimeFormat+'</td>'+
                                '<td>'+val.paymentStatus+'</td>'+
                                '<td style="text-align:center">'+
                                '<button class="btn btn-sm btn-primary" onclick="displayBill(' + val.billId +')" >'+
                                        '<i class="fa fa-fw fa-eye"  title="Details"></i>'+
                                    '</button>'+
                                '</td>'+
                            '</tr >');
                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
            });

            let currentPage = result.currentPage;
            let totalPage = result.totalPage;
            if (page > totalPage) {
                getData(1, 10);
            }
            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
            }

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
            }

            if (currentPage > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
            }
            $(".pagination > li").on("click", function () {
                getData($(this).val());
            })

        }
        ,
        error: function () {

            $("table").css("display", "block");
            $('.load-table').css('display', 'none');
        },
    });

}
function displayBill(id) {
 
    $('#exampleModalDetails').modal('show');
    $('#body-bill-detail').css('display', 'none');
    $('.load-table-detail').css('display', 'flex');
    $.ajax({

        type: 'GET',
        url: 'https://localhost:44314/admin/Billofsale/bill/' + id +'',
        dataType: 'json',

        success: function (result) {
            $('#body-bill-detail').empty();

        
            var dateTimeFormat = formatted(result.data.createdAt);
                let html =   '<div class="row">'+
                                '<div class="col-lg-6"><label>Bill Id :</label><span>'+ result.data.billId +'</span></div>'+
                                '<div class="col-lg-6"><label>Insurance Duration Monthly :</label><span>' + result.data.insuranceDurationMonthly+'</span></div>'+
                             '</div>'+
                             '<div class="row">'+
                                '<div class="col-lg-6"><label>Created By :</label><span>'+result.data.createdBy+'</span></div>'+
                                '<div class="col-lg-6"><label>Total Price :</label><span>'+result.data.totalPrice+'</span></div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6"><label>Payment Status :</label><span>'+result.data.paymentStatus+'</span></div>'+
                                '<div class="col-lg-6"><label>Created Time :</label><span>'+dateTimeFormat+'</span></div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6"><label>Custumer Name :</label><span>'+result.data.custumerName+'</span></div>'+
                                '<div class="col-lg-6"><label>Phone Number :</label><span>'+result.data.phoneNumber+'</span></div>'+
                            '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6"><label>Version Name :</label><span>'+result.data.versionName+'</span></div>'+
                                '<div class="col-lg-6"><label>Model Name :</label><span>'+result.data.modelName+'</span></div>'+
                             '</div>'+
                            '<div class="row">'+
                                '<div class="col-lg-6"><label>Insurance Id :</label><span>'+result.data.insuranceId+'</span></div>'+
                                '<div class="col-lg-6"><label>Price Percent By Car Value :</label><span>'+result.data.pricePercentByCarValue+'</span></div>'+
                            '</div>';
                $('#body-bill-detail').append(html);
                $('#body-bill-detail').css('display', 'block');
                $('.load-table-detail').css('display', 'none');
         

        }
        ,
        error: function () {
            $('.load-table-detail').css('display', 'none');
            $('#exampleModalDetails').modal('hide');
        }

        
    })
}