﻿function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').css('display', 'initial');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}

function hideNotificationModal() {
    $('#notification-modal-container').css('display', 'none');
}
