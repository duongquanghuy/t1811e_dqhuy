﻿$(function () {
    //Datemask2 mm/dd/yyyy
    $("#datemask2").inputmask("mm/dd/yyyy", { "placeholder": "mm/dd/yyyy" });

    //Money Euro
    $("[data-mask]").inputmask();
    // 1 moc thoi gian
    $('#datepicker').datepicker({
        autoclose: true
    })
    // 2 moc thoi gian
    $('#reservation').daterangepicker();
});