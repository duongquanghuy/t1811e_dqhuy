﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Insurance_vehicle_admin_UI.Controllers
{
    public class ClaimController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [Route("claim/approvel")]
        public IActionResult Approvel()
        {
            return View();
        }
        [Route("claim/unpaid")]
        public IActionResult UnPaid()
        {
            return View();
        }

        [Route("claim/Detail")]
        public IActionResult Detail(int ClaimID)
        {
            return View();
        }
        [Route("claim/ClaimDetailPaid")]
        public IActionResult ClaimDetailPaid(int BiilID)
        {
            return View();
        }
    }
}
