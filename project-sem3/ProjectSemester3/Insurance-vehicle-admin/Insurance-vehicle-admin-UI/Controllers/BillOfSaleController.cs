﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Insurance_vehicle_admin_UI.Controllers
{
    public class BillOfSaleController : Controller
    {
        [Route("customer-insurance")]
        public IActionResult Index()
        {
            return View();
        }

        [Route("billOfSale/paid")]
        public IActionResult Paid()
        {
            return View();
        }

        [Route("billOfSale/unpaid")]
        public IActionResult Unpaid()
        {
            return View();
        }
        [Route("billOfSale/billActive")]
        public IActionResult BillActive()
        {
            return View();
        }
        [Route("insurance-bill")]
        public IActionResult InsuranceBill()
        {
            return View();
        }
    }
}
