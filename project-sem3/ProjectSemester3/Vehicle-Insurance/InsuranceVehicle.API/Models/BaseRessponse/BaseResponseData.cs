﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.BaseRessponse
{
    public class BaseResponseData
    {
        public string Message { get; set; }
        public int Code { get; set; }
        public dynamic Data { get; set; }

        public int currentPage { get; set; }
        public int totalPage { get; set; }
    }
}
