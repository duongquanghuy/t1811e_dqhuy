﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.BaseRessponse
{
    public class BaseResponseEmptyData
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}
