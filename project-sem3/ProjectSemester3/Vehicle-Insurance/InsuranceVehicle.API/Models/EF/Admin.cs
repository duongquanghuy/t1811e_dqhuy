﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.API.Models.EF
{
    public partial class Admin
    {
        public Admin()
        {
            BillCompensation = new HashSet<BillCompensation>();
            BillOfSale = new HashSet<BillOfSale>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }
        public int? RoleId { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
    }
}
