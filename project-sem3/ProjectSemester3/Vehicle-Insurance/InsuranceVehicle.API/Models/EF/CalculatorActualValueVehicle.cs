﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.API.Models.EF
{
    public partial class CalculatorActualValueVehicle
    {
        public int Id { get; set; }
        public int MinYear { get; set; }
        public int MaxYear { get; set; }
        public int PercentValue { get; set; }
    }
}
