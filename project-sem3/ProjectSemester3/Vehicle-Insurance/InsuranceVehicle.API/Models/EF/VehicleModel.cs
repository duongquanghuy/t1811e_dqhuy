﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.API.Models.EF
{
    public partial class VehicleModel
    {
        public VehicleModel()
        {
            Estimate = new HashSet<Estimate>();
            PriceListVehicleNew = new HashSet<PriceListVehicleNew>();
            Vehicle = new HashSet<Vehicle>();
            VehicleVersion = new HashSet<VehicleVersion>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Estimate> Estimate { get; set; }
        public virtual ICollection<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual ICollection<Vehicle> Vehicle { get; set; }
        public virtual ICollection<VehicleVersion> VehicleVersion { get; set; }
    }
}
