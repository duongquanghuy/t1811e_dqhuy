﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.API.Models.EF
{
    public partial class BillOfSale
    {
        public BillOfSale()
        {
            InsuranceSold = new HashSet<InsuranceSold>();
        }

        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public int VerhicleId { get; set; }
        public int InsuranceDurationMonthly { get; set; }
        public Guid? CreatedBy { get; set; }
        public double TotalPrice { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceId { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public virtual Admin CreatedByNavigation { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Insurance Insurance { get; set; }
        public virtual Vehicle Verhicle { get; set; }
        public virtual ICollection<InsuranceSold> InsuranceSold { get; set; }
    }
}
