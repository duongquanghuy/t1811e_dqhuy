﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.Request
{
    public class LoginModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string CustomerName { get; set; }
    }
}
