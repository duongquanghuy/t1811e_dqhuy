﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.Request
{
    public class CalculatePriceOfBill
    {
        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public int VerhicleId { get; set; }
        public int InsuranceDurationMonthly { get; set; }
        public Guid? CreatedBy { get; set; }
        public double TotalPrice { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceId { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public int modelId { get; set; }
        public int vesionId { get; set; }
        public int quantityYearUsed { get; set; }
        public int quantity { get; set; }
    }
}
