﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.Request
{
    public class GetTokenModel
    {
        public string Email { get; set; }
        public string  RefreshToken { get; set; }
    }
}
