﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.Request
{
    public class RequestClaimDetail
    {
        public int ClaimId { get; set; }
        public string BodyClaim { get; set; }
        public Guid CustomerId { get; set; }
        public int? InsuranceId { get; set; }
        public int InsurancrSoldId { get; set; }
        public List<string> ListImg { get; set; }
        public List<int> ImgID { get; set; }
    }
}
