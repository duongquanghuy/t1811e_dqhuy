﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class BillOfSaleDTO
    {
        public string BodyNumber { get; set; }
        public string EngineNumber { get; set; }
        public string LicensePlates { get; set; }

        public int Id { get; set; }
        public Guid CustomerId { get; set; }
        public int VerhicleId { get; set; }
        public int InsuranceDurationMonthly { get; set; }
        public Guid? CreatedBy { get; set; }
        public double TotalPrice { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceId { get; set; }
        public DateTime? ModifiedAt { get; set; }

        public string CustumerName { get; set; }

        public double PricePercentByCarValue { get; set; }

        public string ModelName { get; set; }

        public string VersionName { get; set; }
    }
}
