﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class VehicleDTO
    {
        public int Id { get; set; }
        public string BodyNumber { get; set; }
        public string EngineNumber { get; set; }
        public string LicensePlates { get; set; }
        public DateTime PurchaseDate { get; set; }
        public Guid? CustomerId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public VehicleModelDTO Model { get; set; }
        public VehicleVersionDTO Version { get; set; }
    }
}
