﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class LoginSuccessDTO
    {
        public string CustomerName { get; set; }
        public string CustomerID { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }

        public string ImgurClientId { get; set; }
    }
}
