﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class VehicleModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual IEnumerable<VehicleVersionDTO> VehicleVersion { get; set; }
    }
}
