﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class ClaimDetailDTO
    {
        public int Id { get; set; }
        public string BodyClaim { get; set; }
        public Guid CustomerId { get; set; }
        public int? InsuranceId { get; set; }
        public int BillOfSaleId { get; set; }
        public string LicensePlates { get; set; }
        public string VehicleVersionName { get; set; }
        public string VehicleModelName { get; set; }
        public int ClaimStatus { get; set; }
        public int InsuranceSolid { get; set; }
        public List<string> ListImg { get; set; }
        public DateTime CreateAt { get; set; }
    }
}
