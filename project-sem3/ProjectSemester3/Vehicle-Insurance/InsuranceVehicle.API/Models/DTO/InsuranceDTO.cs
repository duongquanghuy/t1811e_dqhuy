﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.API.Models.DTO
{
    public class InsuranceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PricePercentByCarValue { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int? DurationMonth { get; set; }
        public double MaxValueRefund { get; set; }
        public double MinValueRefund { get; set; }
        public int MaxRefundQuantity { get; set; }
        public string Description { get; set; }
    }
}
