﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace InsuranceVehicle.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public CommonController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }
        /// <summary>
        /// Lấy danh sách vehicleVersion ứng với vehicleId
        /// Created by Lqtuan
        /// </summary>
        /// <param name="vehicleModelId"></param>
        /// <returns></returns>
        [HttpGet("versionByVehicleModel/{vehicleModelId}")]
        public BaseResponseData getVersionByVehicleModel(int vehicleModelId)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleVersion.Where(vs => vs.ModelId == vehicleModelId)
                    .Select(vs => new VehicleVersionDTO() { Id = vs.Id, Name = vs.Name })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        /// <summary>
        /// Lấy tất cả các vehicle model công ty hỗ trợ
        /// Created by lqtuan
        /// </summary>
        /// <returns></returns>
        [HttpGet("vehicleModel")]
        public BaseResponseData getAllVehicleModel()
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Select(vm => new VehicleModelDTO()
                {
                    Id = vm.Id,
                    Name = vm.Name,
                    VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO() {
                        Id = vv.Id,
                        Name = vv.Name
                    })
                })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("searchVehicleModelByName/{name}")]
        public BaseResponseData searchVehicleModelByName(string name)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Where(vm => EF.Functions.Like(vm.Name.ToLower(), '%' + name.ToLower() + '%'))
                    .Select(vm => new VehicleModelDTO()
                {
                    Id = vm.Id,
                    Name = vm.Name,
                    VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                    {
                        Id = vv.Id,
                        Name = vv.Name
                    })
                }).AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("vehicleModel/{id}")]
        public BaseResponseData getVehicleModelById(int id)
        {
            try
            {
                var data = insuranceVehicleContext.VehicleModel.Where(vm => vm.Id == id)
                    .Select(vm => new VehicleModelDTO()
                    {
                        Id = vm.Id,
                        Name = vm.Name,
                        VehicleVersion = vm.VehicleVersion.Select(vv => new VehicleVersionDTO()
                        {
                            Id = vv.Id,
                            Name = vv.Name
                        })
                    }).AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPost("vehicleModel")]
        public BaseResponseEmptyData postVehicleModel([FromBody] VehicleModel vm)
        {
            try
            {
                insuranceVehicleContext.VehicleModel.Add(vm);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Add Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPut("vehicleModel")]
        public BaseResponseEmptyData putVehicleModel([FromBody] VehicleModel vm)
        {
            try
            {
                var vmToEdit = insuranceVehicleContext.VehicleModel.Where(vme => vme.Id == vm.Id).FirstOrDefault();
                vmToEdit.Name = vm.Name;
                insuranceVehicleContext.SaveChanges(); //Lưu thay đổi
                return new BaseResponseEmptyData() { Code = 200, Message = "Edit Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpDelete("vehicleModel/{id}")]
        public BaseResponseEmptyData deleteVehicleModel(int id)
        {
            try
            {

                var vmToDelete = insuranceVehicleContext.VehicleModel.Where(vme => vme.Id == id).FirstOrDefault();
                if (vmToDelete == null)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Cannot Delete because this Vehicle Model is not Exist!" };
                }
                insuranceVehicleContext.VehicleModel.Remove(vmToDelete);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Delete Vehicle Model Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error" + e.Message };
            }
        }

        [HttpGet("priceInsurance")]
        public BaseResponseData getPriceInsurance(int insuranceId, int modelId, int vesionId, int quantityYearUsed, int quantity)
        {
            try
            {
                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if(quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if(quantityYearUsed < 10 && quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }else if(quantityYearUsed < 15 && quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == modelId && pv.VersionId == vesionId).FirstOrDefault().Value;
                var priceInsuranceByCar = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == insuranceId)
                    .FirstOrDefault().PricePercentByCarValue;
                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= quantityYearUsed && calc.MaxYear >= quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)quantity);
                return new BaseResponseData() { Code = 200, Data = totalPrice, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getModelVersionByVehicleId/{id}")]
        public BaseResponseData getModelVersionByVehicleId(int id)
        {
            try
            {
                var data = insuranceVehicleContext.Vehicle.Where(vec => vec.Id == id)
                    .Select(vec => new Vehicle() { Id = vec.Id, ModelId = vec.ModelId, VersionId = vec.VersionId })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}