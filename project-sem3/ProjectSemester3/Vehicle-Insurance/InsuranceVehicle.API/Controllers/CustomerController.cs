﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.EF;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceVehicle.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public CustomerController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("{CusID}")]
        public BaseResponseData getSearchCustomer(Guid? CusID)
        {
            try
            {
                var Customer = insuranceVehicleContext.Customer.Where(x => x.Id.Equals(CusID)).
                Select(cus => new CustomerDTO
                {
                    CusId = cus.Id,
                    Email = cus.Email,
                    PhoneNumber = cus.PhoneNumber,
                    Address = cus.Address,
                    Name = cus.Name,
                    Passport = cus.Passport,
                    Password = cus.Password
                }).FirstOrDefault();
                if (Customer == null)
                {
                    return new BaseResponseData() { Code = 200, Data = null, Message = "Not found!" };
                }

                return new BaseResponseData() { Code = 200, Data = Customer, Message = "Success search Cusromer" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message };
            }
        }

        [HttpPut("infor")]
        public BaseResponseEmptyData editCustomer([FromBody] Customer customer)
        {
            try
            {
                var customerEdit = insuranceVehicleContext.Customer.Where(cus => cus.Id == customer.Id).FirstOrDefault();
                customerEdit.Name = customer.Name;
                customerEdit.PhoneNumber = customer.PhoneNumber;
                customerEdit.Email = customer.Email;
                customerEdit.Address = customer.Address;
                customerEdit.Passport = customer.Passport;
                insuranceVehicleContext.SaveChanges(); //Lưu thay đổi
                return new BaseResponseEmptyData() { Code = 200, Message = "Edit Profile Success!" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPut("password")]
        public BaseResponseEmptyData editCustomerPassword([FromBody] Customer customer)
        {
            try
            {
                var customerEdit = insuranceVehicleContext.Customer.Where(cus => cus.Id == customer.Id).FirstOrDefault();
                if(customerEdit != null)
                {
                    customerEdit.Password = customer.Password;
                    insuranceVehicleContext.SaveChanges(); //Lưu thay đổi
                    return new BaseResponseEmptyData() { Code = 200, Message = "Change Password Success!" };

                }
                else
                {
                    return new BaseResponseEmptyData() { Code = 500, Message = "ERROR!"};
                }
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
    }
}
