﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceVehicle.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InsuranceController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public InsuranceController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("")] //Phương thức : lấy dữ liệu dùng get, sửa dùng PUT, Thên dùng POST, Xóa DELETE
        [HttpGet("{page}/{pageSize}")] //Nếu muốn thay đổi size và page truyền vào thì như này
        //Mặc định là lấy trang 1 và 20 item, tên tham số page và pageSize phải giống param ở router
        public BaseResponseData getAllInsurance(int page = 1, int pageSize = 20)
        {
            try
            {
                //Parse data qua DTO để tạo object trả về dữ liệu
                var data = insuranceVehicleContext.Insurance.Select(insurance => new InsuranceDTO()
                {
                    Id = insurance.Id,
                    Name = insurance.InsuranceName,
                    PricePercentByCarValue = insurance.PricePercentByCarValue,
                    CreatedAt = insurance.CreatedAt,
                    DurationMonth = insurance.DurationMonthUnit,
                    MaxRefundQuantity = insurance.MaxRefundQuantity,
                    MaxValueRefund = insurance.MaxValueRefund,
                    MinValueRefund = insurance.MinValueRefund,
                    Description = insurance.Description
            })
                    .Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();

                var totalItem = insuranceVehicleContext.Insurance.Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        //Thêm, [Fombody] để truyền dữ liệu dạng string json trong body ở postman
        /*
             {
                "PolicyId": 1,
                "PricePercentByCarValue": 4,
                "DurationMonth": 24
            }
             */
        [HttpPost]
        public BaseResponseEmptyData postInsurance([FromBody] Insurance insurance)
        {
            try
            {
                insuranceVehicleContext.Insurance.Add(insurance);
                insuranceVehicleContext.SaveChanges(); //Lưu thay đổi
                return new BaseResponseEmptyData() { Code = 200, Message = "POST success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPut]
        public BaseResponseEmptyData editInsurance([FromBody] Insurance insurance)
        {
            try
            {
                var insuranceToEdit = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == insurance.Id).FirstOrDefault();
                insuranceToEdit.PricePercentByCarValue = insurance.PricePercentByCarValue;
                insuranceToEdit.MaxRefundQuantity = insurance.MaxRefundQuantity;
                insuranceToEdit.MinValueRefund = insurance.MinValueRefund;
                insuranceVehicleContext.SaveChanges(); //Lưu thay đổi
                return new BaseResponseEmptyData() { Code = 200, Message = "PUT success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        //Lấy Thông tin Bảo hiểm theo Id
        [HttpGet("getInsuranceById/{id}")]
        public BaseResponseData getInsuranceById(int id)
        {
            try
            {
                if (id <= 0)
                    return new BaseResponseData() { Code = 500, Message = "Not a valid Insurance Id " };

                var data = insuranceVehicleContext.Insurance.Where(Insurance => Insurance.Id == id).FirstOrDefault();

                return new BaseResponseData() { Code = 200, Message = "Success Data", Data = data };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }


    }
}