﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.EF;
using InsuranceVehicle.API.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;

namespace InsuranceVehicle.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClaimController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public ClaimController(InsuranceVehicleContext dbcontext)
        {
            insuranceVehicleContext = dbcontext;
        }

        [HttpPost]
        public BaseResponseData postRequestClaim([FromBody] RequestClaimDetail requestClaim)
        {
            try
            {

                var result = insuranceVehicleContext.InsuranceSold.
                    Where(bill => bill.Id == requestClaim.InsurancrSoldId && bill.BillOfSale.CustomerId == requestClaim.CustomerId).FirstOrDefault();

                if (result == null)
                {
                    return new BaseResponseData() { Code = 400, Message = "Insurance not found!" };
                }

                //Lấy ra số lần đã claim và được thanh toám
                var countClaimSuccess = insuranceVehicleContext.ClaimDetail.Where(CD => CD.CustomerId == requestClaim.CustomerId && CD.InsuranceId == requestClaim.InsurancrSoldId && (CD.Status == 2 || CD.Status == 1)).Count();
                //Lấy ra số lần được phép claim của bảo hiểm
                var countMaxClaimInsurance = insuranceVehicleContext.Insurance.Where(I => I.Id == requestClaim.InsuranceId).FirstOrDefault().MaxRefundQuantity;

                if(countClaimSuccess >= countMaxClaimInsurance)
                {
                    return new BaseResponseData() { Code = 300, Message = "You have exceeded the number of claims for this insurance!" };
                }
                var TimExpirceOnVsTimeNow = DateTime.Compare(Convert.ToDateTime(result.ExpiresOn), DateTime.Now);
                ///Check hết hạn
                if (TimExpirceOnVsTimeNow > 0)
                {
                    ClaimDetail claim = new ClaimDetail()
                    {
                        CustomerId = requestClaim.CustomerId,
                        BodyClaim = requestClaim.BodyClaim,
                        InsuranceId = requestClaim.InsurancrSoldId,

                    };
                    insuranceVehicleContext.ClaimDetail.Add(claim);
                    insuranceVehicleContext.SaveChanges();
                    int claimId = claim.Id;

                    foreach (string linkImg in requestClaim.ListImg)
                    {
                        insuranceVehicleContext.ImageInClaim.Add(new ImageInClaim() { ClaimDetailId = claimId, Link = linkImg });
                    }
                    insuranceVehicleContext.SaveChanges();
                    return new BaseResponseData() { Code = 200, Data = claimId, Message = "Send Claim Success" };
                }
                else
                {
                    return new BaseResponseData() { Code = 300, Message = "Overdue Insurance!" };
                }

            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpPut]
        public BaseResponseEmptyData editClaim([FromBody] RequestClaimDetail requestClaim)
        {
            try
            {
                var result = insuranceVehicleContext.InsuranceSold.
                    Where(bill => bill.Id == requestClaim.InsurancrSoldId && bill.BillOfSale.CustomerId == requestClaim.CustomerId).FirstOrDefault();
                if (result == null)
                {
                    return new BaseResponseEmptyData() { Code = 400, Message = "Insurance not found!" };
                }
               

                var ClaimToEdit = insuranceVehicleContext.ClaimDetail.Where(claim => claim.Id == requestClaim.ClaimId).FirstOrDefault();
               /* if (ClaimToEdit.Status == 1)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Edit not found!" };
                }*/
                if (requestClaim.ImgID != null)
                {
                    foreach (int Id in requestClaim.ImgID)
                    {
                      var ImgID =  insuranceVehicleContext.ImageInClaim.Where(x => x.Id == Id).FirstOrDefault();
                        insuranceVehicleContext.ImageInClaim.Remove(ImgID);
                        
                    }
                    insuranceVehicleContext.SaveChanges();
                }
                if (ClaimToEdit != null)
                {
                    ClaimToEdit.BodyClaim = requestClaim.BodyClaim;
                    if(requestClaim.ListImg != null )
                    foreach (string linkImg in requestClaim.ListImg)
                    {
                        insuranceVehicleContext.ImageInClaim.Add(new ImageInClaim() { ClaimDetailId = requestClaim.ClaimId, Link = linkImg });
                    }
                }
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "Send Edit Claim Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("search/{CusId}/{ClaimId}")]
        public BaseResponseData getClaimDetail(Guid CusId, int ClaimId)
        {
            try
            {
                var ClaimDetail = insuranceVehicleContext.ClaimDetail.Where(claim => claim.CustomerId == CusId && claim.Id == ClaimId).
                    Select(claimd =>
                       new ClaimDetailDTO()
                       {
                           Id = ClaimId,
                           BodyClaim = claimd.BodyClaim,
                           CustomerId = CusId,
                           BillOfSaleId = claimd.Insurance.BillOfSaleId,
                           InsuranceId = claimd.InsuranceId,
                           InsuranceSolid = claimd.Insurance.Id
                       }
                    ).FirstOrDefault();
                return new BaseResponseData { Code = 200, Data = ClaimDetail, Message = "Success Claim" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("claimDetail/{cusId}")]
        public BaseResponseData getAllClaimDetail(Guid cusId ,int status = 4, string Vehicle = null, int page = 1, int pageSize = 20)
        {
            try
            {
                IEnumerable<ClaimDetailDTO> data = null;
                data = insuranceVehicleContext.ClaimDetail.Where(claim => claim.CustomerId == cusId ).OrderByDescending(claim => claim.CreatedAt)
                    .Select(claim => new ClaimDetailDTO()
                    {
                        Id = claim.Id,
                        BodyClaim = claim.BodyClaim,
                        CustomerId = cusId,
                        BillOfSaleId = claim.Insurance.BillOfSaleId,
                        InsuranceId = claim.InsuranceId,
                        InsuranceSolid = claim.Insurance.Id,
                        LicensePlates = claim.Insurance.BillOfSale.Verhicle.LicensePlates,
                        VehicleVersionName = claim.Insurance.BillOfSale.Verhicle.Version.Name,
                        VehicleModelName = claim.Insurance.BillOfSale.Verhicle.Model.Name,
                        ClaimStatus = claim.Status,
                        CreateAt = claim.CreatedAt


                    });
                if(status == 0 || status  == 1 || status  == -1  || status == 2)
                {
                    data = data.Where(x => x.ClaimStatus == status).AsQueryable();
                }
                if (Vehicle != "" && Vehicle != null)
                {
                    var serrchBill = string.IsNullOrEmpty(Vehicle) ? "" : Vehicle.Trim().ToLower();
                    data = data.Where(x => x.LicensePlates.ToLower().Contains(serrchBill) || x.VehicleModelName.ToLower().Contains(serrchBill) || x.VehicleVersionName.ToLower().Contains(serrchBill));
                }

                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "Success", currentPage = page, totalPage = totalPage };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }

        }

        [HttpGet("getAllImg/{claimID}")]
        public BaseResponseData getAllImg(int ClaimId)
        {
            try
            {
                var ImgClaim = insuranceVehicleContext.ImageInClaim.Where(claim => claim.ClaimDetailId == ClaimId).AsQueryable();
                return new BaseResponseData { Code = 200, Data = ImgClaim, Message = "Success" };

            }
            catch (Exception e)
            {
                return new BaseResponseData { Code = 500, Message = "Error: " + e.Message.ToString() };

            }
        }

        [HttpDelete("delete")]
        public BaseResponseEmptyData deleteClaim([FromBody] ClaimDetail claimRequest)
        {
            try
            {

                var ClaimToDelete = insuranceVehicleContext.ClaimDetail.Where(claim => claim.CustomerId == claimRequest.CustomerId && claim.Id == claimRequest.Id).FirstOrDefault();
                if (ClaimToDelete.Status == 1 || ClaimToDelete.Status == 2)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Can Not Delete" };
                }
                var img =  insuranceVehicleContext.ImageInClaim.Where(x => x.ClaimDetailId == ClaimToDelete.Id).AsQueryable();
                foreach(var i in img)
                {
                    insuranceVehicleContext.ImageInClaim.Remove(i);
                }
                insuranceVehicleContext.ClaimDetail.Remove(ClaimToDelete);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Delete Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error" + e.Message };
            }
        }
    }

}
