﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.EF;
using InsuranceVehicle.API.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceVehicle.API.Controllers
{   
    [Route("api/[controller]")]
    [ApiController]
    public class BillOfSaleController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public BillOfSaleController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }
        [HttpPost]
        public BaseResponseEmptyData postBillOfSale([FromBody] CalculatePriceOfBill bill)
        {
            try
            {
                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if (bill.quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if (bill.quantityYearUsed < 10 && bill.quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }
                else if (bill.quantityYearUsed < 15 && bill.quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }
                //tính giá của bill
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == bill.modelId && pv.VersionId == bill.vesionId).FirstOrDefault().Value;

                var insurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == bill.InsuranceId)
                    .FirstOrDefault();
                var priceInsuranceByCar = insurance.PricePercentByCarValue;

                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= bill.quantityYearUsed && calc.MaxYear >= bill.quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)bill.quantity);

                var InsuranceDurationMonthlyBill = insurance.DurationMonthUnit * bill.quantity;
                BillOfSale billOfSale = new BillOfSale()
                {
                    CustomerId = bill.CustomerId,
                    VerhicleId = bill.VerhicleId,
                    InsuranceDurationMonthly = InsuranceDurationMonthlyBill,
                    InsuranceId = bill.InsuranceId,
                    TotalPrice = totalPrice,
                    PaymentStatus = 0,
                };
                insuranceVehicleContext.BillOfSale.Add(billOfSale);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "POST success" };

            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpDelete]
        public BaseResponseEmptyData deleteBillOfSale(int id)
        {
            try
            {
                if (id <= 0)
                    return new BaseResponseEmptyData() { Code = 500, Message = "Not a valid bill of sale Id " };

                var BillToDelete = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.Id == id).FirstOrDefault();
                if (BillToDelete == null)
                {
                    return new BaseResponseEmptyData() { Code = 400, Message = "Not found" };
                }
                if (BillToDelete.PaymentStatus == 1)
                {
                    return new BaseResponseEmptyData() { Code = 300, Message = "Bill Cannot be remove because it was paid already!" };
                }
                insuranceVehicleContext.BillOfSale.Remove(BillToDelete);
                insuranceVehicleContext.SaveChanges();
                return new BaseResponseEmptyData() { Code = 200, Message = "DELETE Success" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("getBillOfSaleById/{id}")]
        public BaseResponseData getBillOfSale(int id)
        {
            try
            {
                if (id <= 0)
                    return new BaseResponseData() { Code = 500, Message = "Not a valid bill of sale Id " };
                var data = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.Id == id).FirstOrDefault();

                return new BaseResponseData() { Code = 200, Message = "Success Data", Data = data };

            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("getByStatusWait/{cusID}/{page}/{pageSize}")]
        [HttpGet("getByStatusWait/{cusID}")]
        public BaseResponseData getByPaymentStatusWait(Guid cusID , string Vehicle, int page = 1, int pageSize = 20)
         {
            try
            {

                IEnumerable<BillOfSalStatusDTO> data = null;
                data = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.PaymentStatus == 0 && BillOfSale.CustomerId == cusID).OrderByDescending(bill => bill.CreatedAt)
                            .Select(BillOfSale => new BillOfSalStatusDTO()
                            {
                                CustomerId = BillOfSale.Customer.Id,
                                VerhicleId = BillOfSale.Verhicle.Id,
                                VersionID = BillOfSale.Verhicle.Version.Id,

                                VersionName = BillOfSale.Verhicle.Version.Name,
                                ModeID = BillOfSale.Verhicle.Model.Id,
                                ModelName =  BillOfSale.Verhicle.Model.Name,
                                LicensePlates = BillOfSale.Verhicle.LicensePlates,
                                MaxValueRefund = BillOfSale.Insurance.MaxValueRefund,
                                MaxRefundQuantity =  BillOfSale.Insurance.MaxRefundQuantity,
                                DurationMonthUnit =  BillOfSale.Insurance.DurationMonthUnit,

                                Id = BillOfSale.Id,
                                TotalPrice = BillOfSale.TotalPrice,
                                PaymentStatus = BillOfSale.PaymentStatus,
                                CreatedAt = BillOfSale.CreatedAt,
                                InsuranceId = BillOfSale.InsuranceId,
                                ModifiedAt = BillOfSale.ModifiedAt,
                            });

                if (Vehicle != "" && Vehicle != null)
                {
                    var serrchBill = string.IsNullOrEmpty(Vehicle) ? "" : Vehicle.Trim().ToLower();
                    data = data.Where(x => x.LicensePlates.ToLower().Contains(serrchBill) || x.VersionName.ToLower().Contains(serrchBill) || x.ModelName.ToLower().Contains(serrchBill));
                }

                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success", totalPage = totalPage, currentPage = page };



            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("getByStatusPaid/{cusID}/{page}/{pageSize}")]
        [HttpGet("getByStatusPaid/{cusID}")]
        public BaseResponseData getByPaymentStatusPaid(Guid cusID , string Vehicle, int page = 1, int pageSize = 20)
        {
            try
            {
                IEnumerable<BillOfSalStatusDTO> data = null;
                 data = insuranceVehicleContext.InsuranceSold.Where(bill => bill.BillOfSale.CustomerId == cusID && bill.BillOfSale.PaymentStatus == 1).OrderByDescending(bill => bill.CreatedAt)
                    .Select(bill => new BillOfSalStatusDTO()
                    {
                        CustomerId = bill.BillOfSale.Customer.Id,
                        VerhicleId = bill.BillOfSale.Verhicle.Id,
                        VersionName = bill.BillOfSale.Verhicle.Version.Name,
                        ModelName = bill.BillOfSale.Verhicle.Model.Name,
                        ExpiresOn = bill.ExpiresOn,
                        LicensePlates = bill.BillOfSale.Verhicle.LicensePlates,
                        MaxValueRefund = bill.BillOfSale.Insurance.MaxValueRefund,
                        MaxRefundQuantity = bill.BillOfSale.Insurance.MaxRefundQuantity,
                        DurationMonthUnit = bill.BillOfSale.Insurance.DurationMonthUnit,

                        Id = bill.BillOfSale.Id,

                        TotalPrice = bill.BillOfSale.TotalPrice,
                        PaymentStatus = bill.BillOfSale.PaymentStatus,
                        CreatedAt = bill.BillOfSale.CreatedAt,
                        InsuranceId = bill.BillOfSale.InsuranceId,
                        ModifiedAt = bill.BillOfSale.ModifiedAt,
                    });

                if (Vehicle != "" && Vehicle != null)
                {
                    var serrchBill = string.IsNullOrEmpty(Vehicle) ? "" : Vehicle.Trim().ToLower();
                    data = data.Where(x => x.LicensePlates.ToLower().Contains(serrchBill) || x.ModelName.ToLower().Contains(serrchBill) || x.VersionName.ToLower().Contains(serrchBill));
                }

                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success", currentPage = page, totalPage = totalPage };



            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpPut]
        public BaseResponseEmptyData editBillOfSale([FromBody] CalculatePriceOfBill bill) 
        {
            try {

                var rateOfCharge = 0.02; //Tỉ lệ phí, mặc định 0,02%
                if (bill.quantityYearUsed < 5)
                {
                    rateOfCharge = 0.14;
                }
                else if (bill.quantityYearUsed < 10 && bill.quantityYearUsed >= 5)
                {
                    rateOfCharge = 0.16;
                }
                else if (bill.quantityYearUsed < 15 && bill.quantityYearUsed >= 10)
                {
                    rateOfCharge = 0.18;
                }

                //tính giá của bill
                var priceNew = insuranceVehicleContext.PriceListVehicleNew
                    .Where(pv => pv.ModelId == bill.modelId && pv.VersionId == bill.vesionId).FirstOrDefault().Value;

                var insurance = insuranceVehicleContext.Insurance.Where(insurance => insurance.Id == bill.InsuranceId)
                    .FirstOrDefault();
                var priceInsuranceByCar = insurance.PricePercentByCarValue;

                var actualValueVehicle = insuranceVehicleContext.CalculatorActualValueVehicle
                    .Where(calc => calc.MinYear <= bill.quantityYearUsed && calc.MaxYear >= bill.quantityYearUsed).FirstOrDefault().PercentValue;
                var totalPrice = Math.Truncate(priceNew * ((priceInsuranceByCar + rateOfCharge) / 100) * ((double)actualValueVehicle / 100) * (double)bill.quantity);

                var InsuranceDurationMonthlyBill = insurance.DurationMonthUnit * bill.quantity;

                var BillOfSaleToEdit = insuranceVehicleContext.BillOfSale.Where(BillOfSale => BillOfSale.Id == bill.Id && BillOfSale.CustomerId == bill.CustomerId).FirstOrDefault();
                BillOfSaleToEdit.TotalPrice = totalPrice;
                BillOfSaleToEdit.InsuranceDurationMonthly = InsuranceDurationMonthlyBill;
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Success Edit" };
            }
            catch (Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("allBillOfSale/{page}/{pageSize}")]
        public BaseResponseData getAllBillOfSale(int page = 1, int pageSize = 20)
        {
            try
            {
                var data = insuranceVehicleContext.BillOfSale.OrderByDescending(bill => bill.CreatedAt).Select(bos => new BillOfSaleDTO()
                {
                    CustomerId = bos.CustomerId,
                    CustumerName = bos.Customer.Name,

                    BodyNumber = bos.Verhicle.BodyNumber,
                    EngineNumber = bos.Verhicle.EngineNumber,
                    LicensePlates = bos.Verhicle.LicensePlates,

                    InsuranceId = bos.InsuranceId,
                    PricePercentByCarValue = bos.Insurance.PricePercentByCarValue,

                    Id = bos.Id,
                    CreatedAt = bos.CreatedAt,
                    ModifiedAt = bos.ModifiedAt,
                    InsuranceDurationMonthly = bos.InsuranceDurationMonthly,
                    TotalPrice = bos.TotalPrice,
                    PaymentStatus = bos.PaymentStatus,

                    ModelName = bos.Verhicle.Model.Name,
                    VersionName = bos.Verhicle.Version.Name

                    /*
                    InsuranceSold = bos.InsuranceSold.Select(iso => new InsuranceSoldDTO()
                    {
                        Id = iso.Id,
                        ExpiresOn = iso.ExpiresOn
                    })
                    */
                }).Skip((page - 1) * pageSize).Take(pageSize)
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }
    
        [HttpGet("BillActive/{cusID}/{page}/{pageSize}")]
        [HttpGet("BillActive/{cusID}")]
        public BaseResponseData getInsuranceSoldActive(Guid cusID,string Vehicle, int page = 1, int pageSize = 20)
        {
            try
            {
                IEnumerable<BillOfSaleActiveDTO> data = null;
                data = insuranceVehicleContext.InsuranceSold.Where(bill => bill.BillOfSale.CustomerId == cusID && bill.ExpiresOn > DateTime.Now).OrderByDescending(bill => bill.CreatedAt)
                    .Select(bill => new BillOfSaleActiveDTO()
                    {
                        InsuranceName = bill.BillOfSale.Insurance.InsuranceName,
                        InsuranceSoldId = bill.Id,
                        InsuranceId = bill.BillOfSale.InsuranceId,
                        ExpiresOn = bill.ExpiresOn,
                        MaxValueRefund = bill.BillOfSale.Insurance.MaxValueRefund,
                        MaxRefundQuantity = bill.BillOfSale.Insurance.MaxRefundQuantity,
                        DurationMonthUnit = bill.BillOfSale.Insurance.DurationMonthUnit,
                        MinRefundQuantity = bill.BillOfSale.Insurance.MinValueRefund,
                        TotalPrice = bill.BillOfSale.TotalPrice,
                        VersionName = bill.BillOfSale.Verhicle.Version.Name,
                        ModelName = bill.BillOfSale.Verhicle.Model.Name,
                        LicensePlates = bill.BillOfSale.Verhicle.LicensePlates,
                        CreatedAt = bill.BillOfSale.CreatedAt,
                    });
                if (Vehicle != "" && Vehicle != null)
                {
                    var serrchBill = string.IsNullOrEmpty(Vehicle) ? "" : Vehicle.Trim().ToLower();
                    data = data.Where(x => x.LicensePlates.ToLower().Contains(serrchBill) || x.VersionName.ToLower().Contains(serrchBill) || x.ModelName.ToLower().Contains(serrchBill));
                }
                var totalItem = data.Count();
                data = data.OrderByDescending(x=> x.CreatedAt).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success" , currentPage = page, totalPage = totalPage};



            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }
        [HttpGet("searchBill/{cusID}/{LicensePlates}/{page}/{pageSize}")]
        [HttpGet("searchBill/{cusID}/{LicensePlates}")]
        public BaseResponseData getSearchBill(string LicensePlates, Guid cusID ,int page =1 ,int pageSize=20)
        {
            try
            {
                var data = insuranceVehicleContext.InsuranceSold.Where(bill => bill.BillOfSale.CustomerId == cusID && bill.ExpiresOn > DateTime.Now && bill.BillOfSale.Verhicle.LicensePlates.Contains(LicensePlates) ).
                   Select(bill => new BillOfSaleActiveDTO()
                   {
                       InsuranceSoldId = bill.Id,
                       InsuranceId = bill.BillOfSale.InsuranceId,
                       ExpiresOn = bill.ExpiresOn,
                       MaxValueRefund = bill.BillOfSale.Insurance.MaxValueRefund,
                       MaxRefundQuantity = bill.BillOfSale.Insurance.MaxRefundQuantity,
                       DurationMonthUnit = bill.BillOfSale.Insurance.DurationMonthUnit,
                       MinRefundQuantity = bill.BillOfSale.Insurance.MinValueRefund,
                       TotalPrice = bill.BillOfSale.TotalPrice,
                       VersionName = bill.BillOfSale.Verhicle.Version.Name,
                       ModelName = bill.BillOfSale.Verhicle.Model.Name,
                       LicensePlates = bill.BillOfSale.Verhicle.LicensePlates,
                       CreatedAt = bill.BillOfSale.CreatedAt,
                   }).Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();

                var totalItem = insuranceVehicleContext.InsuranceSold.Where(bill => bill.BillOfSale.CustomerId == cusID && bill.ExpiresOn > DateTime.Now).Count();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);
                return new BaseResponseData() { Code = 200, Data = data, Message = "GET By Status Success", currentPage = page, totalPage = totalPage };


            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Message = "Error: " + e.Message.ToString() };
            }
        }

    }
}