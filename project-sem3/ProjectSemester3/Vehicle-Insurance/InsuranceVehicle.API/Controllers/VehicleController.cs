﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.API.Models.BaseRessponse;
using InsuranceVehicle.API.Models.DTO;
using InsuranceVehicle.API.Models.EF;
using InsuranceVehicle.API.Models.Request;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;


namespace InsuranceVehicle.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public VehicleController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }

        [HttpGet("vehicleofcustomer/{customerId}/{page}/{pageSize}")]
        [HttpGet("vehicleofcustomer/{customerId}")]
        public BaseResponseData getAllVehicleOfCustomer(Guid customerId ,string Vehicle, int page = 1 , int pageSize = 20)
        {
            try
            {
                IEnumerable<VehicleDTO> data = null;
                 data = insuranceVehicleContext.Vehicle.Where(vehicle => vehicle.CustomerId == customerId)
                    .Select(vehicle => new VehicleDTO()
                    {
                        Id = vehicle.Id,
                        BodyNumber = vehicle.BodyNumber,
                        EngineNumber = vehicle.EngineNumber,
                        LicensePlates = vehicle.LicensePlates,
                        CustomerId = vehicle.CustomerId,
                        Model = new VehicleModelDTO()
                        {
                            Id = vehicle.Model.Id,
                            Name = vehicle.Model.Name
                        },
                        Version = new VehicleVersionDTO()
                        {
                            Id = vehicle.Version.Id,
                            Name = vehicle.Version.Name
                        },
                    });
                if(Vehicle != "" && Vehicle!= null)
                {
                    var serrchBill = string.IsNullOrEmpty(Vehicle) ? "" : Vehicle.Trim().ToLower();
                    data = data.Where(x => x.LicensePlates.ToLower().Contains(serrchBill) || x.Model.Name.ToLower().Contains(serrchBill) || x.Version.Name.ToLower().Contains(serrchBill));
                }
                var totalItem = data.Count();
                data = data.Skip((page - 1) * pageSize).Take(pageSize).AsQueryable();
                var totalPage = (int)Math.Ceiling((double)totalItem / (double)pageSize);

                return new BaseResponseData() { Code = 200, Data = data, Message = "GET All Vehicle Success", currentPage = page, totalPage = totalPage };

            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpGet("getVehicleById/{id}")]
        public BaseResponseData getVehicleById(int id)
        {
            try
            {
                var data = insuranceVehicleContext.Vehicle.Where(vehicle => vehicle.Id == id)
                    .Select(vehicle => new VehicleDTO()
                    {
                        Id = vehicle.Id,
                        BodyNumber = vehicle.BodyNumber,
                        EngineNumber = vehicle.EngineNumber,
                        LicensePlates = vehicle.LicensePlates,
                        CustomerId = vehicle.CustomerId,
                        Model = new VehicleModelDTO()
                        {
                            Id = vehicle.Model.Id,
                            Name = vehicle.Model.Name
                        },
                        Version = new VehicleVersionDTO()
                        {
                            Id = vehicle.Version.Id,
                            Name = vehicle.Version.Name
                        },
                    }).AsEnumerable();

                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

        [HttpPost("addCustomerVehicle")]
        public BaseResponseEmptyData addCustomerVehicle(Vehicle vec)
        {
            try
            {
                var data = new Vehicle()
                {
                    CustomerId = vec.CustomerId,
                    PurchaseDate = vec.PurchaseDate.Date,
                    BodyNumber = vec.BodyNumber,
                    EngineNumber = vec.EngineNumber,
                    LicensePlates = vec.LicensePlates,
                    ModelId = vec.ModelId,
                    VersionId = vec.VersionId
                };

                insuranceVehicleContext.Vehicle.Add(data);
                insuranceVehicleContext.SaveChanges();

                return new BaseResponseEmptyData() { Code = 200, Message = "Add Vehicle Success!" };
            }
            catch(Exception e)
            {
                return new BaseResponseEmptyData() { Code = 500, Message = "Error: " + e.Message };
            }
        }

        [HttpGet("getModelVersionByVehicleId/{id}")]
        public BaseResponseData getModelVersionByVehicleId(int id)
        {
            try
            {
                var data = insuranceVehicleContext.Vehicle.Where(vec => vec.Id == id)
                    .Select(vec => new Vehicle() { Id = vec.Id, ModelId = vec.ModelId, VersionId = vec.VersionId })
                    .AsQueryable();
                return new BaseResponseData() { Code = 200, Data = data, Message = "Get success" };
            }
            catch (Exception e)
            {
                return new BaseResponseData() { Code = 500, Data = null, Message = "Error: " + e.Message.ToString() };
            }
        }

    }
}
