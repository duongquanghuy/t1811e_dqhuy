﻿using PayPal.Api;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
class PayPalHandler{

  //@ Declare a R/O property to store our own custom configuration for PayPal
  private readonly Dictionary<string,string> _payPalConfig;

  //@ The class constructor
  public PayPalHandler(IConfiguration config){

    //@ Fetch the `appsettings.json` and pack it into the custom configuration
    //@ Only the *clientId*,*clientSecret* and *mode* are required to get an access token (as of the time of writing this)
    _payPalConfig = new Dictionary<string, string>()
    {
        { "clientId" , config.GetSection("paypal:settings:clientId").Value },
        { "clientSecret", config.GetSection("paypal:settings:clientSecret").Value },
        { "mode", config.GetSection("paypal:settings:mode").Value },
        { "business", config.GetSection("paypal:settings:business").Value },
        { "merchantId", config.GetSection("paypal:settings:merchantId").Value },
    };

    //@ Use OAuthTokenCredential to request an access token from PayPal using our custom configuration
    var accessToken = new OAuthTokenCredential(_payPalConfig).GetAccessToken();

    //@ Proceed as desired 

  }
}