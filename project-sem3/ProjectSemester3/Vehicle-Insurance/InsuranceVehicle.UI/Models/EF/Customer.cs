﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class Customer
    {
        public Customer()
        {
            BillCompensation = new HashSet<BillCompensation>();
            BillOfSale = new HashSet<BillOfSale>();
            ClaimDetail = new HashSet<ClaimDetail>();
            Estimate = new HashSet<Estimate>();
            Vehicle = new HashSet<Vehicle>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string RefreshToken { get; set; }
        public DateTime? RefreshTokenExpired { get; set; }

        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetail { get; set; }
        public virtual ICollection<Estimate> Estimate { get; set; }
        public virtual ICollection<Vehicle> Vehicle { get; set; }
    }
}
