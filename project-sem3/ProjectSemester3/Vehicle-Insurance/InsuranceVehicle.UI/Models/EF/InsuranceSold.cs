﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class InsuranceSold
    {
        public InsuranceSold()
        {
            BillCompensation = new HashSet<BillCompensation>();
            ClaimDetail = new HashSet<ClaimDetail>();
        }

        public int Id { get; set; }
        public int BillOfSaleId { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual BillOfSale BillOfSale { get; set; }
        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetail { get; set; }
    }
}
