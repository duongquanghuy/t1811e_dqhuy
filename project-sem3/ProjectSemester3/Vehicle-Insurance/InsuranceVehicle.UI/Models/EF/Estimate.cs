﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class Estimate
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public int? ModelId { get; set; }
        public int? VersionId { get; set; }
        public Guid? CustomerId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual VehicleModel Model { get; set; }
        public virtual VehicleVersion Version { get; set; }
    }
}
