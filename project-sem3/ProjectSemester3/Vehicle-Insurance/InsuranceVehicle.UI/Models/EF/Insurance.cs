﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class Insurance
    {
        public Insurance()
        {
            BillOfSale = new HashSet<BillOfSale>();
        }

        public int Id { get; set; }
        public double PricePercentByCarValue { get; set; }
        public double MinValueRefund { get; set; }
        public double MaxValueRefund { get; set; }
        public int MaxRefundQuantity { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int DurationMonthUnit { get; set; }

        public virtual ICollection<BillOfSale> BillOfSale { get; set; }
    }
}
