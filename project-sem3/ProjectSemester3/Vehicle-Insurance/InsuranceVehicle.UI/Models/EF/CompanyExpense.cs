﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class CompanyExpense
    {
        public int Id { get; set; }
        public double ExpensesAmount { get; set; }
        public int ExpensesTypeId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual ExpenseType ExpensesType { get; set; }
    }
}
