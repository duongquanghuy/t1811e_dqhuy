﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class ClaimDetail
    {
        public ClaimDetail()
        {
            BillCompensation = new HashSet<BillCompensation>();
            ImageInClaim = new HashSet<ImageInClaim>();
        }

        public int Id { get; set; }
        public string BodyClaim { get; set; }
        public Guid CustomerId { get; set; }
        public int? InsuranceId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual InsuranceSold Insurance { get; set; }
        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<ImageInClaim> ImageInClaim { get; set; }
    }
}
