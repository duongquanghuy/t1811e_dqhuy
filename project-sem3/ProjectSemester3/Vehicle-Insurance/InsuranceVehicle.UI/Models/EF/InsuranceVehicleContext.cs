﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class InsuranceVehicleContext : DbContext
    {
        public InsuranceVehicleContext()
        {
        }

        public InsuranceVehicleContext(DbContextOptions<InsuranceVehicleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<BillCompensation> BillCompensation { get; set; }
        public virtual DbSet<BillOfSale> BillOfSale { get; set; }
        public virtual DbSet<CalculatorActualValueVehicle> CalculatorActualValueVehicle { get; set; }
        public virtual DbSet<ClaimDetail> ClaimDetail { get; set; }
        public virtual DbSet<CompanyExpense> CompanyExpense { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Estimate> Estimate { get; set; }
        public virtual DbSet<ExpenseType> ExpenseType { get; set; }
        public virtual DbSet<ImageInClaim> ImageInClaim { get; set; }
        public virtual DbSet<Insurance> Insurance { get; set; }
        public virtual DbSet<InsuranceSold> InsuranceSold { get; set; }
        public virtual DbSet<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }
        public virtual DbSet<VehicleVersion> VehicleVersion { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=t1811e-aptech-group1.database.windows.net;Initial Catalog=InsuranceVehicle;Persist Security Info=True;User ID=aptech-t1811e-group1;Password=12345678@Abc");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Admin)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Admin__RoleId__7C4F7684");
            });

            modelBuilder.Entity<BillCompensation>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Reason).HasColumnType("text");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.AdminId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Admin__7D439ABD");

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ClaimDetailId");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Custo__7E37BEF6");

                entity.HasOne(d => d.InsuranceSold)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.InsuranceSoldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Insur__7F2BE32F");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Verhi__00200768");
            });

            modelBuilder.Entity<BillOfSale>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.PaymentStatus).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CreatedBy)
                    .HasConstraintName("FK__BillOfSal__Creat__02084FDA");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Custo__02FC7413");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.InsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Insur__03F0984C");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Verhi__04E4BC85");
            });

            modelBuilder.Entity<ClaimDetail>(entity =>
            {
                entity.Property(e => e.BodyClaim).HasColumnType("text");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ClaimDeta__Custo__05D8E0BE");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.InsuranceId)
                    .HasConstraintName("FK__ClaimDeta__Insur__06CD04F7");
            });

            modelBuilder.Entity<CompanyExpense>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.ExpensesType)
                    .WithMany(p => p.CompanyExpense)
                    .HasForeignKey(d => d.ExpensesTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CompanyEx__Expen__07C12930");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasIndex(e => e.Email)
                    .HasName("UQ__Customer__A9D10534DB464BB8")
                    .IsUnique();

                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");
            });

            modelBuilder.Entity<Estimate>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Estimate__Custom__08B54D69");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Estimate__ModelI__09A971A2");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Estimate__Versio__0A9D95DB");
            });

            modelBuilder.Entity<ExpenseType>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ImageInClaim>(entity =>
            {
                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.ImageInClaim)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ImageInCl__Claim__0B91BA14");
            });

            modelBuilder.Entity<Insurance>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<InsuranceSold>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExpiresOn).HasColumnType("datetime");

                entity.HasOne(d => d.BillOfSale)
                    .WithMany(p => p.InsuranceSold)
                    .HasForeignKey(d => d.BillOfSaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Insurance__BillO__0C85DE4D");
            });

            modelBuilder.Entity<PriceListVehicleNew>(entity =>
            {
                entity.HasOne(d => d.Model)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__PriceList__Model__0D7A0286");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__PriceList__Versi__0E6E26BF");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.Property(e => e.BodyNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.EngineNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LicensePlates)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Vehicle__Custome__0F624AF8");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Vehicle__ModelId__10566F31");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Vehicle__Version__114A936A");
            });

            modelBuilder.Entity<VehicleModel>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<VehicleVersion>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.VehicleVersion)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__VehicleVe__Model__123EB7A3");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
