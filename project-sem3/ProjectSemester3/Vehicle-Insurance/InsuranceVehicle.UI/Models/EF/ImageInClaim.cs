﻿using System;
using System.Collections.Generic;

namespace InsuranceVehicle.UI.Models.EF
{
    public partial class ImageInClaim
    {
        public int Id { get; set; }
        public int ClaimDetailId { get; set; }
        public string Link { get; set; }

        public virtual ClaimDetail ClaimDetail { get; set; }
    }
}
