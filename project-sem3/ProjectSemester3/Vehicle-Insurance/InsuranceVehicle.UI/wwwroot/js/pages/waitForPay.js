﻿$(document).ready(function () {
    var cusID = getCookie("cus-id");
    $("table").css("display", "none");
    getData();


});
// tim kiem
function searchVehicle() {
    var search = $('#myVehicle').val();

    if (search != '') {
        $('.loader').modal('show');
        getData();
    }
    else {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
}
$("#myVehicle").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
})
function DeleteBill(billOfsaleID) {
    $('#exampleModalDeleteBill').modal('show');
    $('#billOfsaleID').val(billOfsaleID);
    
}
//button Delete
function ComfirmdeleteBillOfSale() {
    $('.loader').modal('show');
    var BillID = $('#billOfsaleID').val();
    $.ajax({
        url: 'https://localhost:44336/api/billOfSale?id=' + BillID + '',
        type: 'Delete',
        dataType: 'json',
        success: function (data) {
            console.log(data.message);
            $('.loader').modal('hide');
            location.reload();
        }
        ,
        error: function () {
            $('.loader').modal('hide');
            alert("Erro From the Systetem");
        },

    });
}
function getData(page = 1, pageSize = 5) {

    var customerID = getCookie("cus-id");
    var url = '';
    var search = $('#myVehicle').val();
    if (search != '' && search != null) {
        url = 'https://localhost:44336/api/billOfSale/getByStatusWait/' + customerID + '/' + page + '/' + pageSize + '?Vehicle=' + search;
    } else {
        url = 'https://localhost:44336/api/billOfSale/getByStatusWait/' + customerID + '/' + page + '/' + pageSize;
    }
    
    $('.load-table').css('display', 'flex');
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (result) {
            if (result.data == null || result.data == '') {
                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
                $('#myVehicle').val('');
                var msgHead = "Announcement";
                var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                showNotificationModal(msgHead, msgBody);
               
            } else {
                $("#bill-list-item").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {

                    $("#bill-list-item").append('<tr>' +
                        '<td data-th="Insurance" >' +
                        '<div>' +
                        '<span class="key-table">Insurance number:   </span><span class="value-table">' + val.insuranceId + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Maximum refund value($):   </span><span class="value-table">' + val.maxValueRefund + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Maximum number of refunds:   </span><span class="value-table">' + val.maxRefundQuantity + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Duration:   </span><span class="value-table">' + val.durationMonthUnit + '</span><span class="key-table"> months</span>' +
                        '</div>' +
                        '</td >' +
                        '<td>' +
                        '<div>' +
                        '<span class="key-table">License Plates:   </span><span class="value-table">' + val.licensePlates + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Vehicle model:   </span><span class="value-table">' + val.modelName + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Vehicle version:   </span><span class="value-table">' + val.versionName + '</span>' +
                        '</div>' +
                        '</td>' +
                        '<td data-th="Price">$<span class="value-table" style="font-size:20px">' + val.totalPrice + '</span></td>' +
                        '<td class="actions" data-th="">' +
                        '<div class="wrap-acitons">' +
                        '<a href="/paypal/paymentwithpaypal/' + val.id + '"><img src="/asset/checkout-logo-small.png" alt="Check out with PayPal"></a>' +
                        '<button class="btn-dot-menu"><i class="fa fa-ellipsis-h" style="font-size:24px; color: #344659;"></i></button>' +
                        '</div>' +
                        '<ul class="action-menu">' +
                        '<li id="btn-edit-item-table" data-target="#edit-bill" data-toggle="modal" onclick="openEditBillModal(' + val.id + ')"><i class="fa fa-edit action-menu-icon" aria-hidden="true"></i>Edit</li>' +
                        '<li id="btn-detelte-item-table" onclick="DeleteBill(' + val.id + ')"><i class="fa fa-trash action-menu-icon" aria-hidden="true"></i>Delete</li>' +
                        '</ul>' +
                        '</td>' +
                        '</tr>');
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                });
                let currentPage = result.currentPage;
                let totalPage = result.totalPage;

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })
                //button option
                $(document).on("click", function () {
                    $(".action-menu").hide();
                });

                $(".btn-dot-menu").on("click", function (event) {
                    event.stopPropagation();
                    if ($(".action-menu").is(":visible")) {
                        $(".action-menu").hide();
                    } else {
                        $(this).closest(".actions").find(".action-menu").slideToggle("fast");
                    }

                });

                $("#btn-edit-item-table").on("click", function () {
                    console.log("click edit");
                });
            }
        }
        ,
        error: function () {
            $("table").css("display", "inline-table");
            $('.load-table').css('display', 'none');

        },
    });

};
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}