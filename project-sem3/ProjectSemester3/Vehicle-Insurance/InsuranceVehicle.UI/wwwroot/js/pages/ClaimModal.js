﻿var currentClaimInsuranceSoldId;
var currentClaimInsuranceId;
var filesUploaded = [];
var files;

$('#btnShowFiles').click(function () {
    $('#image-upload').trigger('click');
});

//lấy id của Bảo hiểm đang được Claim
function openClaimModal(insSoldId, insId) {
    currentClaimInsuranceSoldId = insSoldId;
    currentClaimInsuranceId = insId;
}

$("#image-upload").change(function () {
    if (window.FormData !== undefined) {
        var fileUpload = $('#image-upload').get(0);
        filesUploaded = [];
        files = Array.from(fileUpload.files);
        for (let i = 0; i < files.length; i++) {
            if (files && files[i]) {
                console.log(files[i]);
                showImageLocal(files[i], "imgContainer" + i, i);
            }
        }
    }
});

function showImageLocal(input, id, indexImageSelected) {
    if (input) {
        $('#uploaded-image-list').empty();
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#uploaded-image-list').append(`
                <span id="`+ id + `" style="position: relative; margin: 10px 20px;">
                    <img id="blah" src="` + e.target.result + `" alt="your image" style="width: 100px; height: 150px;" />
                    <i style="position: absolute;" onClick="deleteImage('`+ id + `', ` + indexImageSelected + `)" class="fa fa-times" aria-hidden="true"></i>
                </span>
`);
        };

        reader.readAsDataURL(input);
    }
}

function deleteImage(id, indexImageSelected) {
    files[indexImageSelected] = null;
    var element = `#` + id;
    $(element).remove();
    console.log(files);
}

//gửi Claim Request
function sendClaimRequest() {
    var bodyClaim = $('#body-claim-textarea').val();
    var customerId = getCookie('cus-id');
    var insuranceId = currentClaimInsuranceId;
    var insuranceSoldId = currentClaimInsuranceSoldId;

    var promises = [];
    if (files && files.length !== 0) {
        for (let i = 0; i < files.length; i++) {
            /* $.ajax returns a promise*/
            var request = $.ajax(
                {
                    type: 'POST',
                    headers: {
                        "Authorization": "Client-ID 53ed872c32df698"
                    },
                    url: 'https://api.imgur.com/3/image',
                    contentType: false,
                    processData: false,
                    data: files[i],
                    success: function (result) {
                        filesUploaded.push(result.data.link);

                    },
                    error: function (err) {
                        console.log(err)
                    }
                }
            );

            promises.push(request);
        }
    }



    $.when.apply(null, promises).done(function () {
        var claimRequest = {
            "BodyClaim": bodyClaim,
            "CustomerId": customerId,
            "InsuranceId": insuranceId,
            "InsurancrSoldId": insuranceSoldId,
            "ListImg": filesUploaded
        }
        $.ajax({
            url: 'https://localhost:44336/api/Claim',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(claimRequest),
            dataType: 'json',
            success: function (data) {
                showNotificationModal('Claim Request', data.message);
                closeClaimRequestModal();
                setTimeout(function () { closeNotiModal(); }, 2000);
            },
            error: function () {
                showNotificationModal('Claim Request', data.message);
                setTimeout(function () { closeNotiModal(); }, 2000);
            },
        });
    })
}

//Đóng Modal Thông báo
function closeNotiModal() {
    $('#close-noti-modal-btn').trigger('click');
}

//Đóng Modal Claim Request
function closeClaimRequestModal() {
    $('#close-claim-request-modal-btn').trigger('click');
}