﻿$(document).ready(function () {

    var cusID = getCookie('cus-id');
    console.log(cusID);
    if (cusID == '') {
        alert('ERROR USER! PLEASE CONTACT US TO SUPPORT');
    } else {
        getData();
    }
});
// tim kiem
function searchVehicle() {
    var search = $('#myVehicle').val();

    if (search != '' && search != null) {
        $('.loader').modal('show');
        getData();
    } else {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
}
$("#myVehicle").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
})
function getData(page = 1, pageSize = 5) {

    var customerID = getCookie("cus-id");
    var url = '';
    var search = $('#myVehicle').val();
    if (search != '' && search != null) {
        url = 'https://localhost:44336/api/Vehicle/vehicleofcustomer/' + customerID + '/' + page + '/' + pageSize + '?Vehicle=' + search;
    } else {
        url = 'https://localhost:44336/api/Vehicle/vehicleofcustomer/' + customerID + '/' + page + '/' + pageSize;
    }

    $('.load-table').css('display', 'flex');
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (result) {
            if (result.data == null || result.data == '') {
                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
                $('#myVehicle').val('');
                var msgHead = "Announcement";
                var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                showNotificationModal(msgHead, msgBody);
               
            } else {
                $('#table-buy-insurance').empty();//đưa list về rỗng trước
                $(".pagination").empty();
              
                $.each(result.data, function (index, item) {

                    let html = `<tr>
                                    <td class="column1">`+ item.id + `</td>
                                    <td class="column2">`+ item.bodyNumber + `</td>
                                    <td class="column3">`+ item.engineNumber + `</td>
                                    <td class="column4">`+ item.licensePlates + `</td>
                                    <td class="column5">`+ item.model.name + `</td>
                                    <td class="column6">`+ item.version.name + `</td>
                                </tr>`
                    $("#table-buy-insurance").append(html);
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                });
                let currentPage = result.currentPage;
                let totalPage = result.totalPage;

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })
               
            }
        }
        ,
        error: function () {
            showNotificationModal("Get Customer Vehicle", "Error: Get Customer Vehicle Failed!")
            $("table").css("display", "inline-table");
            $('.load-table').css('display', 'none');

        },
    });

};
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}