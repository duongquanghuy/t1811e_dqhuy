﻿var currentEditVehicleChosenId; //Đánh dấu xe đang được chọn hiện tại
var currentEditBillVehicleId; //Đánh dấu xe đang được chọn trong Bảo hiểm hiện tại (theo CSDL)
var currentEditBillInsuranceId; //Đánh dấu Bảo hiểm hiện tại
var currentEditBillModelId; //Đánh dấu Model của xe đang được chọn trong Bảo hiểm hiện tại
var currentEditBillVersionId; //Đánh dấu Version của xe đang được chọn trong Bảo hiểm hiện tại
var editBillYearsOfUsed; //Đánh dấu Năm sử dụng của xe đang được chọn trong Bảo hiểm hiện tại
var editBillQuantityToBill; //Đánh dấu số lượng Bảo hiểm hiện tại trong Bill
var currentBillOfSaleId; //Đánh dấu Bill hiện tại

//Lấy dữ liệu về Bill từ api
function openEditBillModal(id) {
    currentBillOfSaleId = id;

    $.ajax({
        url: 'https://localhost:44336/api/billOfSale/getBillOfSaleById/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            currentEditBillVehicleId = data.data.verhicleId;
            currentEditBillInsuranceId = data.data.insuranceId;

            getCurrentVehicleInformation(currentEditBillVehicleId);

            $('#insurance-price-value').html(data.data.totalPrice + '$');
        },
        error: function () {
            showNotificationModal('Edit Bill', data.data.Message);
        },
    });

    //Lấy Danh sách các Xe mà Khách hàng hiện có
    getVehiclesOfCustomer();
}

//Lấy các phương tiện của Khách hàng hiện có
function getVehiclesOfCustomer() {
    var customerId = getCookie('cus-id');

    if (customerId == '') {
        alert('Please Login to use our service');

    } else {

        $.ajax({
            url: 'https://localhost:44336/api/Vehicle/vehicleofcustomer/' + customerId,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#customer-vehicle-list').html('');//đưa list về rỗng trước

                if (data.data.length == 0) {
                    $('#customer-vehicle-list').append('You dont have any Vehicle(s) yet!');

                    if (confirm('You dont have any Vehicle(s) yet!\nDo you want to add your vehicle now?')) {
                        return window.location.href = 'https://localhost:44377/insurance-vehicle';
                    }

                } else {
                    $.each(data.data, function (index, item) {//đẩy dữ liệu vào list sau
                        $('#customer-vehicle-list').append(
                            '<li onclick="chooseCustomerVehicle(' + item.id + ')">' +
                            '<a>' +
                            '*Model: <span class="model">' + item.model.name + '</span> |' +
                            'License Plate: <span class="license-plate">' + item.licensePlates + '</span> |' +
                            'Version: <span class="version">' + item.version.name + '</span><br />' +
                            'Engine Number: <span class="engine-number">' + item.engineNumber + '</span> |' +
                            'Body Number: <span class="body-number">' + item.bodyNumber + '</span>' +
                            '</a >' +
                            '</li>');
                    });
                }
            },
            error: function () {
                showNotificationModal("Get Customer Vehicle", "Error: Get Customer Vehicle Failed!")
            },
        });
    }
}

//Lấy thông tin về xe đang được bảo hiểm hiện tại
function getCurrentVehicleInformation(id) {
    $.ajax({
        url: 'https://localhost:44336/api/vehicle/getVehicleById/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#model').html(data.data[0].model.name);
            $('#license-plate').html(data.data[0].licensePlates);
            $('#version').html(data.data[0].version.name);
            $('#engine-number').html(data.data[0].engineNumber);
            $('#body-number').html(data.data[0].bodyNumber);
        },
        error: function () {
            showNotificationModal('Edit Bill', data.data.Message);
        },
    });
}

//Chọn xe cho Khách hàng để áp dụng Bảo hiểm vào
function chooseCustomerVehicle(id) {
    currentEditBillVehicleId = id;

    $.ajax({
        url: 'https://localhost:44336/api/vehicle/getVehicleById/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            currentEditBillModelId = data.data[0].model.id;
            currentEditBillVersionId = data.data[0].version.id;

            $('#model').html(data.data[0].model.name);
            $('#license-plate').html(data.data[0].licensePlates);
            $('#version').html(data.data[0].version.name);
            $('#engine-number').html(data.data[0].engineNumber);
            $('#body-number').html(data.data[0].bodyNumber);

            getPriceInsurance();

            showNotificationModal("Vehicle Chosen By Customer", "Vehicle Chosen Success!:<br>*Model: <b>" + data.data[0].model.name + "</b><br>* Version: <b>" + data.data[0].version.name + "</b><br>*License Plate: <b>" + data.data[0].licensePlates + "</b>");
        },
        error: function () {
            showNotificationModal("Vehicle Chosen By Customer", "Error: Get Vehicle Chosen By Customer Error!");
        },
    });
}

//Lấy giá tạm tính của Bảo hiểm đối với Xe được chọn
function getPriceInsurance() {
    editBillYearsOfUsed = $('#years-of-used').val();
    editBillQuantityToBill = $('#insurance-quantity').val();

    $('#insurance-price-noti').css('display', 'block');
    $('#btn-create-bill').css('display', 'block');

    $.ajax({
        url: 'https://localhost:44336/api/common/priceInsurance?insuranceId=' + currentEditBillInsuranceId +
            '&modelId=' + currentEditBillModelId + '&vesionId=' + currentEditBillVersionId + '&quantityYearUsed=' + editBillYearsOfUsed + '&quantity=' + editBillQuantityToBill,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#insurance-price-value').html(data.data + '$');
            $('#btn-create-bill').css('display', 'block');
        },
        error: function () {
            showNotificationModal('Get Price Insurance', "Error: Get Price Insurance Failed!");
        },
    });
}

//Cập nhật lại Bill
function updateBillAction() {
    updateBillCustomerId = getCookie('cus-id');

    var updateBill = {
        'CustomerId': updateBillCustomerId,
        'Id': currentBillOfSaleId,
        'insuranceId': currentEditBillInsuranceId,
        'modelId': currentEditBillModelId,
        'vesionId': currentEditBillVersionId,
        'quantityYearUsed': parseInt(editBillYearsOfUsed),
        'quantity': parseInt(editBillQuantityToBill)
    }

    $.ajax({
        url: 'https://localhost:44336/api/billOfSale',
        type: 'PUT',
        data: JSON.stringify(updateBill),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            showNotificationModal('Update Bill', "Update Bill Success!");
            getData(1, 10);
        },
        error: function () {
            showNotificationModal('Update Bill', "Update Bill Success!");
            getData(1, 10);
        },
    });
}