﻿$(document).ready(function () {
    var cusID = getCookie("cus-id");
    $("table").css("display", "none");
    getData();

});
//tim kiem
function searchVehicle() {
    var search = $('#myVehicle').val();

    if (search != '') {
        $('.loader').modal('show');
        getData();
    } else {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
}
$("#myVehicle").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
});
function getData(page = 1, pageSize = 5) {

    var customerID = getCookie("cus-id");
    $('.load-table').css('display', 'flex');
    var url = '';
    var search = $('#myVehicle').val();
    if (search != '' && search != null) {
        url = 'https://localhost:44336/api/billOfSale/getByStatusPaid/' + customerID + '/' + page + '/' + pageSize + '?Vehicle=' + search;
    } else {
        url = 'https://localhost:44336/api/billOfSale/getByStatusPaid/' + customerID + '/' + page + '/' + pageSize;
    }
    $.ajax({
        type: 'GET',
        url: url,
        dataType: 'json',
        success: function (result) {
            if (result.data == null || result.data == '') {
                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
                $('#myVehicle').val('');
                var msgHead = "Announcement";
                var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                showNotificationModal(msgHead, msgBody);
               
            } else {
                $("#bill-list-item-paid").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {
                    console.log(val);
                    var expiresOnf = formatted(val.expiresOn);
                    var createdAtf = formatted(val.createdAt);

                    $("#bill-list-item-paid").append('<tr>' +
                        '<td data-th="Insurance">' +
                        '<div>' +
                        '<span class="key-table">Insurance number:   </span><span class="value-table">' + val.verhicleId + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Maximum refund value($):   </span><span class="value-table">' + val.maxValueRefund + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Maximum number of refunds:   </span><span class="value-table">' + val.maxRefundQuantity + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Duration:   </span><span class="value-table">' + val.durationMonthUnit +  '</span><span class="key-table"> months</span>' +
                        '</div>' +
                        '</td>' +
                        '<td>' +
                        '<div>' +
                        '<span class="key-table">License Plates:   </span><span class="value-table">' + val.licensePlates + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Vehicle model:   </span><span class="value-table">' + val.modelName + '</span>' +
                        '</div>' +
                        '<div>' +
                        '<span class="key-table">Vehicle version:   </span><span class="value-table">' + val.versionName + '</span>' +
                        '</div>' +
                        '</td>' +

                        '<td>' +
                        '<span class="key-table datimeBill">Expires On:   </span><span class="value-table">' + expiresOnf + '</span>' +
                        '</td>' +
                        '<td><span class="key-table datimeBill">Date Created:   </span><span class="value-table">' + createdAtf + '</span></td>' +
                        '<td data-th="Price">$<span class="value-table" style="font-size:20px">' + val.totalPrice + '</span></td>' +

                        '</tr>');
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                });
                let currentPage = result.currentPage;
                let totalPage = result.totalPage;

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })
            }
        }
        ,
        error: function () {
            $("table").css("display", "inline-table");
            $('.load-table').css('display', 'none');
        },

    });
}
// format datime
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;

    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}