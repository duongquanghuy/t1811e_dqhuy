﻿function openContent(evt, tabName) {
    //Lấy toàn bộ nội dung trong các tab-content và che dấu nó đi
    $('#present-tag-content').find('.tab-content').css('display', 'none');

    //Lấy toàn bộ các thành phần có class tablinks và đổi thành trạng thái bình thường"
    $('#present-tags > ul').find('.tab-links').removeClass("present-tag-active");

    //Hiện nội dung của tab hiện tại, và thêm class active cho nó
    $('#' + tabName).css('display', 'block');
    evt.currentTarget.className += " present-tag-active";
}

function openFaqTag(evt) {
    $('#faqs-area').find('.faq').removeClass('faq-active');
    $('.faq').find('.faq-question').removeClass('faq-q-active');
    $('.faq').find('.faq-sign').removeClass('faq-s-active');
    $('.faq').find('.faq-answer').removeClass('faq-a-active');
    $('.faq-sign').html('+');

    evt.currentTarget.className += " faq-active";

    $('#faqs-area > .faq-active > p').addClass('faq-q-active');
    $('#faqs-area > .faq-active > span').addClass('faq-s-active');
    $('#faqs-area > .faq-active > span').html('−');
    $('#faqs-area > .faq-active > div').addClass('faq-a-active');
}

function closeFaqTag(evt) {
    console.log(evt.currentTarget.className);
    if (evt.currentTarget.className == 'faq-sign faq-s-active') {
        setTimeout(function () {
            $('#faqs-area').find('.faq').removeClass('faq-active');
            $('.faq').find('.faq-question').removeClass('faq-q-active');
            $('.faq').find('.faq-sign').removeClass('faq-s-active');
            $('.faq').find('.faq-answer').removeClass('faq-a-active');
            $('.faq-sign').html('+');
        }, 1);
    }
}