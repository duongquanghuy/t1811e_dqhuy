﻿//Mở Form Thêm Xe
function openAddNewVehicleForm() {
    $('#add-new-vehicle-form').css('display', 'block');
    $('#btn-open-add-new-vehicle-form').css('display', 'none');
    $('#btn-close-add-new-vehicle-form').css('display', 'initial');

    $('#customer-chosen-vehicle').css('display', 'none');
    $('#insurance-price-noti').css('display', 'none');
    $('#btn-create-bill').css('display', 'none');
}

//Đóng Form Thêm Xe
function closeAddNewVehicleForm() {
    $('#add-new-vehicle-form').css('display', 'none');
    $('#btn-open-add-new-vehicle-form').css('display', 'initial');
    $('#btn-close-add-new-vehicle-form').css('display', 'none');

    $('#new-vehicle-body-number').val('');
    $('#new-vehicle-engine-number').val('');
    $('#new-vehicle-license-plate').val('');
    $('#new-vehicle-version').html('');
    $('#new-vehicle-purchase-date').val('');
    $('#new-vehicle-model').val('');
}

//Các phương thức sẽ thực hiện khi mở Modal
var insuranceToBillId,
    modelToBillId,
    versionToBillId,
    yearsOfUsedToBill,
    quantityToBill,
    verhicalToBillId,
    insuranceDurationMonthlyToBill,
    customerToBillId,
    newVehicleModelId;
//Các biến hỗ trợ tạo hóa đơn


function openBuyInsuranceModal(id) {

    getInsuranceInformationInModal(id);

    getVehiclesOfCustomer();
    
}

//Lấy thông tin bảo hiểm đẩy vào Modal
function getInsuranceInformationInModal(id) {
    //cái này sẽ dựa vào sự kiện onlick ở nút của bảo hiểm nào thì lấy theo id của bảo hiểm đấy
    insuranceToBillId = id;

    $.ajax({
        url: 'https://localhost:44336/api/insurance/getInsuranceById/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#price-percent-by-car-value').html(data.data.pricePercentByCarValue);
            $('#min-value-refund').html(data.data.minValueRefund);
            $('#max-value-refund').html(data.data.maxValueRefund);
            $('#max-refund-quantity').html(data.data.maxRefundQuantity);
            $('#duration-month-unit').html(data.data.durationMonthUnit);

            insuranceDurationMonthlyToBill = data.data.durationMonthUnit;
        },
        error: function () {
            showNotificationModal("Get Insurance Information", "Error: Get Insurance Failed!");
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Lấy các phương tiện của Khách hàng hiện có
function getVehiclesOfCustomer() {
    var customerId = getCookie('cus-id');

    if (customerId == '') {
        alert('Please Login to use our service');

    } else {

        $.ajax({
            url: 'https://localhost:44336/api/Vehicle/vehicleofcustomer/' + customerId,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#customer-vehicle-list').html('');//đưa list về rỗng trước

                if (data.data.length == 0) {
                    $('#customer-vehicle-list').append('You dont have any Vehicle(s) yet!');

                    if (confirm('You dont have any Vehicle(s) yet!\nDo you want to add your vehicle now?')) {
                        openAddNewVehicleForm();
                    }

                } else {
                    $.each(data.data, function (index, item) {//đẩy dữ liệu vào list sau
                        $('#customer-vehicle-list').append(
                            '<li onclick="chooseCustomerVehicle(' + item.id + ')">' +
                            '<a>' +
                            '*Model: <span class="model">' + item.model.name + '</span> |' +
                            'License Plate: <span class="license-plate">' + item.licensePlates + '</span> |' +
                            'Version: <span class="version">' + item.version.name + '</span><br />' +
                            'Engine Number: <span class="engine-number">' + item.engineNumber + '</span> |' +
                            'Body Number: <span class="body-number">' + item.bodyNumber + '</span>' +
                            '</a >' +
                            '</li>');
                    });
                }
            },
            error: function () {
                showNotificationModal("Get Customer Vehicle", "Error: Get Customer Vehicle Failed!");
                setTimeout(function () { closeNotificationModal() }, 2000);
            },
        });
    }
}


//Chọn xe cho Khách hàng để áp dụng Bảo hiểm vào
function chooseCustomerVehicle(id) {
    verhicalToBillId = id;

    $.ajax({
        url: 'https://localhost:44336/api/vehicle/getVehicleById/' + id,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            modelToBillId = data.data[0].model.id;
            versionToBillId = data.data[0].version.id;

            $('#customer-chosen-vehicle').css('display', 'block');

            $('#model').html(data.data[0].model.name);
            $('#license-plate').html(data.data[0].licensePlates);
            $('#version').html(data.data[0].version.name);
            $('#engine-number').html(data.data[0].engineNumber);
            $('#body-number').html(data.data[0].bodyNumber);

            $('#years-of-used').val(1);
            $('#insurance-quantity').val(1);

            getPriceInsurance();

            showNotificationModal("Get Vehicle Chosen By Customer", "<b>Vehicle Chosen Success!:</b><br>*Model: <b>" + data.data[0].model.name + "</b><br>*Version: <b>" + data.data[0].version.name + "</b><br>*License Plate: <b>" + data.data[0].licensePlates + "</b>");

            setTimeout(function () { closeNotificationModal() }, 2000);
        },
        error: function () {
            showNotificationModal("Get Vehicle Chosen By Customer", "Error: Get Vehicle Chosen By Customer Error!");
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Ẩn đi phương tiện đã chọn (khi bật/ tắt Modal)
function hideChosenCustomerVehicle() {
    $('#customer-chosen-vehicle').css('display', 'none');
}

//Lấy các Model phương tiện mà Bảo hiểm hỗ trợ
function getVehicleModel() {
    var searchString = $('#new-vehicle-model').val();

    if (searchString != '') {
        $.ajax({
            url: 'https://localhost:44336/api/common/searchVehicleModelByName/' + searchString,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                $('#search-result-area').css('display', 'block');

                $('#search-result-list').css('display', 'block');
                $('#search-result-list').html('');//đưa list về rỗng trước

                if (data.data.length == 0) {
                    $('#search-result-list').append("<li>We dont support this Model!</li>");
                } else {
                    $.each(data.data, function (index, item) {
                        $('#search-result-list').append(
                            '<li onclick="getVehicleVersionByModel(' + item.id + ', \'' + item.name + '\')">' + item.name + '</li>'
                        );
                    });
                }
            },
            error: function () {
                showNotificationModal("Get Vehicle Model", "Error: Get Vehicle Models Failed!");
                setTimeout(function () { closeNotificationModal() }, 2000);
            },
        });
    } else {
        hideSearchResult();
    }
}

//Ẩn đi các kết quả về Model được tìm kiếm
function hideSearchResult() {
    setTimeout(function () {
        $('#search-result-list').css('display', 'none');
        $('#search-result-list').html('');
        $('#new-vehicle-version').html('');
    }, 100);
}

//Lấy các Version phương tiện theo Model được chọn
function getVehicleVersionByModel(modelId, modelName) {
    newVehicleModelId = modelId;

    $.ajax({
        url: 'https://localhost:44336/api/common/versionByVehicleModel/' + modelId,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#new-vehicle-version').html('');//đưa list option về rỗng trước

            $('#search-result-area').css('display', 'none');

            $('#new-vehicle-model').val(modelName);

            $('#search-result-list').html('');//đưa list về rỗng

            if (data.data.length == 0) {
                $('#btn-add-new-vehicle').css('display', 'none');
                $('#none-version-warning').css('display', 'block');

            } else {
                $.each(data.data, function (index, item) {//đẩy các version vào list option
                    $('#new-vehicle-version').append(
                        '<option value="' + item.id + '">' + item.name + '</option>'
                    );
                });

                $('#btn-add-new-vehicle').css('display', 'block');
                $('#none-version-warning').css('display', 'none');
            }
        },
        error: function () {
            showNotificationModal("Get Vehicle Model's Version", "Error: Get Vehicle Model's Version Failed!");
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Thêm Xe cho Khách hàng
function addCustomerVehicle() {
    var customerId = getCookie('cus-id');
    var bodyNumber = $('#new-vehicle-body-number').val();
    var engineNumber = $('#new-vehicle-engine-number').val();
    var licensePlate = $('#new-vehicle-license-plate').val();
    var modelId = newVehicleModelId;
    var versionId = $('#new-vehicle-version').val();
    var purchaseDate = $('#new-vehicle-purchase-date').val();

    var newVehicle = {
        "CustomerId": customerId,
        "PurchaseDate": purchaseDate,
        "BodyNumber": bodyNumber,
        "EngineNumber": engineNumber,
        "LicensePlates": licensePlate,
        "ModelId": modelId,
        "VersionId": parseInt(versionId)
    }

    $.ajax({
        url: 'https://localhost:44336/api/vehicle/addCustomerVehicle',
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(newVehicle),
        dataType: 'json',
        success: function (data) {
            getVehiclesOfCustomer();
            showNotificationModal("Add Vehicle for Customer", "Add Vehicle <b>" + licensePlate + "</b> Success!");
            closeAddNewVehicleForm();
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
        error: function () {
            showNotificationModal("Add Vehicle for Customer", "Add Vehicle Failed!");
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Lấy giá tạm tính của Bảo hiểm đối với Xe được chọn
function getPriceInsurance() {
    yearsOfUsedToBill = $('#years-of-used').val();
    quantityToBill = $('#insurance-quantity').val();

    $('#insurance-price-noti').css('display', 'block');
    $('#btn-create-bill').css('display', 'block');

    $.ajax({
        url: 'https://localhost:44336/api/common/priceInsurance?insuranceId=' + insuranceToBillId +
            '&modelId=' + modelToBillId + '&vesionId=' + versionToBillId + '&quantityYearUsed=' + yearsOfUsedToBill + '&quantity=' + quantityToBill,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#insurance-price-value').html(data.data + '$');
        },
        error: function () {
            showNotificationModal('Get Price Insurance', "Error: Get Price Insurance Failed!");
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Tạo Hóa Đơn
function createBillAction() {
    customerToBillId = getCookie('cus-id');
    yearsOfUsedToBill = $('#years-of-used').val();
    quantityToBill = $('#insurance-quantity').val();

    var billOfSale = {
        'CustomerId': customerToBillId,
        'VerhicleId': verhicalToBillId,
        'InsuranceDurationMonthly': 12,
        'insuranceId': insuranceToBillId,
        'modelId': modelToBillId,
        'vesionId': versionToBillId,
        'quantityYearUsed': parseInt(yearsOfUsedToBill),
        'quantity': parseInt(quantityToBill)
    }

    $.ajax({
        url: 'https://localhost:44336/api/billOfSale',
        type: 'POST',
        data: JSON.stringify(billOfSale),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            showNotificationModal('Create Bill', data.message);
            $('#close-create-bill-modal').trigger('click');
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
        error: function () {
            showNotificationModal('Create Bill', data.message);
            setTimeout(function () { closeNotificationModal() }, 2000);
        },
    });
}

//Đóng Modal Thông báo
function closeNotificationModal() {
    $('#close-noti-modal-btn').trigger('click');
}