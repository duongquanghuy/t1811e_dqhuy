﻿var insuranceSoldIdEdit;
var customerIdEdit;
var claimIdEdit;
var claimImgToEdit = [];
var filesUploaded = [];
var files;
//Hàm thực hiện khi mở Edit Claim Modal
function openEditClaimModal(id) {
    customerIdEdit = getCookie('cus-id');
    claimIdEdit = id;
    
    getClaimContent(customerIdEdit, claimIdEdit);
}

//Hàm lấy thông tin của Claim đang được sửa
function getClaimContent(customerIdEdit, claimIdEdit) {
    $.ajax({
        url: 'https://localhost:44336/api/Claim/search/' + customerIdEdit + '/' + claimIdEdit,
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            $('#edit-body-claim-textarea').val(data.data.bodyClaim);
            insuranceSoldIdEdit = data.data.insuranceSolid;
            console.log(insuranceSoldIdEdit);
        },
        error: function () {
            showNotificationModal('Get Claim Details', 'Get Claim Failed!')
        },
    });
    $.ajax({
        type: 'GET',
        url: 'https://localhost:44336/api/claim/getAllImg/' + claimIdEdit + '',
        dataType: 'json',
        success: function (result) {
            $('#showImgClaim').empty();
            $.each(result.data, function (index, val) {

                $('#showImgClaim').append('<a id="imgclaim-' + val.id + '" style="position: relative; margin: 10px 20px; dislay:inline-block">' +
                        '<img class="myImg' + val.id + ' imgRemove" src ="' + val.link +'" alt= "Trolltunga, Norway" style="max-width:300px; max-height:300px;" >'+
                    '<i style="position: absolute;" onClick="deleteImageClaim(' + val.id + ')" class="fa fa-times" aria-hidden="true"></i></a>');
            });

        },
        error: function () {
            console.log('Error');
        }

    });
}
function deleteImageClaim(id) {
 

    claimImgToEdit.push(id);
    console.log(id);
    $('#imgclaim-' + id).remove();
}

//Hàm xác nhận đẩy dữ liệu đã chỉnh sửa lên server
function confirmEditClaim() {
    $('#notification-modal-container').modal('show');
    $('#edit-claim').modal('hide');
  
}
$('close-noti-modal-btn').on(function () {
    $('#edit-claim').modal('show');

})
//Hàm đẩy dữ liệu đã chỉnh sửa lên server
function editClaim() {
    $('#notification-modal-container').modal('hide');
    //gửi Claim Request
    var bodyClaimEdit = $('#edit-body-claim-textarea').val();

        var promises = [];
        if (files && files.length !== 0) {
            for (let i = 0; i < files.length; i++) {
                /* $.ajax returns a promise*/
                var request = $.ajax(
                    {
                        type: 'POST',
                        headers: {
                            "Authorization": "Client-ID 53ed872c32df698"
                        },
                        url: 'https://api.imgur.com/3/image',
                        contentType: false,
                        processData: false,
                        data: files[i],
                        success: function (result) {
                            filesUploaded.push(result.data.link);

                        },
                        error: function (err) {
                            console.log(err)
                        }
                    }
                );

                promises.push(request);
            }
        }



        $.when.apply(null, promises).done(function () {
            var claimEdit = {
                "InsurancrSoldId": insuranceSoldIdEdit,
                "CustomerId": customerIdEdit,
                "ClaimId": claimIdEdit,
                "BodyClaim": bodyClaimEdit,
                "ListImg": filesUploaded,
                "ImgID": claimImgToEdit
            }

            console.log(claimEdit);

            var claimEditJson = JSON.stringify(claimEdit);

            console.log(claimEditJson);

            $.ajax({
                url: 'https://localhost:44336/api/claim/',
                type: 'PUT',
                data: claimEditJson,
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) {
                    filesUploaded = null;
                    $('#uploaded-image-list').empty();
                    showNotificationModalClaim('Edit Claim', 'Edit Your Claim Success!');
                    getData(1, 1);
                   
                    
                },
                error: function () {
                    filesUploaded = null;
                    $('#uploaded-image-list').empty();
                    showNotificationModalClaim('Edit Claim', data.message);
                   
                },
            });
        })
  }


  

   
