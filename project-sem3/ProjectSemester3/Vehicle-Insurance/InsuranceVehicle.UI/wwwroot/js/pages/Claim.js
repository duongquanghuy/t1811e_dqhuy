﻿$(document).ready(function () {
    $("table").css("display", "none");
    getData();
});
//tim kiem
function searchVehicle() {
    var search = $(`#myVehicle`).val();

    if (search != ``) {
        $(`.loader`).modal(`show`);
        getData();
    } else {
        getData();
    }
    $(`.loader`).modal(`hide`);
    $(`.load-table`).css(`display`, `none`);
}
$("#myVehicle").on(`keyup`, function (e) {
    if (e.keyCode === 13) {
        getData();
    }
    $(`.loader`).modal(`hide`);
    $(`.load-table`).css(`display`, `none`);
});
// delate claimdetail
function DeleteClaim(claimId, claimStatus) {

    console.log(claimStatus)
    if (claimStatus == 0 || claimStatus == -1) {
        $(`#exampleModalDeleteClaim`).modal(`show`);
        $(`#ClaimID`).val(claimId);
    } else {
        //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
        var msgHead = `Deleted Claim`;
        var msgBody = `You request has been paid cannot be deleted`;
        showNotificationModalClaim(msgHead, msgBody); 
    }
}
function ConfirmDeleteClaim() {
    $(`.loader`).modal(`show`);
    var CusIdDelete = getCookie("cus-id");
    var claimIdDelete = $(`#ClaimID`).val();
    var claimIDparse = parseInt(claimIdDelete);
    var claimToDelete = { CustomerId: CusIdDelete, Id: claimIDparse };
    console.log(claimToDelete);
    $.ajax({
        url: `https://localhost:44336/api/claim/delete`,
        type: `DELETE`,
        dataType: `json`,
        contentType: `application/json`,
       
        data: dataClaim,
        
        success: function (result) {
                $(`.loader`).modal(`hide`);
                $(`#exampleModalDeleteClaim`).modal(`hide`);
                var msgHead = `Deleted Claim`;
                var msgBody = `Delete Successful`;
                setTimeout(showNotificationModal(msgHead, msgBody), 3000);
                location.reload();
        },
        error: function () {
            $(`.loader`).modal(`hide`);
            $(`#exampleModalDeleteClaim`).modal(`hide`);
        }
    });

}

function ShowImgClaim(id) {
    $.ajax({
        type: `GET`,
        url: `https://localhost:44336/api/claim/getAllImg/` + id + ``,
        dataType: `json`,
        success: function (result) {
            
            $.each(result.data, function (index, val) {

                $(`#displayImgs`).append(`<img onclick="showThisImg(` + val.id + `)"  class="myImg` + val.id + ` imgRemove" src=` + val.link + ` alt="Trolltunga, Norway" style="max-width:300px; max-height:300px;" >`);
            });
          
        },
        error: function () {
            console.log(`Error`);
        }
        
    });

}

// Lấy phần Modal
var modal = document.getElementById(`myModal-img`);
// Lấy đường dẫn của hình ảnh và gán vào trong phần Modal

var modalImg = document.getElementById("img01");

function showThisImg(imgId) {
    modal.style.display = "block";
    modalImg.src = $(`.myImg` + imgId + ``).attr("src");
    $(`#exampleModalCenter`).modal(`hide`);
}
// lấy button span có chức năng đóng Modal
var span = document.getElementsByClassName("close-img")[0];

//Khi button được click, đóng modal
span.onclick = function () {
    modal.style.display = "none";
    $(`#exampleModalCenter`).modal(`show`);
}
// xóa các thẻ img
function RemoveImg() {
    $(".imgRemove").remove();
}

function showNotificationModalClaim(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $(`#notification-modal-container-claim`).modal(`show`);
    $(`#noti-modal-header-claim`).html(msgHead);
    $(`#noti-modal-body-claim`).html(msgBody);
    $(`#noti-modal-button-claim`).trigger("click");
}

//tim kiem thoe status
$("#type-claim").on("change", function () {
   
    getData();
})
//nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 5) {

    var customerID = getCookie("cus-id");
    $(`.load-table`).css(`display`, `flex`);
   
    var search = $(`#myVehicle`).val();

    var status = $(`#type-claim`).val();
    let url = `https://localhost:44336/api/claim/claimDetail/` + customerID + `?page=` + page + `&pageSize=` + pageSize; 
    if ( status != null && status != ``) {
        url = url + `&status=` + status;
    }
    if(search != `` && search != null) {
       url = url +`&Vehicle=` + search;
    } 
    $.ajax({
        type: `GET`,
        url: url,
        dataType: `json`,

        success: function (result) {
            if (result.data == null || result.data == ``) {
                $("table").css("display", "inline-table");
                $(`.load-table`).css(`display`, `none`);
                $(`#myVehicle`).val(``);
                var msgHead = "Announcement";
                var msgBody = `<p style="text-algin : center">Currently the System has no data</p>`;
                showNotificationModalClaim(msgHead, msgBody);
            } else {
                $("#bill-list-item-claim").empty();
                $(".pagination").empty();
                $.each(result.data, function (index, val) {
                    var statusClaim;
                    if (val.claimStatus > 0) {
                        statusClaim = `<sapn class="claim-Approved">Approved</span>`;
                    } else {
                        statusClaim = `<sapn class="claim-wait">Wait</span>`;
                    }
                    var datetimeformat = formatDateTime(val.createAt);
                    console.log(datetimeformat);
                    var claimStatus = val.claimStatus;
                    switch (parseInt(claimStatus)) {
                        case 0:
                            status = "Pending";
                            break;
                        case 1:
                            status = "Success";
                            break;
                        case -1:
                            status = "Rejected";
                            break;
                        case 2:
                            status = "Accepted";
                            break;
                        default:
                            status = "Unknown";
                            break;
                    }

                    $("#bill-list-item-claim").append(`<tr>` +
                        `<td data-th="Insurance">` +
                        `<div>` +
                        `<span class="key-table">Body Claim:   </span><span class="value-table">` + val.bodyClaim + `</span>` +
                        `</div>` +
                        `</td>` +
                        `<td>` +
                        `<div>` +
                        `<span class="key-table">License Plates:   </span><span class="value-table">` + val.licensePlates + `</span>` +
                        `</div>` +
                        `<div>` +
                        `<span class="key-table">Vehicle model:   </span><span class="value-table">` + val.vehicleModelName + `</span>` +
                        `</div>` +
                        `<div>` +
                        `<span class="key-table">Vehicle version:   </span><span class="value-table">` + val.vehicleVersionName + `</span>` +
                        `</div>` +
                        `</td>` +
                        `<td><span class="key-table datimeBill">Create Datime:   </span><span class="value-table">` + datetimeformat + `</span></td>` +
                        `<td class="color-` + status + `">` + status + `</td>` +
                        `<td class="actions" data-th="">` +
                        `<div class="wrap-acitons">` +
                        `<button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalCenter" onclick="ShowImgClaim(` + val.id +`)">Picture <i class="fa fa-picture-o" aria-hidden="true"></i></button>` +
                        `<button class="btn-dot-menu"><i class="fa fa-ellipsis-h" style="font-size:24px; color: #344659;"></i></button>` +
                        `</div>` +
                        `<ul class="action-menu">` +
                        `<li id="btn-edit-item-table" data-toggle="modal" data-target="#edit-claim" onclick="openEditClaimModal(` + val.id + `)"><i class="fa fa-edit action-menu-icon" aria-hidden="true"></i>Edit</li>` +
                        `<li id="btn-detelte-item-table" onclick="DeleteClaim(` + val.id + ` , ` + claimStatus + `)"><i class="fa fa-trash action-menu-icon" aria-hidden="true"></i>Delete</li>` +
                        `</ul>` +
                        `</td>` +
                        `</tr>`);
                    $("table").css("display", "inline-table");
                    $(`.load-table`).css(`display`, `none`);

                });

                let currentPage = result.currentPage;
                let totalPage = result.totalPage;
                if (page > totalPage) {
                    getData(1, 10);
                }
                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                <a class="page-link" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
            </li>`)
                }

                if (currentPage - 1 > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                }

                if (currentPage > 0) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                }

                if (currentPage + 1 <= totalPage) {
                    $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
                }

                $(".pagination > li").on("click", function () {
                    getData($(this).val());
                })


                //button option
                $(document).on("click", function () {
                    $(".action-menu").hide();
                });

                $(".btn-dot-menu").on("click", function (event) {
                    event.stopPropagation();
                    if ($(".action-menu").is(":visible")) {
                        $(".action-menu").hide();
                    } else {
                        $(this).closest(".actions").find(".action-menu").slideToggle("fast");
                    }

                });

                $("#btn-edit-item-table").on("click", function () {
                    console.log("click edit");
                });
            }
        }
        ,
        error: function () {

            $("table").css("display", "block");
            $(`.load-table`).css(`display`, `none`);
        },
    });

}
function formatDateTime(dateTime) {
    var date = new Date(dateTime);
    return ((date.getMonth() > 8) ? (date.getMonth() + 1) : (`0` + (date.getMonth() + 1))) + `/` + ((date.getDate() > 9) ? date.getDate() : (`0` + date.getDate())) + `/` + date.getFullYear();
}