﻿$(document).ready(function () {
    $("table").css("display", "none");
    getData();

});
function getData(page = 1, pageSize = 10) {
    $('.load-table').css('display', 'flex');
    $.ajax({
        type: "GET",
        url: 'https://localhost:44336/api/Insurance/' + page + '/' + pageSize,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            $("#bill-list-item").empty();
            $(".pagination").empty();
            $("tbody").empty();
            $.each(response.data, function (index, data) {

                let html = `<tr>
            <td class="column1">`+ data.id + ` - ` + data.name + `<i onClick="showInfoInsurance('` + data.name + `','` + replaceBreakLine(data.description) +`')" style="cursor:pointer;margin-left: 10px;" class="fa fa-info-circle" aria-hidden="true"></i></td>
            <td class="column2">`+ data.maxValueRefund + `$</td>
            <td class="column3">`+ data.durationMonth + ` (months)</td>
            <td class="column4">` + data.pricePercentByCarValue + `%</td>
            <td class="column5 column-action"><button type="button" class="btn btn-add-to-cart" onclick="openBuyInsuranceModal(` + data.id + `)"><i class="fa fa-cart-plus" aria-hidden="true"></i> Add to cart</button></td>
        </tr>`

                $("tbody").append(html);

                $("table").css("display", "inline-table");
                $('.load-table').css('display', 'none');
            });
            $(".btn-add-to-cart").on("click", function () {
                let cusId = getCookie("cus-id");
                if (cusId) {
                    $("#buy-insurance").modal("show");
                } else {
                    location.href = "login";
                }
            })
            let currentPage = response.currentPage;
            let totalPage = response.totalPage;

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                            <a class="page-link" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                    </li>`)
            }

            if (currentPage - 1 > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
            }

            if (currentPage > 0) {
                $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
            }

            if (currentPage + 1 <= totalPage) {
                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                <a class="page-link" aria-label="Next">
                     <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
            </li>`)
            }

            $(".pagination > li").on("click", function () {
                getData($(this).val());
            })
        },
        error: function (response) {
            $('.modelDataTable').css('display', 'none');
            alert("Erro From the Systetem");
        }
    });

    $(document).on("click", function () {
        $(".action-menu").hide();
    });

    $(".btn-dot-menu").on("click", function (event) {
        event.stopPropagation();
        if ($(".action-menu").is(":visible")) {
            $(".action-menu").hide();
        } else {
            $(this).closest(".actions").find(".action-menu").slideToggle("fast");
        }

    });

    $("#btn-edit-item-table").on("click", function () {
        console.log("click edit");
    });
}

function showInfoInsurance(insuranceName, insuranceDescription) {
    $("#common-modal-wrap-content").attr("class", "modal-dialog modal-xl");
    $("#common-modal .modal-header h4").text(insuranceName);
    $("#common-modal .modal-body").empty();
    $("#common-modal .modal-body").append(`
                 <div class="form-group">
                    ` + insuranceDescription + `
                </div>
            `);
    $("#common-modal").modal("show");
}

function replaceBreakLine(text) {
    text = text.replace(/'/g, `\\'`);
    text = text.replace(/"/g, `\\"`);
    return text.replace(/(\r\n|\n|\r)/gm, '<br/>');
}