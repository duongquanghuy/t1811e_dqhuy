﻿
$(document).ready(function () {
    var now = new Date();
    var DateAlert = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8);
    var offset1 = DateAlert.getTime() - now.getTime();
    var readAlert = Math.round(offset1 / 1000 / 60 / 60 / 24);
    var cusID = getCookie("cus-id");
    $("table").css("display", "none");
    getData();

});
///tim kiem
function searchVehicle() {
    var search = $('#licensePlates').val();

    if (search != '') {
        $('.loader').modal('show');
        getData();
    } else {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
}
$("#licensePlates").on('keyup', function (e) {
    if (e.keyCode === 13) {
        getData();
    }
    $('.loader').modal('hide');
    $('.load-table').css('display', 'none');
});
			
// format datime
function formatted(dateRequest) {
    var formatted = Date.parse(dateRequest);
    var currentTime = new Date(parseInt(formatted));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var monthformat;
    var dayformat;
    
    if (month < 10 || dayformat < 10) {
        monthformat = "0" + month;
        dayformat = "0" + day;
    }
    if (month >= 10 || dayformat >= 10) {
        monthformat = month;
        dayformat = day;
    }

    var date = monthformat + "-" + dayformat + "-" + year;
    return date;
}
function showNotificationModal(msgHead, msgBody) {
    //msgHead là Tiêu đề của Thông báo, msgBody là Nội dung Thông báo
    $('#notification-modal-container').modal('show');
    $('#noti-modal-header').html(msgHead);
    $('#noti-modal-body').html(msgBody);
    $('#noti-modal-button').trigger("click");
}

    //nếu không nhập thì 1 vs 10 là mặc định
function getData(page = 1, pageSize = 5) {
     var now = new Date();
     var DateAlert = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 8);
     var offset1 = DateAlert.getTime() - now.getTime();
     var readAlert = Math.round(offset1 / 1000 / 60 / 60 / 24);
     var cusID = getCookie("cus-id");
      $('.load-table').css('display', 'flex');
     var url = '';
    var search = $('#licensePlates').val();
    if (search != '' && search != null) {
         url = 'https://localhost:44336/api/billOfSale/BillActive/' + cusID + '/' + page + '/' + pageSize + '?Vehicle=' + search;
     } else {
        url = 'https://localhost:44336/api/billOfSale/BillActive/' + cusID + '/' + page + '/' + pageSize;
     }
        $.ajax({

            type: 'get',
            url: url,
            dataType: 'json',
            success: function (result) {
                if (result.data == null || result.data == '') {
                    $("table").css("display", "inline-table");
                    $('.load-table').css('display', 'none');
                    $('#licensePlates').val('');
                    var msgHead = "Announcement";
                    var msgBody = '<p style="text-algin : center">Currently the System has no data</p>';
                    showNotificationModal(msgHead, msgBody);
                
                } else {
                    console.log(result.data);
                    $("#bill-list-item-active").empty();
                    $(".pagination").empty();
                    $.each(result.data, function (index, val) {

                        var DateExpiresOn = Date.parse(val.expiresOn);
                        var offset = DateExpiresOn - now.getTime();

                        var totalDays = Math.round(offset / 1000 / 60 / 60 / 24);


                        var dateTimeFormat = formatted(val.expiresOn);
                        let html =
                            '<tr>' +
                            '<td data-th="Insurance">' +
                            '<div>' +
                            '<span class="key-table">Insurance Name:   </span><span class="value-table">' + val.insuranceName + '</span>' +
                            '</div>' +
                            '<div>' +
                            '<span class="key-table">Maximum refund value($):   </span><span class="value-table">' + val.maxValueRefund + '</span>' +
                            '</div>' +
                            '<div>' +
                            '<span class="key-table">Maximum number of refunds:   </span><span class="value-table">' + val.maxRefundQuantity + '</span>' +
                            '</div>' +
                            '<div>' +
                            '<span class="key-table">Duration:   </span><span class="value-table"> ' + val.durationMonthUnit + '</span><span class="key-table"> months</span>' +
                            '</div>' +
                            '</td>' +
                            '<td>' +
                            '<div>' +
                            '<span class="key-table">License Plates:   </span><span class="value-table">' + val.licensePlates + '</span>' +
                            '</div>' +
                            '<div>' +
                            '<span class="key-table">Vehicle model:   </span><span class="value-table">' + val.modelName + '</span>' +
                            '</div>' +
                            '<div>' +
                            '<span class="key-table">Vehicle version:   </span><span class="value-table">' + val.versionName + '</span>' +
                            '</div>' +
                            '</td>' +
                            '<td >' +
                            '<div><span class="key-table">Expires On:   </span><span class="value-table"> ' + dateTimeFormat + ' </span></div>' +
                            '<div><span class="key-table readAlert' + totalDays + '">Tim Remaining:   </span><span class="value-table readAlert' + totalDays + '"> ' + totalDays + ' Days </span></div>' +
                            '</td>' +
                            '<td data-th="Price" style="text-align: center">$<span class="value-table" style="font-size:20px">' + val.totalPrice + '</span></td>' +
                            '<td style="text-align: center">' +
                            '<div><button type="button" class="btn btn-warning" data-toggle="modal" data-target="#claim-detail" onclick="openClaimModal(' + val.insuranceSoldId + ', ' + val.insuranceId + ')">Claim <i class="fa fa-comments-o" aria-hidden="true"></i></button></div>' +
                            '</td>' +
                            '</tr>';

                        $("#bill-list-item-active").append(html);
                        if (totalDays <= readAlert) {
                            $('.readAlert' + totalDays + '').css('color', 'red');
                        }
                        $("table").css("display", "inline-table");
                        $('.load-table').css('display', 'none');
                    });

                    let currentPage = result.currentPage;
                    let totalPage = result.totalPage;

                    if (currentPage - 1 > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" id="pagination-previous">
                            <a class="page-link" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                                <span class="sr-only">Previous</span>
                            </a>
                    </li>`)
                    }

                    if (currentPage - 1 > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage - 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage - 1) + `</span></a></li>`)
                    }

                    if (currentPage > 0) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + currentPage + `</span></a></li>`)
                    }

                    if (currentPage + 1 <= totalPage) {
                        $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" class="page-item"><a class="page-link"><span class="val-page-number">` + parseInt(currentPage + 1) + `</span></a></li>`)
                    }

                    if (currentPage + 1 <= totalPage) {
                                $(".pagination").append(`<li value="` + parseInt(currentPage + 1) + `" id="pagination-next" class="page-item">
                        <a class="page-link" aria-label="Next">
                             <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                        </li>`)
                    }

                    $(".pagination > li").on("click", function () {
                        getData($(this).val());
                    })
                }
            },
            error: function () {
                $("table").css("display", "block");
                $('.load-table').css('display', 'none');
                
            }
        });
}

