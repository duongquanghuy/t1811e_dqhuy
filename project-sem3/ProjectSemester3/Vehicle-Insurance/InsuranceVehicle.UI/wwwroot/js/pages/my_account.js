﻿var password;

$(document).ready(function () {
    $('#information-form').css('display', 'none');
    $('.load-table').css('display', 'flex');
    let customerId = getCookie('cus-id');
    if (customerId == '') {
        alert('ERROR USER! PLEASE CONTACT US TO SUPPORT');
    } else {
        $.ajax({
            url: 'https://localhost:44336/api/customer/' + customerId,
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                password = data.data.password;
                $('#full-name').val(data.data.name);
                $('#e-mail').val(data.data.email);
                $('#phone').val(data.data.phoneNumber);
                $('#address').val(data.data.address);
                $('#passport').val(data.data.passport);
                $('#edit-button').append('<button type="button" class="btn btn-default" id="btn-edit-account"' +
                                    'onclick="editAccount()">'+
                    'Edit Profile</button>');
                $('#edit-button-password').append('<a data-toggle="modal" data-target="#change-password"><button type="button" class="btn btn-default btn-lg">Change Password</button></a>');
                $('.load-table').css('display', 'none');
                $('#information-form').css('display', 'block');
            },
            error: function () {
                showNotificationModal("Get Profile", "Error: Get Profile Failed!")
            },
        });
    }
});

function editAccount() {
    var customerId = getCookie('cus-id');
    var fullName = $('#full-name').val();
    var email = $('#e-mail').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    var passport = $('#passport').val();

    if (fullName == '' || phone == '' || address == '') {
        showNotificationModal('Edit Customer', 'Please enter all fields!');
    } else {
        var objEditCustomer = {
            "Id": customerId,
            "Name": fullName,
            "Email": email,
            "PhoneNumber": phone,
            "Address": address,
            "Passport": passport,
        }

        var customerJSON = JSON.stringify(objEditCustomer);

        $.ajax({
            url: 'https://localhost:44336/api/customer/infor',
            type: 'PUT',
            data: customerJSON,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                console.log(data);
                showNotificationModal('Edit Success!', data.message);
            },
            error: function (data) {
                showNotificationModal('Edit Failed!', data.message);
            },
        });
    }
}

function changePassword() {
    var customerId = getCookie('cus-id');
    var passwordLocal = password;
    var oldPassword = $('#old').val();
    var newPassword = $('#new').val();
    var confirmPassword = $('#confirm').val();

    if (newPassword !== confirmPassword) {
        showNotificationModal('Edit Customer', 'Confirmation password does not match!');
    } else if (passwordLocal !== oldPassword) {
        showNotificationModal('Edit Customer', 'Present password incorrect !');
    } else {
        var objEditCustomer = {
            "Id": customerId,
            "Password": newPassword,
        }

        var customerJSON = JSON.stringify(objEditCustomer);

        $.ajax({
            url: 'https://localhost:44336/api/customer/password',
            type: 'PUT',
            data: customerJSON,
            dataType: 'json',
            contentType: 'application/json',
            success: function (data) {
                showNotificationModal('Edit Success!', data.message);
            },
            error: function () {
                showNotificationModal('Edit Failed!','ERROR');
            },
        });
    }
}

function hideChangePassword()
{
    $('#change-password').css('display', 'none');
}