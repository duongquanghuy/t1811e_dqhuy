﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceVehicle.UI.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Index()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if (string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }

        [Route("account/my-vehicle-insurance")]
        public IActionResult MyVehicleInsurance()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if(string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }

        [Route("account/my-vehicle")]
        public IActionResult MyVehicle()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if (string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }

        [Route("account/wait-for-pay")]
        public IActionResult WaitForPay()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if (string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }

        [Route("account/paid")]
        public IActionResult Paid()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if (string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }

        [Route("account/claim")]
        public IActionResult Claim()
        {
            var CustomerIdCookie = Request.Cookies["cus-id"];
            if (string.IsNullOrEmpty(CustomerIdCookie))
                return RedirectToAction("Index", "Authentication");
            return View();
        }
    }
}