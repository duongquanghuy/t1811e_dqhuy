﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace InsuranceVehicle.UI.Controllers
{
    [Route("insurance-vehicle")]
    public class InsuranceVehicleController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Route("create-bill-modal")]
        public IActionResult CreateBillModal()
        {
            return View();
        }
    }
}