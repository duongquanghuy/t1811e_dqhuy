﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsuranceVehicle.UI.Models.EF;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PayPal.Api;

namespace InsuranceVehicle.UI.Controllers
{
    public class PaypalController : Controller
    {
        public readonly InsuranceVehicleContext insuranceVehicleContext;
        public PaypalController(InsuranceVehicleContext dbContext)
        {
            insuranceVehicleContext = dbContext;
        }
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult testSuccess()
        {
            return View("FailureView");
        }

        [Route("paypal/paymentwithpaypal")]
        [Route("paypal/paymentwithpaypal/{idBillOfSale}")]
        public ActionResult PaymentWithPaypal(int? idBillOfSale)
        {
            APIContext apiContext = Configuration.GetAPIContext();
            var billOfSale = insuranceVehicleContext.BillOfSale.SingleOrDefault(bill => bill.Id == idBillOfSale);
            try
            {
                string payerId = HttpContext.Request.Query["PayerID"];

                if (string.IsNullOrEmpty(payerId))
                {

                    string baseURI = this.Request.Scheme + "://" + this.Request.Host + "/Paypal/PaymentWithPayPal?";


                    var guid = Convert.ToString((new Random()).Next(100000));

                    var createdPayment = this.CreatePayment(apiContext, baseURI + "idBillOfSale=" + billOfSale.Id + "&guid=" + guid, billOfSale);


                    var links = createdPayment.links.GetEnumerator();

                    string paypalRedirectUrl = null;

                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;

                        if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {

                            paypalRedirectUrl = lnk.href;
                        }
                    }

                    HttpContext.Session.SetString(guid, createdPayment.id);

                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = HttpContext.Request.Query["guid"];

                    var executedPayment = ExecutePayment(apiContext, payerId, HttpContext.Session.GetString(guid) as string);

                    if (executedPayment.state.ToLower() != "approved")
                    {
                        return View("FailureView");
                    }

                }
            }
            catch (Exception ex)
            {
                return View("FailureView");
            }
            billOfSale.PaymentStatus = 1;
            insuranceVehicleContext.InsuranceSold.Add(new InsuranceSold() { BillOfSaleId = billOfSale.Id, ExpiresOn = DateTime.Now.AddMonths(billOfSale.InsuranceDurationMonthly) });
            insuranceVehicleContext.SaveChanges();

            return View("SuccessView");
        }

        private PayPal.Api.Payment payment;

        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution() { payer_id = payerId };
            this.payment = new Payment() { id = paymentId };
            return this.payment.Execute(apiContext, paymentExecution);
        }

        private Payment CreatePayment(APIContext apiContext, string redirectUrl, BillOfSale billOfSale)
        {


            var itemList = new ItemList() { items = new List<Item>() };

            itemList.items.Add(new Item()
            {
                name = "Insurance Id " + billOfSale.InsuranceId,
                currency = "USD",
                price = billOfSale.TotalPrice.ToString(),
                quantity = "1",
                sku = "sku"
            });

            var payer = new Payer() { payment_method = "paypal" };


            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl,
                return_url = redirectUrl
            };

            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = billOfSale.TotalPrice.ToString()
            };

            var amount = new Amount()
            {
                currency = "USD",
                total = billOfSale.TotalPrice.ToString(),
                details = details
            };

            var transactionList = new List<Transaction>();

            transactionList.Add(new Transaction()
            {
                description = "One product from T1811E Insurance Company group 1 <3",
                invoice_number = Convert.ToString((new Random()).Next(100000)), //Số hóa đơn, mỗi 1 đơn phải 1 số khác nhau, nếu trùng thì lần sau thanh toán sẽ lỗi
                amount = amount,
                item_list = itemList
            });

            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };


            return this.payment.Create(apiContext);

        }
    }
}