﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsuranceVehicle.UI
{
    public class Configuration
    {
        public readonly static string cliendId;
        public readonly static string clientSecret;
        private readonly Dictionary<string, string> _payPalConfig;
        static Configuration()
        {
            var config = getconfig();
            cliendId = "AdVIJLVEBMGOI3JbTXAo3DuTk32E8N-wAkGKY6nmSKrV6ojlrWGEM1L6E_-fN1xU3FejKkP7XVswtHtD";
            clientSecret = "EBDXtWAx0vHs-vDc299qj5G4jkT1Gx9z6vuqOEN5fQDjW3YEkH941kQgG8blpcgAKvMdK5DIS6kIwDjE";

        }

        private static Dictionary<string, string> getconfig()
        {
            return new Dictionary<string, string>()
            {
                { "clientId" , "AdVIJLVEBMGOI3JbTXAo3DuTk32E8N-wAkGKY6nmSKrV6ojlrWGEM1L6E_-fN1xU3FejKkP7XVswtHtD" },
                { "clientSecret", "EBDXtWAx0vHs-vDc299qj5G4jkT1Gx9z6vuqOEN5fQDjW3YEkH941kQgG8blpcgAKvMdK5DIS6kIwDjE" },
                { "mode", "sandbox" },
                { "requestRetries", "1" },
                { "connectionTimeout", "360000" },
            };
        }

        private static string GetAccessToken()
        {

            string accessToken = new OAuthTokenCredential(cliendId, clientSecret ,getconfig()).GetAccessToken();
            return accessToken;
        }

        public static APIContext GetAPIContext()
        {

            APIContext apiContext = new APIContext(GetAccessToken());
            apiContext.Config = getconfig();
            return apiContext;
        }
    }
}
