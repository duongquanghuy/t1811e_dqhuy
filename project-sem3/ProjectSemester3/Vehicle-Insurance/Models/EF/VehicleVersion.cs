﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class VehicleVersion
    {
        public VehicleVersion()
        {
            Estimate = new HashSet<Estimate>();
            PriceListVehicleNew = new HashSet<PriceListVehicleNew>();
            Vehicle = new HashSet<Vehicle>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int? ModelId { get; set; }

        public virtual VehicleModel Model { get; set; }
        public virtual ICollection<Estimate> Estimate { get; set; }
        public virtual ICollection<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual ICollection<Vehicle> Vehicle { get; set; }
    }
}
