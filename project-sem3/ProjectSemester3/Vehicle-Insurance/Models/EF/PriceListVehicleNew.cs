﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class PriceListVehicleNew
    {
        public int Id { get; set; }
        public double Value { get; set; }
        public int? ModelId { get; set; }
        public int? VersionId { get; set; }

        public virtual VehicleModel Model { get; set; }
        public virtual VehicleVersion Version { get; set; }
    }
}
