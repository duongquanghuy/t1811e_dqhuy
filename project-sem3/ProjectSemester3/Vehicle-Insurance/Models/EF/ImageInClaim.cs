﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class ImageInClaim
    {
        public int Id { get; set; }
        public int ClaimDetailId { get; set; }
        public string Link { get; set; }

        public virtual ClaimDetail ClaimDetail { get; set; }
    }
}
