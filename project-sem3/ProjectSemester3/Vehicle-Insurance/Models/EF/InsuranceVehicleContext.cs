﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class InsuranceVehicleContext : DbContext
    {
        public InsuranceVehicleContext()
        {
        }

        public InsuranceVehicleContext(DbContextOptions<InsuranceVehicleContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Admin> Admin { get; set; }
        public virtual DbSet<BillCompensation> BillCompensation { get; set; }
        public virtual DbSet<BillOfSale> BillOfSale { get; set; }
        public virtual DbSet<CalculatorActualValueVehicle> CalculatorActualValueVehicle { get; set; }
        public virtual DbSet<ClaimDetail> ClaimDetail { get; set; }
        public virtual DbSet<CompanyExpense> CompanyExpense { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Estimate> Estimate { get; set; }
        public virtual DbSet<ExpenseType> ExpenseType { get; set; }
        public virtual DbSet<ImageInClaim> ImageInClaim { get; set; }
        public virtual DbSet<Insurance> Insurance { get; set; }
        public virtual DbSet<InsuranceSold> InsuranceSold { get; set; }
        public virtual DbSet<Policy> Policy { get; set; }
        public virtual DbSet<PriceListVehicleNew> PriceListVehicleNew { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Vehicle> Vehicle { get; set; }
        public virtual DbSet<VehicleModel> VehicleModel { get; set; }
        public virtual DbSet<VehicleVersion> VehicleVersion { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=aptech-t1811e-group1.database.windows.net;Initial Catalog=InsuranceVehicle;Persist Security Info=True;User ID=aptech-t1811e-group1;Password=12345678@Abc");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Admin>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Admin)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK__Admin__RoleId__5FB337D6");
            });

            modelBuilder.Entity<BillCompensation>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Reason).HasColumnType("text");

                entity.HasOne(d => d.Admin)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.AdminId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Admin__02084FDA");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Custo__00200768");

                entity.HasOne(d => d.InsuranceSold)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.InsuranceSoldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Insur__03F0984C");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillCompensation)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillCompe__Verhi__01142BA1");
            });

            modelBuilder.Entity<BillOfSale>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ModifiedAt).HasColumnType("datetime");

                entity.Property(e => e.PaymentStatus).HasDefaultValueSql("((0))");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Creat__6A30C649");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Custo__68487DD7");

                entity.HasOne(d => d.Verhicle)
                    .WithMany(p => p.BillOfSale)
                    .HasForeignKey(d => d.VerhicleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__BillOfSal__Verhi__693CA210");
            });

            modelBuilder.Entity<ClaimDetail>(entity =>
            {
                entity.Property(e => e.BodyClaim).HasColumnType("text");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ClaimDeta__Custo__0C85DE4D");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.ClaimDetail)
                    .HasForeignKey(d => d.InsuranceId)
                    .HasConstraintName("FK__ClaimDeta__Insur__0D7A0286");
            });

            modelBuilder.Entity<CompanyExpense>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.ExpensesType)
                    .WithMany(p => p.CompanyExpense)
                    .HasForeignKey(d => d.ExpensesTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CompanyEx__Expen__08B54D69");
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newsequentialid())");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(64)
                    .IsUnicode(false);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RefreshToken).HasColumnType("text");

                entity.Property(e => e.RefreshTokenExpired).HasColumnType("datetime");
            });

            modelBuilder.Entity<Estimate>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Estimate__Custom__59063A47");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Estimate__ModelI__571DF1D5");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Estimate)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Estimate__Versio__5812160E");
            });

            modelBuilder.Entity<ExpenseType>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<ImageInClaim>(entity =>
            {
                entity.Property(e => e.Link)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.ClaimDetail)
                    .WithMany(p => p.ImageInClaim)
                    .HasForeignKey(d => d.ClaimDetailId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ImageInCl__Claim__22751F6C");
            });

            modelBuilder.Entity<Insurance>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Policy)
                    .WithMany(p => p.Insurance)
                    .HasForeignKey(d => d.PolicyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Insurance__Polic__778AC167");
            });

            modelBuilder.Entity<InsuranceSold>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ExpiresOn).HasColumnType("datetime");

                entity.HasOne(d => d.BillOfSale)
                    .WithMany(p => p.InsuranceSold)
                    .HasForeignKey(d => d.BillOfSaleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Insurance__BillO__7B5B524B");

                entity.HasOne(d => d.Insurance)
                    .WithMany(p => p.InsuranceSold)
                    .HasForeignKey(d => d.InsuranceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Insurance__Insur__7D439ABD");
            });

            modelBuilder.Entity<Policy>(entity =>
            {
                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<PriceListVehicleNew>(entity =>
            {
                entity.HasOne(d => d.Model)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__PriceList__Model__5070F446");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.PriceListVehicleNew)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__PriceList__Versi__5165187F");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Vehicle>(entity =>
            {
                entity.Property(e => e.BodyNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.EngineNumber)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.LicensePlates)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.PurchaseDate).HasColumnType("datetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Vehicle__Custome__6477ECF3");

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__Vehicle__ModelId__628FA481");

                entity.HasOne(d => d.Version)
                    .WithMany(p => p.Vehicle)
                    .HasForeignKey(d => d.VersionId)
                    .HasConstraintName("FK__Vehicle__Version__6383C8BA");
            });

            modelBuilder.Entity<VehicleModel>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<VehicleVersion>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.HasOne(d => d.Model)
                    .WithMany(p => p.VehicleVersion)
                    .HasForeignKey(d => d.ModelId)
                    .HasConstraintName("FK__VehicleVe__Model__4D94879B");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
