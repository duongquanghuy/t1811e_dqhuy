﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class InsuranceSold
    {
        public InsuranceSold()
        {
            BillCompensation = new HashSet<BillCompensation>();
            ClaimDetail = new HashSet<ClaimDetail>();
        }

        public int Id { get; set; }
        public int BillOfSaleId { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceId { get; set; }

        public virtual BillOfSale BillOfSale { get; set; }
        public virtual Insurance Insurance { get; set; }
        public virtual ICollection<BillCompensation> BillCompensation { get; set; }
        public virtual ICollection<ClaimDetail> ClaimDetail { get; set; }
    }
}
