﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class Insurance
    {
        public Insurance()
        {
            InsuranceSold = new HashSet<InsuranceSold>();
        }

        public int Id { get; set; }
        public int PolicyId { get; set; }
        public double PricePercentByCarValue { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual Policy Policy { get; set; }
        public virtual ICollection<InsuranceSold> InsuranceSold { get; set; }
    }
}
