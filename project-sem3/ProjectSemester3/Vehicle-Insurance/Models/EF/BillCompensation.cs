﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class BillCompensation
    {
        public int Id { get; set; }
        public string Reason { get; set; }
        public double TotalValue { get; set; }
        public Guid CustomerId { get; set; }
        public int VerhicleId { get; set; }
        public Guid AdminId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public int InsuranceSoldId { get; set; }

        public virtual Admin Admin { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual InsuranceSold InsuranceSold { get; set; }
        public virtual Vehicle Verhicle { get; set; }
    }
}
