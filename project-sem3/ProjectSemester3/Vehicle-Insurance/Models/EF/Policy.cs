﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class Policy
    {
        public Policy()
        {
            Insurance = new HashSet<Insurance>();
        }

        public int Id { get; set; }
        public double MinValueRefund { get; set; }
        public double MaxValueRefund { get; set; }
        public int MaxRefundQuantity { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual ICollection<Insurance> Insurance { get; set; }
    }
}
