﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class ExpenseType
    {
        public ExpenseType()
        {
            CompanyExpense = new HashSet<CompanyExpense>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<CompanyExpense> CompanyExpense { get; set; }
    }
}
