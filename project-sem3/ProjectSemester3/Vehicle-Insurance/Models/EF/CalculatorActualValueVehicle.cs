﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class CalculatorActualValueVehicle
    {
        public int Id { get; set; }
        public int MinYear { get; set; }
        public int MaxYear { get; set; }
        public int PercentValue { get; set; }
    }
}
