﻿using System;
using System.Collections.Generic;

namespace Fpt_Aptech_T1811E_Group1_Vehicle_Insurance_Api.Models.EF
{
    public partial class CompanyExpense
    {
        public int Id { get; set; }
        public double ExpensesAmount { get; set; }
        public int ExpensesTypeId { get; set; }
        public DateTime? CreatedAt { get; set; }

        public virtual ExpenseType ExpensesType { get; set; }
    }
}
