package com.example.demo.model;

import javax.persistence.*;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private boolean isActive;

    @OneToOne(mappedBy = "user" , cascade = CascadeType.ALL)
    private VerificationToken verificationtoken;

    public User() {
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public VerificationToken getVerificationtoken() {
        return verificationtoken;
    }

    public void setVerificationtoken(VerificationToken verificationtoken) {
        this.verificationtoken = verificationtoken;
    }

    public User(Long id, String email, boolean isActive, VerificationToken verificationtoken) {
        this.id = id;
        this.email = email;
        this.isActive = isActive;
        this.verificationtoken = verificationtoken;
    }
}
