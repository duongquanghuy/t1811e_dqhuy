package com.example.demo.service;

import com.example.demo.model.MailProperties;


import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;


import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SendingMailService {
    private final MailProperties mailProperties;
    private final Configuration templates;


    public SendingMailService(MailProperties mailProperties, Configuration templates) {
        this.mailProperties = mailProperties;
        this.templates = templates;
    }
    public boolean sendVerificationMail(String toEmail , String verificationCode){
        String subject = "Please verify your email";
        String body ="";
        try{
            Template t = templates.getTemplate("email-verification.ftl");
            Map<String , String> map = new HashMap<>();
            map.put("VERIFICATION_URL" , mailProperties.getVerificationapi() + verificationCode);
            body = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
        }catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE , e.getMessage(), e);
        }
        return sendMail(toEmail , subject ,body);
    }
    private boolean sendMail(String toEmail , String subject, String body){
        try{
            Properties properties = System.getProperties();
            properties.put("mail.transport.protocol" , "smtp");
            properties.put("mail.smtp.port" , mailProperties.getSmtp().getPort());
            properties.put("mail.smtp.starttls.enable" , "true");
            properties.put("mail.smtp.auth" , "true");
            Session session = Session.getDefaultInstance(properties);
            session.setDebug(true);
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(mailProperties.getFrom() , mailProperties.getFromName()));
            msg.setRecipients(Message.RecipientType.TO, String.valueOf(new InternetAddress(toEmail)));
            msg.setSubject(subject);
            msg.setContent(body , "text/html");
            Transport transport = session.getTransport();
            transport.connect(mailProperties.getSmtp().getHost(), mailProperties.getSmtp().getUsername() ,
            mailProperties.getSmtp().getPassword());
            transport.sendMessage(msg , msg.getAllRecipients());
            return true;

        }catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE , e.getMessage(), e);
        }
        return false;
    }
}


