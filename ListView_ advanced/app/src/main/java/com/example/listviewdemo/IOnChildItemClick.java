package com.example.listviewdemo;

public interface IOnChildItemClick {
    void onItemChildClick(int position);
}
