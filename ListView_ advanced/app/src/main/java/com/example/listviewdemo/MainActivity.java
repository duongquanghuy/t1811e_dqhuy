package com.example.listviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements IOnChildItemClick {

    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();
    private ContactAdapter mAdapter;
    private ImageView ivUser;
    private TextView tvName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        initView();

        mAdapter = new ContactAdapter(listContacts, this);
        mAdapter.registerChildItemClick(this);


        lvContact.setAdapter(mAdapter);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                ContactModel model =listContacts.get(i);
                Toast.makeText(MainActivity.this , model.getName() +": "+ model.getPhone(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView() {
        lvContact = (ListView) findViewById(R.id.lvContact);
        ivUser = (ImageView) findViewById(R.id.ivUser);
        tvName = (TextView) findViewById(R.id.tvName);
    }

    private void initData() {
        ContactModel contact= new ContactModel("Nguyen vam A" ,"012232131", R.drawable.ic_user_a);
        listContacts.add(contact);
        contact= new ContactModel("Nguyen vam b" ,"012232131", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam c" ,"0122321131", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam d" ,"01238", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam e" ,"23173012", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam f" ,"12312783", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam g" ,"5487347", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam l" ,"324823", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam m" ,"28742323", R.drawable.ic_user_a);
        listContacts.add(contact);
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        mAdapter.unRegisterChildClick();
    }

    @Override
    public void onItemChildClick(int position) {
        ContactModel contact = listContacts.get(position);
        ivUser.setImageResource(contact.getImage());
        tvName.setText(contact.getName());
    }
}