package com.example.authenticationregisterspringrest.service;

import com.example.authenticationregisterspringrest.entity.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);

}
