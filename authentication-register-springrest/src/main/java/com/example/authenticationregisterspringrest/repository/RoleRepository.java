package com.example.authenticationregisterspringrest.repository;

import com.example.authenticationregisterspringrest.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role , Long> {
}
