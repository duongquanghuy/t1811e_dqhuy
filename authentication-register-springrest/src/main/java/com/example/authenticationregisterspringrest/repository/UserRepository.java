package com.example.authenticationregisterspringrest.repository;

import com.example.authenticationregisterspringrest.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User , Long> {
    User findByUsername(String username);
}
