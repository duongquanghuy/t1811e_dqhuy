import React, { useEffect, useState } from "react";

import { Button, Modal, Input } from "antd";
import ProductList from "./components/ProductClient/ProductList";
import axios from "axios";
import TextArea from "antd/lib/input/TextArea";

ProductPage.propTypes = {};

function ProductPage(props) {
  const [productList, setProductList] = useState([]);
  const [show, setShowModal] = useState(false);
  const [showSell, setShowModalSell] = useState(false);
  const [item, setItem] = useState({});
  const [actionType, setActionType] = useState("");
  const showModal = () => {
    setActionType("post");
    setShowModal(true);
    setItem({});
  };
  function handleSell(item) {
    setActionType("put");
    console.log(item);
    setItem(item);
    setShowModalSell(true);
  }
  //delete
  function handleDeleteProduct(pro_id) {
    axios
      .delete(`http://localhost:9999/api/v1/products/` + pro_id)
      .then((res) => getDataFromApi())
      .catch((error) => console.log(error));
  }
  //create
  function handleOk() {
    if (actionType === "post") {
      axios
        .post("http://localhost:9999/api/v1/products", JSON.stringify(item), {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => getDataFromApi())
        .catch((err) => console.log(err));
    } else {
      axios
        .put(
          "http://localhost:9999/api/v1/products/" + item.id,
          JSON.stringify({
            ...item,
            quantity: item.quantity - item.quantitySell,
          }),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((res) => getDataFromApi())
        .catch((err) => console.log(err));
    }
    setShowModalSell(false);
    setShowModal(false);
  }

  const handleCancel = () => {
    setShowModal(false);
    setShowModalSell(false);
  };

  function getDataFromApi() {
    const requestURL = "http://localhost:9999/api/v1/products";
    axios
      .get(requestURL)
      .then((res) => setProductList(res.data))
      .catch((err) => console.log(err));
  }
  useEffect(() => {
    getDataFromApi();
  }, []);
  return (
    <div>
      <div>
        <Button type="primary" onClick={showModal}>
          Create Product
        </Button>
      </div>

      <ProductList
        products={productList}
        onClickProduct={handleDeleteProduct}
        product={handleSell}
      />

      <Modal
        visible={show}
        title="Title"
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk}>
            Submit
          </Button>,
        ]}
      >
        <span className="label">Tên sản phẩm</span>
        <Input
          autoFocus={true}
          value={item.name}
          onChange={(e) => {
            setItem({ ...item, name: e.target.value });
          }}
        />
        <span className="label">Giá sản phẩm</span>
        <Input
          value={item.price}
          onChange={(e) => {
            setItem({ ...item, price: e.target.value });
          }}
          type={"number"}
        />
        <span className="label">Quantity</span>
        <Input
          value={item.quantity}
          onChange={(e) => {
            setItem({ ...item, quantity: e.target.value });
          }}
          type={"number"}
        />
      </Modal>
      <Modal
        visible={showSell}
        title="Title"
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk}>
            Submit
          </Button>,
        ]}
      >
        <span className="label">Tên sản phẩm</span>
        <p>{item.name}</p>
        <span className="label">Giá sản phẩm</span>
        <p>{item.price} </p>
        <span className="label">so san lu chon</span>
        <Input
          value={item.quantitySell}
          onChange={(e) => {
            setItem({ ...item, quantity: e.target.value });
          }}
          type={"number"}
        />
      </Modal>
    </div>
  );
}

export default ProductPage;
