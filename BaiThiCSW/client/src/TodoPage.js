import React, { useEffect, useState } from "react";
import TodoList from "./components/TodoClient/TodoList";
import "./App.css";
import FormAdd from "./components/TodoClient/FormAdd";
import axios from "axios";

TodoPage.propTypes = {};

function TodoPage(props) {
  const [todoList, setTodoList] = useState([]);

  function getDataFromApi() {
    const requestURL = "http://localhost:9191/api/v1/todos";
    axios
      .get(requestURL)
      .then((res) => setTodoList(res.data))
      .catch((err) => console.log(err));
  }

  function postDataToApi(item) {
    axios
      .post(
        "http://localhost:9191/api/v1/todos",
        [...todoList, { title: item }],
        {
          headers: { "Content-Type": "application/json" },
        }
      )
      .then((res) => setTodoList(res.data))
      .catch((err) => console.log(err));
  }
  function handleDeleteTask(todo_id) {
    axios
      .delete(`http://localhost:9191/api/v1/todos/` + todo_id)
      .then((res) => getDataFromApi())
      .catch((error) => console.log(error));
  }
  useEffect(() => {
    getDataFromApi();
  }, []);

  return (
    <div>
      <TodoList todos={todoList} onTodoClick={handleDeleteTask} />
      <FormAdd handleSubmit={(item) => postDataToApi(item)} />
    </div>
  );
}

export default TodoPage;
