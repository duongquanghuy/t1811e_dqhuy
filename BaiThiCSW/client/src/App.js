import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import ProductPage from "./ProductPage";

function App() {
  return (
    <Router>
      {/* <Route path="/todos" component={ProductPage} /> */}
      <Redirect exact from="/" to="/products" />
      <Route path="/products" component={ProductPage} />
    </Router>
  );
}
export default App;
