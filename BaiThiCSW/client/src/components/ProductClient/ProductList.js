import React from "react";
import PropTypes from "prop-types";
import { Table, Space, Button, Modal } from "antd";
import "../styles/Product.css";
import { ExclamationCircleOutlined } from "@ant-design/icons";

productList.propTypes = {
  products: PropTypes.array,
  onClickProduct: PropTypes.func,
  product: PropTypes.array,
};
productList.defaultProps = {
  products: [],
  onClickProduct: null,
  product: null,
};

function productList(props) {
  const { products, onClickProduct, product } = props;
  const { confirm } = Modal;
  function showConfirmDelete(item) {
    confirm({
      title: "Bạn có thật sự muốn xóa sản phẩm này?",
      icon: <ExclamationCircleOutlined />,
      content: item.name,
      onOk() {
        rqDeleteItem(item.id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }
  function onClickEditIttem(item) {
    console.log(item);
    if (product) {
      product(item);
    }
  }
  function rqDeleteItem(id) {
    console.log(id);
    if (onClickProduct) {
      onClickProduct(id);
    }
  }

  // function formatDate(date) {
  //   let dateFormat = new Date(date);
  //   return (
  //     formatDateNumber(dateFormat.getDate()) +
  //     "-" +
  //     formatDateNumber(dateFormat.getMonth() + 1) +
  //     "-" +
  //     dateFormat.getFullYear()
  //   );
  // }

  function formatDateNumber(number) {
    if (number < 10) return "0" + number;
    return number + "";
  }

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },

    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="default"
            className="btn-edit"
            onClick={() => {
              onClickEditIttem(record);
            }}
          >
            Sell
          </Button>{" "}
        </Space>
      ),
    },
  ];

  return (
    <div>
      <div className="list-product-page-body">
        <Table dataSource={products} columns={columns} rowKey={"id"} />;
      </div>
    </div>
  );
}

export default productList;
