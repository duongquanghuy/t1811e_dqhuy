import React from 'react';
import PropTypes from 'prop-types';
import { useState } from 'react';

FormAdd.propTypes = {
    onSubmit: PropTypes.func
};
FormAdd.defaultProps = {
    onSubmit: null
}

function FormAdd(props) {
 
    const [value, setValue] = useState('');

    function handleValueOnchange(e) {
        console.log(e.target.value);
        setValue(e.target.value);
    }
    

    return (
        <form onSubmit={(e) => { props.handleSubmit(value); e.preventDefault(); setValue("") }}>
            <input type="text" value={value} onChange={handleValueOnchange} />
        </form>
    );
}

export default FormAdd;