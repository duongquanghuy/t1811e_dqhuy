import React from 'react';
import PropTypes from 'prop-types';
import "../styles/Todo.css";


TodoList.propTypes = {
    todos: PropTypes.array,
    onTodoClick: PropTypes.func,

};

TodoList.defaultProps = {
    todos: [],
    onTodoClick: null,
}
function TodoList(props) {
    const { todos, onTodoClick } = props;
  
    function handleClick(todo) {
        console.log(todo)
        if (onTodoClick) {
        
          onTodoClick(todo);
          
        }
      }
    return (
        <div className="todo-list">
            {todos.map(task => (
                <div className="todo">
                    <span key={task.id} style={{ fontSize: 16 }} >
                        {task.title}
                    </span>
                    <button onClick={() =>handleClick(task.id)}  style={ { fontSize: 20 } }>x</button>
                </div>
            ))}
        </div>
    );
}

export default TodoList;