package com.example.demo.api;

import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
@Slf4j
@RestController
@RequestMapping("api/v1/products")
public class ProductAPI {
    private final ProductService productService;

    public ProductAPI(ProductService productService) {
        this.productService = productService;
    }
    @GetMapping
    public ResponseEntity<List<Product>> findAll(){
        return ResponseEntity.ok(productService.finAll());
    }

    @PostMapping
    public ResponseEntity<Product> save(@Valid @RequestBody Product product){
        return  ResponseEntity.ok(productService.save(product));
    }
    @PutMapping
    public ResponseEntity<Product> update(@PathVariable Long id , @Valid @RequestBody Product product){
        if(!productService.finById(id).isPresent()){
            log.error(" is not existed");
        }
        return  ResponseEntity.ok(productService.save(product));
    }
}
