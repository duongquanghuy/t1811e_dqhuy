package com.example.demo.api;

import com.example.demo.entity.People;
import com.example.demo.service.PeopleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/peoples")
public class PeopleAPI {
    private final PeopleService peopleService;

    public PeopleAPI(PeopleService peopleService) {
        this.peopleService = peopleService;
    }


    @GetMapping
    public ResponseEntity<List<People>> findAll(){
        return ResponseEntity.ok(peopleService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody People people){
        return ResponseEntity.ok(peopleService.save(people));
    }

    @GetMapping("/{id}")
    public ResponseEntity<People> findById(@PathVariable Long id){
        Optional<People> product = peopleService.findById(id);
        if(!product.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(product.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<People> findById(@PathVariable Long id, @Valid @RequestBody People peopleUpdate){
        Optional<People> product = peopleService.findById(id);
        if(!product.isPresent()){
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(peopleService.save(peopleUpdate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<People> deleteById(@PathVariable Long id){
        Optional<People> product = peopleService.findById(id);
        if(!product.isPresent()){
            ResponseEntity.badRequest().build();
        }
        peopleService.DeleteById(id);
        return ResponseEntity.ok().build();
    }
}
