package com.example.demo.service;

import com.example.demo.entity.People;
import com.example.demo.repository.PeopleRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public PeopleService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }
    public List<People> findAll(){
        return peopleRepository.findAll();
    }

    public Optional<People> findById(Long id){
        return peopleRepository.findById(id);
    }

    public People save(People people){
        return peopleRepository.save(people);
    }

    public void DeleteById(Long id){
        peopleRepository.deleteById(id);
    }
}
