import React from "react";
import logo from "./logo.svg";
import "./App.css";
import PeoplePage from "./components/PeopleClient/PeoplePage";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

function App() {
  return (
    <Router>
      <Redirect exact from="/" to="/peoples" />
      <Route path="/peoples" component={PeoplePage} />
    </Router>
  );
}

export default App;
