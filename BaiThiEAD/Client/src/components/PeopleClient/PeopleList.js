import React from "react";
import PropTypes from "prop-types";
import { Table, Space, Button, Modal } from "antd";
import "../style/People.css";
import { ExclamationCircleOutlined } from "@ant-design/icons";

PeopleList.propTypes = {
  peoples: PropTypes.array,
  onClickPeople: PropTypes.func,
  people: PropTypes.array,
};
PeopleList.defaultProps = {
  peoples: [],
  onClickPeople: null,
  people: null,
};

function PeopleList(props) {
  const { peoples, onClickPeople, people } = props;
  const { confirm } = Modal;
  function showConfirmDelete(item) {
    confirm({
      title: "Bạn có thật sự muốn xóa sản phẩm này?",
      icon: <ExclamationCircleOutlined />,
      content: item.name,
      onOk() {
        rqDeleteItem(item.id);
      },
      onCancel() {
        console.log("Cancel");
      },
    });
  }
  function onClickEditIttem(item) {
    console.log(item);
    if (people) {
      people(item);
    }
  }
  function rqDeleteItem(id) {
    console.log(id);
    if (onClickPeople) {
      onClickPeople(id);
    }
  }

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            type="default"
            className="btn-edit"
            onClick={() => {
              onClickEditIttem(record);
            }}
          >
            Sửa
          </Button>{" "}
          <Button
            type="danger"
            danger
            className="btn-delete"
            onClick={() => {
              showConfirmDelete(record);
            }}
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <div>
      <div className="list-product-page-body">
        <Table dataSource={peoples} columns={columns} rowKey={"id"} />;
      </div>
    </div>
  );
}

export default PeopleList;
