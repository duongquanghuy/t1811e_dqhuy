import React, { useEffect, useState } from "react";

import { Button, Modal, Input } from "antd";
import PeopleListPage from "./PeopleList";
import axios from "axios";
import TextArea from "antd/lib/input/TextArea";

PeoplePage.propTypes = {};

function PeoplePage(props) {
  const [peopleList, setPeopleList] = useState([]);
  const [show, setShowModal] = useState(false);
  const [item, setItem] = useState({});
  const [actionType, setActionType] = useState("");
  const showModal = () => {
    setActionType("post");
    setShowModal(true);
    setItem({});
  };
  function handleEdit(item) {
    setActionType("put");
    console.log(item);
    setItem(item);
    setShowModal(true);
  }
  //delete
  function handleDeletePeople(peo_id) {
    axios
      .delete(`http://localhost:9090/api/v1/peoples/` + peo_id)
      .then((res) => getDataFromApi())
      .catch((error) => console.log(error));
  }
  //create
  function handleOk() {
    if (actionType === "post") {
      axios
        .post("http://localhost:9090/api/v1/peoples", JSON.stringify(item), {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((res) => getDataFromApi())
        .catch((err) => console.log(err));
    } else {
      axios
        .put(
          "http://localhost:9090/api/v1/peoples/" + item.id,
          JSON.stringify(item),
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((res) => getDataFromApi())
        .catch((err) => console.log(err));
    }

    setShowModal(false);
  }

  const handleCancel = () => {
    setShowModal(false);
  };

  function getDataFromApi() {
    const requestURL = "http://localhost:9090/api/v1/peoples";
    axios
      .get(requestURL)
      .then((res) => setPeopleList(res.data))
      .catch((err) => console.log(err));
  }
  useEffect(() => {
    getDataFromApi();
  }, []);
  return (
    <div>
      <div>
        <Button type="primary" onClick={showModal}>
          Create Product
        </Button>
      </div>

      <PeopleListPage
        peoples={peopleList}
        onClickPeople={handleDeletePeople}
        people={handleEdit}
      />

      <Modal
        visible={show}
        title="Title"
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="back" onClick={handleCancel}>
            Cancel
          </Button>,
          <Button key="submit" type="primary" onClick={handleOk}>
            Submit
          </Button>,
        ]}
      >
        <span className="label">name</span>
        <Input
          autoFocus={true}
          value={item.name}
          onChange={(e) => {
            setItem({ ...item, name: e.target.value });
          }}
        />
        <span className="label">Address</span>
        <TextArea
          value={item.address}
          onChange={(e) => {
            setItem({ ...item, address: e.target.value });
          }}
        />
      </Modal>
    </div>
  );
}

export default PeoplePage;
