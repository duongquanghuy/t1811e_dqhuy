package com.example.fullstackrestspringboottodoapplication.service;

import com.example.fullstackrestspringboottodoapplication.entity.Todo;
import com.example.fullstackrestspringboottodoapplication.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }


    public List<Todo> findAll(){
        return todoRepository.findAll();
    }
    public List<Todo> savaAll(List<Todo> todos){
        return todoRepository.saveAll(todos);
    }
    public Optional<Todo> findById(Long id){
        return todoRepository.findById(id);
    }
    public void deleteById(Long id){  todoRepository.deleteById(id) ;   }
}
