package com.example.fullstackrestspringboottodoapplication.api;

import com.example.fullstackrestspringboottodoapplication.entity.Todo;
import com.example.fullstackrestspringboottodoapplication.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/todos")
public class TodoAPI {
    private final TodoService todoService;

    @Autowired
    public TodoAPI(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public ResponseEntity<List<Todo>> findAll(){
        return ResponseEntity.ok(todoService.findAll());
    }
    @PostMapping
    public ResponseEntity savaAll(@Valid @RequestBody List<Todo> todos){
        return  ResponseEntity.ok(todoService.savaAll(todos));

    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Todo> delete(@PathVariable Long id ){

        Optional<Todo> t = todoService.findById(id);
        if(!t.isPresent()){
            ResponseEntity.badRequest().build();
        }
        todoService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
