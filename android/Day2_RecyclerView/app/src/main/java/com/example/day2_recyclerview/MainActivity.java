package com.example.day2_recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Product> productList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();

        ProductAdapter productAdapter = new ProductAdapter(this,productList);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this ,2);

        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(productAdapter);
    }

    private void initData() {
        productList.add(new Product("Product 1" , "5000" , R.drawable.p1));
        productList.add(new Product("Product 2" , "6000" , R.drawable.p2));
        productList.add(new Product("Product 3" , "7000" , R.drawable.p3));
        productList.add(new Product("Product 4" , "8000" , R.drawable.p4));
        productList.add(new Product("Product 5" , "8000" , R.drawable.p5));
        productList.add(new Product("Product 6" , "10000" , R.drawable.p6));
    }


}