package com.example.listviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        lvContact = (ListView)findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContacts, this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel =listContacts.get(position);
                Toast.makeText(MainActivity.this , contactModel.getName() , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initData() {
        ContactModel contact= new ContactModel("Nguyen vam A" ,"012232131", R.drawable.ic_user_a);
        listContacts.add(contact);
        contact= new ContactModel("Nguyen vam b" ,"012232131", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam c" ,"0122321131", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam d" ,"01238", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam e" ,"23173012", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam f" ,"12312783", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam g" ,"5487347", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam l" ,"324823", R.drawable.ic_user_a);
        listContacts.add(contact);
         contact= new ContactModel("Nguyen vam m" ,"28742323", R.drawable.ic_user_a);
        listContacts.add(contact);
    }
}