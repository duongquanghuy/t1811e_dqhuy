package com.example.network_demo.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
