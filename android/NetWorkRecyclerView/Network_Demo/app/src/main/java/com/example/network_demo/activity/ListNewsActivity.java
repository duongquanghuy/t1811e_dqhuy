package com.example.network_demo.activity;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.network_demo.R;
import com.example.network_demo.adapter.NewsAdapter;
import com.example.network_demo.model.Item;
import com.example.network_demo.network.ApiManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListNewsActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Item> listData;
    NewsAdapter adapter;
    @Override
    protected void onCreate(Bundle saveInstanceState) {

        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_list_news);

        getListData();

        listData = new ArrayList<>();
        adapter = new NewsAdapter( ListNewsActivity.this , listData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL , false);

        recyclerView = findViewById(R.id.rvListNews);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void getListData() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiManager.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        ApiManager service = retrofit.create(ApiManager.class);
        service.getListData().enqueue(new Callback<List<Item>>() {
            @Override
            public void onResponse(Call<List<Item>> call, Response<List<Item>> response) {
                if(response.body() != null){
                    listData= response.body();
                    adapter.reloadData(listData);
                }
            }

            @Override
            public void onFailure(Call<List<Item>> call, Throwable t) {
                Toast.makeText(ListNewsActivity.this , "Fail" ,Toast.LENGTH_LONG).show();
            }
        });
    }
}
