package com.example.appusersapi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.appusersapi.IOnChildItemClick;
import com.example.appusersapi.R;
import com.example.appusersapi.model.User;

import java.util.List;

public class UserAdapter extends  BaseAdapter {

    private List<User> userList;
    private IOnChildItemClick iOnChildItemClick;
    private Context mContext;

    public UserAdapter(Context mContext, List<User> userList) {
        this.mContext = mContext;
        this.userList = userList;
    }
    public void setUsers(List<User> userList) {
        this.userList = userList;
        this.notifyDataSetChanged();
    }
    public void onChildItemClick(IOnChildItemClick iOnChildItemClick){
        this.iOnChildItemClick = iOnChildItemClick;
    }


    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final  int i, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if(rowView == null){
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            rowView = inflater.inflate(R.layout.activity_item , null);
            viewUser user = new viewUser();
            user.tvName = (TextView)rowView.findViewById(R.id.tvName);
            user.tvUsername = (TextView)rowView.findViewById(R.id.tvUsername);
            user.tvPassword = (TextView)rowView.findViewById(R.id.tvPassword);
            user.tvEmail = (TextView)rowView.findViewById(R.id.tvEmail);
            user.btEdit = (ImageButton) rowView.findViewById(R.id.btEdit);
            user.btDetail = (ImageButton) rowView.findViewById(R.id.btDetail);
            user.btDelete = (ImageButton) rowView.findViewById(R.id.btDelete);
            rowView.setTag(user);
        }

        viewUser user = (viewUser) rowView.getTag();
        user.tvName.setText(userList.get(i).getName());
      /*  user.tvEmail.setText(userList.get(i).getEmail());*/
      /*  user.tvPassword.setText(userList.get(i).getPassword());*/
        user.tvUsername.setText(userList.get(i).getUsername());
        user.btDetail.setOnClickListener(new View.OnClickListener(){
            @Override
            public  void onClick(View v){
                iOnChildItemClick.onItemChildClickDetail(i);
            }
        });
        user.btDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iOnChildItemClick.onItemChildClickDelete(i);
            }
        });
        user.btEdit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){iOnChildItemClick.onItemChildClick(i);}
        });
        return rowView;
    }

    static class viewUser{
        ImageButton btEdit;
        ImageButton btDetail;
        ImageButton btDelete;
        TextView tvName;
        TextView tvUsername;
        TextView tvPassword;
        TextView tvEmail;

    }


}
