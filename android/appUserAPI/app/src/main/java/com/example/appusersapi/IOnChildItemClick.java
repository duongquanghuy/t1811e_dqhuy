package com.example.appusersapi;

public interface IOnChildItemClick {
    void onItemChildClick(int position);
    void onItemChildClickDelete(int position);
    void onItemChildClickDetail(int position);
}
