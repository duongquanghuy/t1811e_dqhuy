package com.example.appusersapi.network;

import com.example.appusersapi.model.User;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface APIManager {

    String BASE_URL = "https://5fca48db3c1c22001644217d.mockapi.io/v1/user/";

    @GET("users")
    Call<List<User>> getHour();

    @Headers({"Content-Type: application/json"})
    @PUT("users/{id}")
    Call<User> updateUser(@Path("id") long id, @Body User user);

    @Headers({"Content-Type: application/json"})
    @POST("users")
    Call<User> createUser(@Body User user);

    @DELETE("users/{id}")
    Call<String> deleteUser(@Path("id") long id);
}
