package com.example.appusersapi.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.appusersapi.R;
import com.example.appusersapi.model.User;

public class DetailUserActivity extends AppCompatActivity {
    private TextView tvName;
    private  TextView tvUsername;
    private TextView tvPassword;
    private  TextView tvEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        initView();

        User user = (User) getIntent().getSerializableExtra("user");

        tvName.setText(user.getName());
        tvEmail.setText(user.getEmail());
        tvPassword.setText(user.getPassword());
        tvUsername.setText(user.getUsername());
    }

    private void initView() {
        tvName = (TextView) findViewById(R.id.tvName);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvPassword = (TextView) findViewById(R.id.tvPassword);
        tvEmail =  (TextView) findViewById(R.id.tvEmail);
    }
}
