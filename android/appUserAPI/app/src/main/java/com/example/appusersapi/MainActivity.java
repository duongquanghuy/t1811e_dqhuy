package com.example.appusersapi;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.appusersapi.activity.CreateUserActivity;
import com.example.appusersapi.activity.DetailUserActivity;
import com.example.appusersapi.adapter.UserAdapter;
import com.example.appusersapi.model.User;
import com.example.appusersapi.network.APIManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener , IOnChildItemClick{
    private ListView lvUser;
    private UserAdapter adapter;
    private List<User> userList = new ArrayList<>();
    private Button btCreate;
    private TextView tvUsername;
    private TextView tvName;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btCreate = (Button)findViewById(R.id.btCreate);
        btCreate.setOnClickListener(this);

        getHours();
        initView();
        adapter = new UserAdapter( this, userList);
        adapter.onChildItemClick(this);

        lvUser.setAdapter(adapter);
        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = userList.get(position);
            }
        });




    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btCreate:
                onAction();
                break;

        }
    }
    private void initView() {
        lvUser = (ListView) findViewById(R.id.lvUsers);
        tvUsername = (TextView) findViewById(R.id.tvUsername);
        tvName = (TextView) findViewById(R.id.tvName);
    }
    private void onAction() {
        Intent intent = new Intent(this, CreateUserActivity.class);
        startActivity(intent);
    }

    private void getHours() {
       /* User user = new User((long) 1,"Nguyen vam A" ,"012232131", "email" , "d");
        userList.add(user);
        user = new User((long) 2,"Nguyen vam A" ,"012232131", "email" , "d");
        userList.add(user);

        user = new User((long) 3,"Nguyen vam A" ,"012232131", "email" , "d");
        userList.add(user);*/
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIManager.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIManager service = retrofit.create(APIManager.class);
        service.getHour().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.body() == null) {
                    return;
                }
                userList = response.body();
                adapter.setUsers(userList);


            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                System.out.println(t);
            }
        });
    }

    @Override
    public void onItemChildClick(int position) {
        User model = userList.get(position);
        Intent intent = new Intent(this, CreateUserActivity.class);
        intent.putExtra("user" ,model);
        startActivity(intent);
    }

    @Override
    public void onItemChildClickDelete(int position) {
        User model = userList.get(position);
            long id = model.getId();

                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Ban co muon xoa "+model.getName()+" ?");
                // alert.setMessage("Message");

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton ) {
                        //Your action here

                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl(APIManager.BASE_URL)
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        APIManager service = retrofit.create(APIManager.class);
                        service.deleteUser(id).enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Toast.makeText(MainActivity.this, "Delete Success" , Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(MainActivity.this, MainActivity.class);
                                startActivity(intent);
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Toast.makeText(MainActivity.this, "Delete Fail" , Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });

                alert.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        });

                alert.show();




        alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }
                });

        alert.show();

    }

    @Override
    public void onItemChildClickDetail(int position) {
        User user = userList.get(position);
        Intent intent = new Intent(this , DetailUserActivity.class);
        intent.putExtra("user" , user);
        startActivity(intent);
    }


}