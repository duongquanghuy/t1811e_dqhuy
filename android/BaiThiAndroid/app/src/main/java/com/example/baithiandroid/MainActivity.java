package com.example.baithiandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.baithiandroid.database.AppDatabase;
import com.example.baithiandroid.database.UserEntity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    AppDatabase db;
    private EditText etName;
    private EditText etEmail;
    private EditText etDes;
    private Button btSubmit;
    private Spinner spinner;
    private CheckBox checkBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );

        setContentView( R.layout.activity_main );
        db = AppDatabase.getAppDatabase( this );

        btSubmit = (Button) findViewById(R.id.btSubmit);
        btSubmit.setOnClickListener(this);
        initView();

    }

    private void initView() {
        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        spinner = (Spinner) findViewById(R.id.spinner);
        etDes =  (EditText) findViewById(R.id.etEmail);
        checkBox = (CheckBox) findViewById(R.id.ck);

        String[] gender = {"Male" , "Female" , "UnKnow"};
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, gender);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btSubmit:
                onSubmit();
                break;
            default:
                break;
        }
    }

    private void onSubmit() {
        if (etName.getText().toString().isEmpty()) {
            Toast.makeText( this, "Please enter username", Toast.LENGTH_SHORT ).show();
            return;
        }
        if (!checkBox.isChecked()) {
            Toast.makeText( this, "Please agree rules", Toast.LENGTH_SHORT ).show();
            return;
        }
        UserEntity user = new UserEntity();
        user.setName( etName.getText().toString() );
        user.setDescription( etDes.getText().toString() );
        user.setEmail( etEmail.getText().toString() );
        user.setGender( spinner.getSelectedItem().toString() );

        db.userDao().insertUser( user );


        Toast.makeText( this, "Count : " + db.userDao().getCountUser(), Toast.LENGTH_SHORT ).show();
    }
}