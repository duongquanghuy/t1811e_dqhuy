package com.example.baithiandroid.database;


import androidx.room.Dao;

import androidx.room.Insert;
import androidx.room.Query;




import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDao {
    @Insert(onConflict = REPLACE)
    void insertUser(UserEntity userEntity);

    @Query( "SELECT count(*) FROM User " )
    Integer getCountUser();


}
